//
//  ViewController.swift
//  JSCoreDemo
//
//  Created by Hardik Trivedi on 25/09/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import JavaScriptCore

class ViewController: UIViewController {

    @IBOutlet weak var num1TextField: UITextField!
    @IBOutlet weak var num2TextField: UITextField!
    @IBOutlet weak var resultslabel: UILabel!
    
    var context: JSContext? = {
        let context = JSContext()
        guard let
            commonJSPath = Bundle.main.path(forResource: "commons", ofType: "js") else {
                print("Unable to read resource files.")
                return nil
        }
        do {
            let common = try String(contentsOfFile: commonJSPath, encoding: String.Encoding.utf8)
            _ = context?.evaluateScript(common)
        } catch (let error) {
            print("Error while processing script file: \(error)")
        }
        
        return context
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        resultslabel.text = ""
    }
    
    @IBAction func addUsingJSCode(_ sender: UIButton) {
        if let context = context{
            let addFunc = context.objectForKeyedSubscript("addNumbers")
            if let resultValue = addFunc?.call(withArguments: [Int64(num1TextField.text!)!,Int64(num2TextField.text!)!]){
                resultslabel.text = resultValue.toString()
            }
        }
    }
}



/**
 *************************************************
    MovieService class
 *************************************************
 */
import UIKit
import JavaScriptCore

let movieUrl = "https://itunes.apple.com/us/rss/topmovies/limit=50/json"

var context: JSContext? = {
    let context = JSContext()
    guard let
        commonJSPath = Bundle.main.path(forResource: "common", ofType: "js") else {
            print("Unable to read resource files.")
            return nil
    }
    do {
        let common = try String(contentsOfFile: commonJSPath, encoding: String.Encoding.utf8)
        _ = context?.evaluateScript(common)
    } catch (let error) {
        print("Error while processing script file: \(error)")
    }
    
    return context
}()

class MovieService {
  
  func loadMoviesWith(limit: Double, onComplete complete: @escaping ([Movie]) -> ()) {
    guard let url = URL(string: movieUrl) else {
        print("Invalid url format: \(movieUrl)")
        return
    }
    
    URLSession.shared.dataTask(with: url) { data, _, _ in
        guard let data = data, let jsonString = String(data: data, encoding: String.Encoding.utf8) else {
            print("Error while parsing the response data.")
            return
        }
        
        let movies = self.parse(response: jsonString, withLimit: limit)
        complete(movies)
        }.resume()
  }
  
  func parse(response: String, withLimit limit: Double) -> [Movie] {
    guard let context = context else {
        print("JSContext not found.")
        return []
    }
    let parseFunction = context.objectForKeyedSubscript("parseJson")
    guard let parsed = parseFunction?.call(withArguments: [response]).toArray() else {
        print("Unable to parse JSON")
        return []
    }
    let filterFunction = context.objectForKeyedSubscript("filterByLimit")
    let filtered = filterFunction?.call(withArguments: [parsed, limit]).toArray()
    
    let builderBlock = unsafeBitCast(Movie.movieBuilder, to: AnyObject.self)
    
    context.setObject(builderBlock, forKeyedSubscript: "movieBuilder" as (NSCopying & NSObjectProtocol)!)
    let builder = context.evaluateScript("movieBuilder")
    
    guard let unwrappedFiltered = filtered,
        let movies = builder?.call(withArguments: [unwrappedFiltered]).toArray() as? [Movie] else {
            print("Error while processing movies.")
            return []
    }
    
    return movies

  }
}

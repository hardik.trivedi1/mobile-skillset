/*
Movie model class
*/

import Foundation

class Movie: NSObject {
  
  var title: String
  var price: String
  var imageUrl: String
  
  init(title: String, price: String, imageUrl: String) {
    self.title = title
    self.price = price
    self.imageUrl = imageUrl
  }
    
    static let movieBuilder: @convention(block) ([[String : String]]) -> [Movie] = { object in
        return object.map { dict in
            
            guard
                let title = dict["title"],
                let price = dict["price"],
                let imageUrl = dict["imageUrl"] else {
                    print("unable to parse Movie objects.")
                    fatalError()
            }
            
            return Movie(title: title, price: price, imageUrl: imageUrl)
        }
    }

}

//
//  ViewController.swift
//  HealthKitDemo
//
//  Created by Hardik Trivedi on 01/10/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HealthKit

class HKDHomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var data = [Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        authorizeHealthKit(completion: { (authorized,error) in
            if error == nil{
                if authorized{
                    print("HealthKit authorized the given read/write types.")
                    
                    self.loadHealthData()
                    
                }else{
                    print("HealthKit not authorized.")
                }
            }else{
                print(error?.localizedDescription ?? "Authorization Error.")
            }
        })
    }
    
    func authorizeHealthKit(completion: @escaping (Bool, Error?) -> Void) {
        if HKHealthStore.isHealthDataAvailable(){
            if let dateOfBirth = HKObjectType.characteristicType(forIdentifier: .dateOfBirth),
                let bloodType = HKObjectType.characteristicType(forIdentifier: .bloodType),
                let biologicalSex = HKObjectType.characteristicType(forIdentifier: .biologicalSex),
                let bodyMassIndex = HKObjectType.quantityType(forIdentifier: .bodyMassIndex),
                let height = HKObjectType.quantityType(forIdentifier: .height),
                let bodyMass = HKObjectType.quantityType(forIdentifier: .bodyMass),
                let activeEnergy = HKObjectType.quantityType(forIdentifier: .activeEnergyBurned){
                
                let healthKitTypesToWrite: Set<HKSampleType> = [bodyMassIndex,
                                                                activeEnergy,
                                                                HKObjectType.workoutType()]
                
                let healthKitTypesToRead: Set<HKObjectType> = [dateOfBirth,
                                                               bloodType,
                                                               biologicalSex,
                                                               bodyMassIndex,
                                                               height,
                                                               bodyMass,
                                                               HKObjectType.workoutType()]
                
                HKHealthStore().requestAuthorization(toShare: healthKitTypesToWrite,
                                                     read: healthKitTypesToRead) { (success, error) in
                                                        completion(success, error)
                }


            } else {
                print("HealthKit Type not available.")
            }

        }else{
            print("HealthKit is not available.")
        }
    }

    func loadHealthData(){
        do{
            let healthdata = try self.getAgeSexAndBloodType()
            self.data.append(healthdata.age)
            self.data.append(healthdata.biologicalSex.stringRepresentation)
            self.data.append(healthdata.bloodType.stringRepresentation)
            
            guard let heightSampleType = HKSampleType.quantityType(forIdentifier: .height) else {
                print("Height Sample Type is no longer available in HealthKit")
                return
            }
            
            self.getMostRecentSample(for: heightSampleType) { (sample, error) in
                if let data = sample {
                    self.data.append(data.quantity.doubleValue(for: HKUnit.meter()))
                    print("\(data.quantity.doubleValue(for: HKUnit.meter()))")
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }

            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    func getAgeSexAndBloodType() throws -> (age: Int, biologicalSex: HKBiologicalSex, bloodType: HKBloodType) {
            
            let healthKitStore = HKHealthStore()
            
            do {
                
                let birthdayComponents =  try healthKitStore.dateOfBirthComponents()
                let biologicalSex =       try healthKitStore.biologicalSex()
                let bloodType =           try healthKitStore.bloodType()
                
                let today = Date()
                let calendar = Calendar.current
                let todayDateComponents = calendar.dateComponents([.year],
                                                                  from: today)
                let thisYear = todayDateComponents.year!
                let age = thisYear - birthdayComponents.year!
                
                let unwrappedBiologicalSex = biologicalSex.biologicalSex
                let unwrappedBloodType = bloodType.bloodType
                
                return (age, unwrappedBiologicalSex, unwrappedBloodType)
            }
    }
    
    func getMostRecentSample(for sampleType: HKSampleType,
                                   completion: @escaping (HKQuantitySample?, Error?) -> Swift.Void) {
        let mostRecentPredicate = HKQuery.predicateForSamples(withStart: Date.distantPast,
                                                              end: Date(),
                                                              options: .strictEndDate)
        
        let sortDescriptor = NSSortDescriptor(key: HKSampleSortIdentifierStartDate,
                                              ascending: false)
        
        let limit = 1
        
        let sampleQuery = HKSampleQuery(sampleType: sampleType,
                                        predicate: mostRecentPredicate,
                                        limit: limit,
                                        sortDescriptors: [sortDescriptor]) { (query, samples, error) in
                                            
                                            DispatchQueue.main.async {
                                                
                                                guard let samples = samples,
                                                    let mostRecentSample = samples.first as? HKQuantitySample else {
                                                        
                                                        completion(nil, error)
                                                        return
                                                }
                                                
                                                completion(mostRecentSample, nil)
                                            }
        }
        
        HKHealthStore().execute(sampleQuery)
    }


}

extension HKDHomeViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "User Info"
        default:
            return ""
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HKDCell", for: indexPath)
        switch indexPath.row {
        case 0:
            cell.textLabel?.text = "Age"
            cell.detailTextLabel?.text = "\(data[indexPath.row] as! Int)"
        case 1:
            cell.textLabel?.text = "Sex"
            cell.detailTextLabel?.text = "\(data[indexPath.row] as! String)"
        case 2:
            cell.textLabel?.text = "Blood Type"
            cell.detailTextLabel?.text = "\(data[indexPath.row] as! String)"
        case 3:
            cell.textLabel?.text = "Height"
            cell.detailTextLabel?.text = "\(data[indexPath.row] as! Double) m"
        default:
            break
        }
        return cell
    }
}
extension HKBiologicalSex {
    
    var stringRepresentation: String {
        switch self {
        case .notSet: return "Unknown"
        case .female: return "Female"
        case .male: return "Male"
        case .other: return "Other"
        }
    }
}
extension HKBloodType {
    
    var stringRepresentation: String {
        switch self {
        case .notSet: return "Unknown"
        case .aPositive: return "A+"
        case .aNegative: return "A-"
        case .bPositive: return "B+"
        case .bNegative: return "B-"
        case .abPositive: return "AB+"
        case .abNegative: return "AB-"
        case .oPositive: return "O+"
        case .oNegative: return "O-"
        }
    }
}


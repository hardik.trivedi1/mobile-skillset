//
//  RPSTurn.h
//  RockPaperScissor
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPSTurn.h"
typedef NS_ENUM(NSInteger, Move) {
    Rock,
    Paper,
    Scissors,
    Invalid
};

@interface RPSTurn : NSObject


@property (nonatomic) Move move;

-(instancetype)initWithMove:(Move) move;
-(BOOL) defeats: (RPSTurn*) opponent;
-(NSString*) description;
@end

//
//  RPSController.h
//  RockPaperScissor
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPSGame.h"
@interface RPSController : NSObject

@property (nonatomic) RPSGame *game;
-(void) throwDown: (Move) playerMove;
-(void) printResult;
@end

//
//  main.m
//  RockPaperScissor
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RPSController.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        RPSController *gameController = [[RPSController alloc] init];
        [gameController throwDown: Scissors];
        [gameController printResult];
    }
    return 0;
}

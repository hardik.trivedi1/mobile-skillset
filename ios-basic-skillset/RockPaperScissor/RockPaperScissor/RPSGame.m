//
//  RPSGame.m
//  RockPaperScissor
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "RPSGame.h"

@implementation RPSGame

-(instancetype)initWithFirstTurn:(RPSTurn*) playerTurn secondTurn: (RPSTurn*)computerTurn {
    self = [super init];
    
    if(self) {
        _firstTurn = playerTurn;
        _secondTurn = computerTurn;
    }
    
    return self;
}

-(RPSTurn*) winner{
    return [self.firstTurn defeats:self.secondTurn] ? self.firstTurn : self.secondTurn;
}

-(RPSTurn*) loser{
    return [self.firstTurn defeats:self.secondTurn] ? self.secondTurn : self.firstTurn;
}
@end

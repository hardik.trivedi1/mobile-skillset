//
//  RPSController.m
//  RockPaperScissor
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "RPSController.h"
#import "RPSTurn.h"

@implementation RPSController

-(void) throwDown: (Move) playerMove{
    RPSTurn *playersTurn = [[RPSTurn alloc] initWithMove:playerMove];
    RPSTurn *computerTurn = [[RPSTurn alloc] init];
    
    self.game = [[RPSGame alloc] initWithFirstTurn:playersTurn secondTurn:computerTurn];
}

-(void) printResult{
    if (self.game.firstTurn.move == self.game.secondTurn.move){
        NSLog(@"It's a tie.");
    }else{
        NSLog(@"%@ defeats %@.",[[self.game winner] description],
          [[self.game loser] description]);
        if ([self.game.firstTurn defeats:self.game.secondTurn]){
            NSLog(@"You win!");
        }else{
            NSLog(@"You lost!!");
        }
    }
}


@end

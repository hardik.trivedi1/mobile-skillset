//
//  PhotoViewCollectionViewCell.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 06/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire
class PhotoViewCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        loadingIndicatorView.hidesWhenStopped = true
        loadingIndicatorView.startAnimating()
        photoImageView.layer.cornerRadius = 10
        photoImageView.clipsToBounds = true
    }
    
    func setPhoto(from url: String){
        Alamofire.request(url).responseData { response in
            print("Outer in")
            if let data = response.result.value {
                print("Inside")
                self.loadingIndicatorView.stopAnimating()
               self.photoImageView.image = UIImage(data: data)
            }
            print("Outer out")
        }
    }
}

//
//  HomeKitDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 19/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit
import CocoaLumberjack
class HomeKitDemoViewController: UIViewController {

    var homeManager = HMHomeManager()
    var accessoryBrowser = HMAccessoryBrowser()
    var accessories = [HMAccessory](){
        didSet{
            devicesTableView.reloadData()
        }
    }
    
    @IBOutlet weak var devicesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DDLog.add(DDTTYLogger.sharedInstance)
        setUpHome()
        setUpBrowser()
        setUpDevicesList()
    }

    func setUpHome(){
        homeManager.addHome(withName: "Heaven", completionHandler: { (home, error) in
            if error != nil{
                DDLogError("Error in adding home. \(String(describing: error))")
            }else{
                home?.addRoom(withName: "Living Room", completionHandler: { (room, error) in
                    if error != nil{
                        DDLogError("Error in adding room.")
                    }else{
                        
                    }
                })
                self.homeManager.updatePrimaryHome(home!, completionHandler: { (error) in
                    DDLogError("Error in making this home Primary.")
                })
            }
        })
    }

    func setUpBrowser(){
        accessoryBrowser.delegate = self
        accessoryBrowser.startSearchingForNewAccessories()
    }
    
    func setUpDevicesList(){
        devicesTableView.dataSource = self
        devicesTableView.delegate = self
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 64))
        let headerLabel = UILabel(frame: CGRect(x: 16, y: 8, width: view.frame.width/2, height: 56))
        headerLabel.text = "Available Devices"
        headerView.addSubview(headerLabel)
        let headerButton = UIButton(type: .system)
        headerButton.setTitle("Show Devices Added in Home", for: .normal)
        headerButton.frame = CGRect(x: headerLabel.frame.width - 16, y: 8, width: view.frame.width/2, height: 56)
        headerButton.addTarget(self, action: #selector(showHomeDevices), for: .touchUpInside)
        headerView.addSubview(headerButton)
        devicesTableView.tableHeaderView = headerView
    }
    
    @objc func showHomeDevices(){
        performSegue(withIdentifier: "ShowHomeDevices", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowHomeDevices"{
            if let vc = segue.destination as? HomeDevicesViewController{
                vc.homeAccessories = (homeManager.primaryHome?.accessories)!
            }
        }
    }
    

}
extension HomeKitDemoViewController: HMAccessoryBrowserDelegate{
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didFindNewAccessory accessory: HMAccessory) {
        accessories.append(accessory)
    }
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didRemoveNewAccessory accessory: HMAccessory) {
        DDLogInfo("Accessory removed.")
        accessories.remove(at: accessories.index(of: accessory)!)
    }
}
extension HomeKitDemoViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accessories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "deviceCell", for: indexPath)
        cell.textLabel?.text = accessories[indexPath.row].name
        cell.detailTextLabel?.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let room: HMRoom = homeManager.primaryHome?.rooms.first else {
            fatalError()
        }
        let actionsAlert = UIAlertController(title: accessories[indexPath.row].name, message: nil, preferredStyle: .actionSheet)
        let accessoryToBeAdded = accessories[indexPath.row]
        actionsAlert.addAction(UIAlertAction(title: "Add to Living room", style: .default, handler: { action in
            self.homeManager.primaryHome?.addAccessory(accessoryToBeAdded, completionHandler: { error in
                if error != nil{
                    print(error!)
                }else{
                    self.homeManager.primaryHome?.assignAccessory(accessoryToBeAdded, to: room, completionHandler: { error in
                        if error != nil{
                            print(error!)
                        }else{
                            DDLogInfo("Accessory added to \(room.name)")
                        }
                    })
                }
            })
        }))
        actionsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionsAlert, animated: true, completion: nil)
    }
}

//
//  FavouriteTabViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 08/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class FavouriteTabViewController: UIViewController {

    @IBOutlet weak var segmentedView: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        segmentedView.addTarget(self, action: #selector(doWhenSegmentSelected), for: .valueChanged)
    }

    @objc func doWhenSegmentSelected(){
        switch segmentedView.selectedSegmentIndex {
        case 0:
            view.backgroundColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
            break
        case 1:
            view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            break
        case 2:
            view.backgroundColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            break
        default:
            print("Nothing selected")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

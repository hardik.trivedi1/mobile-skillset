//
//  ReachabilityDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 13/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import SystemConfiguration
import SVProgressHUD
class ReachabilityDemoViewController: UIViewController {

    @IBOutlet weak var dataShowingLabel: UILabel!
    @IBOutlet weak var oneImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        dataShowingLabel.text = "Network status here ..."
        
    }

    @IBAction func showNetworkStatus(_ sender: UIButton) {
        var data = String()
        let reachability = SCNetworkReachabilityCreateWithName(nil, "www.google.com")
        var flags = SCNetworkReachabilityFlags()
        SCNetworkReachabilityGetFlags(reachability!, &flags)
        data += "Reachable : " + (flags.contains(.reachable) ? "Yes" : "No") + "\n"
        data += "Connection Required : " + (flags.contains(.connectionRequired) ? "Yes" : "No") + "\n"
        data += "Can Connect automatically : " + (flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic) ? "Yes" : "No") + "\n"
        data += "Can connect without user interaction : " + ((flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)) && !flags.contains(.interventionRequired) ? "Yes" : "No") + "\n"
        data += "Internet available : " + (isNetworkReachable(with: flags) ? "Yes" : "No") + "\n"
        
        dataShowingLabel.text = data
    }
    
    @IBAction func downloadOneImage(_ sender: UIButton) {
        SVProgressHUD.show(withStatus: "Downloading image...")
        DispatchQueue.global(qos: .userInitiated).async {
            let url = URL(string: "https://koenig-media.raywenderlich.com/uploads/2016/12/fake.png")
            URLSession(configuration: .default).dataTask(with: url!, completionHandler: {(data, response, error) in
                DispatchQueue.main.async {
                    SVProgressHUD.showSuccess(withStatus: "Download Complete")
                    SVProgressHUD.dismiss(withDelay: 2)
                    self.oneImageView.image = UIImage(data: data!)
                }
            }).resume()
        }
    }
    
    func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

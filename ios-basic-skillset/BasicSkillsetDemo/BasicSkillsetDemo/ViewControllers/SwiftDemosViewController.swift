//
//  SwiftDemosViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Crashlytics

class SwiftDemosViewController: UIViewController {

    @IBOutlet weak var firstXTextField: UITextField!
    @IBOutlet weak var firstYTextField: UITextField!
    @IBOutlet weak var secondXTextField: UITextField!
    @IBOutlet weak var secondYTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    var sumedUpCoordinate = Coordinate2D(x: 0, y: 0)
    typealias DeliveryOptions = ShippingOptions
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        AppDelegate.makeFirebaseLog(eventName: "SwiftDemosViewController",
                                    eventTitle: "SwiftDemos",
                                    eventDesrciption: "On swift demos.")
        let _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(makeUpdateWithTime(_:)), userInfo: ["data": "Do Update"], repeats: true)
    }
    
    @IBAction func serializeJSON(_ sender: UIButton) {
        let str = "{\"names\": [\"Bob\", \"Tim\", \"Tina\"]}"
        let data = str.data(using: String.Encoding.utf8, allowLossyConversion: false)!
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            if let names = json["names"] as? [String] {
                print(names)
            }
        } catch let error as NSError {
            print("Failed to load: \(error.localizedDescription)")
        }
    }
    
    @objc func makeUpdateWithTime(_ timer: Timer){
        if let data = timer.userInfo as? [String:String]{
            print(data["data"]!)
        }
    }
    
    @IBAction func addTwoCoordinates(_ sender: UIButton) {
        let firstCoordinate = Coordinate2D(x: Int(firstXTextField.text!)!,
                                           y: Int(firstYTextField.text!)!)
        let secondCoordinate = Coordinate2D(x: Int(secondXTextField.text!)!,
                                            y: Int(secondYTextField.text!)!)
        sumedUpCoordinate = firstCoordinate + secondCoordinate
        let sumAlert = UIAlertController(title: "Sum of Two Coordinates",
                                         message: "Sumed Coordinate is (x: \(sumedUpCoordinate.x),y: \(sumedUpCoordinate.y!)).",
                                            preferredStyle: .alert)
        sumAlert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        present(sumAlert, animated: true, completion: nil)
        let mirror = Mirror.init(reflecting: firstCoordinate)
        print(mirror.description + " :: firstCoordinate")
        print("Total members : " + String(mirror.children.count))
        mirror.children.forEach({child in
            print("Name : " + child.label! + "\tValue : " + String(child.value as! Int))
        })
        
        AppDelegate.makeFirebaseLog(eventName: "SwiftdemosViewController",
                                    eventTitle: "AddTwoCoordinates-func",
                                    eventDesrciption: "(x: \(firstCoordinate.x), y: \(firstCoordinate.y)) and (x: \(secondCoordinate.x), y: \(secondCoordinate.y)) are added. Result: (x: \(sumedUpCoordinate.x), y: \(sumedUpCoordinate.y))")
    }
    @IBAction func checkoutDeliveryModes(_ sender: UIButton) {
        let standardModes: Set = ["Standard","Second day","Next day"]
        let premiumModes: Set = ["Priority","Next day"]
        let deliveryModes = standardModes.union(premiumModes)
        var allModes = String()
        for mode in deliveryModes{
            allModes.append(mode + "\n")
        }
        var commonMode = String()
        let commonDeliveryModes = standardModes.intersection(premiumModes)
        for mode in commonDeliveryModes{
            commonMode.append(mode)
        }
        let deliveryModesAlert = UIAlertController(title: "Delivery Modes",
                                                   message: "All modes :\n\(allModes)Common modes :\n\(commonMode)", preferredStyle: .alert)
        deliveryModesAlert.addAction(UIAlertAction(title: "Okay",
                                                   style: .default,
                                                   handler: nil))
        present(deliveryModesAlert, animated: true, completion: nil)
    }
    @IBAction func makeCrash(_ sender: UIButton) {
        Crashlytics.sharedInstance().crash()
    }
    @IBAction func checkFreeDelivery(_ sender: UIButton) {
        let amount = Int(amountTextField.text!)!
        var freeOptions : DeliveryOptions = []
        if amount > 50{
            freeOptions.insert(.priority)
        }
        var deliveryTitle = "Free delivery "
        
        if freeOptions.contains(.priority){
            deliveryTitle.append("available.")
        }else{
            deliveryTitle.append("unavailable.")
        }
        let freeAlert = UIAlertController(title: deliveryTitle, message: nil, preferredStyle: .alert)
        freeAlert.addAction(UIAlertAction(title: "Okay",
                                          style: .default,
                                          handler: nil))
        present(freeAlert, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

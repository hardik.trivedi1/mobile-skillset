//
//  MqttDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 14/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CocoaMQTT
import Fabric
import Crashlytics

class MqttDemoViewController: UIViewController {

    @IBOutlet weak var messageShowingLabel: UILabel!
    @IBOutlet weak var messageTextField: UITextField!

    var mqtt: CocoaMQTT!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logEventOnFabric(name: "MQTTDemo", title: "MQTT-didLoad", description: "MQTTDemoVC didLoad")
        
        let clientID = "drnedwni"
        mqtt = CocoaMQTT(clientID: clientID, host: "m12.cloudmqtt.com", port: 14752)
        mqtt.username = "drnedwni"
        mqtt.password = "IeMqj97jHY7G"
        //mqtt.willMessage = CocoaMQTTWill(topic: "testTopic", message: "dieout")
        mqtt.delegate = self
        mqtt.keepAlive = 60
        mqtt.allowUntrustCACertificate = true
        mqtt.connect()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mqtt.didConnectAck = { mqtt, ack in
            if ack == .accept{
                mqtt.subscribe("testTopic")
                self.logEventOnFabric(name: "MQTTDemo", title: "MQTT-connection", description: "CloudMQTT connected.")
            }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mqtt.unsubscribe("testTopic")
    }
    @IBAction func publishMessage(_ sender: UIButton) {
        mqtt.publish("testTopic", withString: messageTextField.text!)
    }

    func logEventOnFabric(name: String, title: String, description: String){
        Answers.logCustomEvent(withName: name, customAttributes: [
            "title": title,
            "description": description])
    }
}

extension MqttDemoViewController: CocoaMQTTDelegate{
    func mqtt(_ mqtt: CocoaMQTT, didConnectAck ack: CocoaMQTTConnAck){

    }
    func mqtt(_ mqtt: CocoaMQTT, didPublishMessage message: CocoaMQTTMessage, id: UInt16){
        print("Message : \"\(message.string!)\" published.")
    }
    func mqtt(_ mqtt: CocoaMQTT, didPublishAck id: UInt16){
        
    }
    func mqtt(_ mqtt: CocoaMQTT, didReceiveMessage message: CocoaMQTTMessage, id: UInt16 ){
        print("Message : \"\(message.string!)\" received.")
        messageShowingLabel.text = "Message : \(message.string!)"
    }
    func mqtt(_ mqtt: CocoaMQTT, didSubscribeTopic topic: String){
        print("Subscribed to \(topic).")
        logEventOnFabric(name: "MQTTDemo", title: "Subscribed", description: "Subscribed to \(topic).")
    }
    func mqtt(_ mqtt: CocoaMQTT, didUnsubscribeTopic topic: String){
        print("Unsubscribed to \(topic).")
        logEventOnFabric(name: "MQTTDemo", title: "Unsubscribed", description: "Unsubscribed to \(topic).")
    }
    func mqttDidPing(_ mqtt: CocoaMQTT){
        
    }
    func mqttDidReceivePong(_ mqtt: CocoaMQTT){
        
    }
    func mqttDidDisconnect(_ mqtt: CocoaMQTT, withError err: Error?){
        print(err!)
    }
}

//
//  VersioningDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 07/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
class VersioningDemoViewController: UIViewController {

    let iOSVersions = ["iOS 3","iOS 4","iOS 5","iOS 6","iOS 7","iOS 8","iOS 9","iOS 10","iOS 11","iOS 12"]

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!
    @IBOutlet weak var chooseVersionButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        AppDelegate.makeFirebaseLog(eventName: "VersioningDemoViewController",
                                    eventTitle: "Versioning Demo",
                                    eventDesrciption: "On versioning demo.")
    }

    @IBAction func chooseiOSVersion(_ sender: UIButton) {
        let pickVersionAlert = UIAlertController(title: "Chhose iOS version", message: " ", preferredStyle: .actionSheet)
        let picker = UIPickerView(frame: CGRect(x: 8, y: 4, width: 380, height: 200))
        picker.dataSource = self
        picker.delegate = self
        pickVersionAlert.view.addSubview(picker)
        pickVersionAlert.addAction(UIAlertAction(title: "Done", style: .default, handler: {action in
            self.chooseVersionButton.setTitle(self.iOSVersions[picker.selectedRow(inComponent: 0)], for: .normal)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Version_info")
            fetchRequest.predicate = NSPredicate(format: "ios.version == %@", self.iOSVersions[picker.selectedRow(inComponent: 0)])
            do{
                let data = try context.fetch(fetchRequest) as! [VersionInfo]
                let line1 = "iOS version : " + (data[0].ios?.version)!
                let line2 = "\niOS release date : " + (data[0].ios?.release_date)!
                let line3 = "\nSwift version : " + (data[0].swift?.version)!
                let line4 = "\nSwift release data : " + (data[0].swift?.release_date)!
                let line5 = "\nWhat's new : " + data[0].whats_new!
                self.detailLabel.text = line1 + line2 + line3 + line4 + line5
                self.commentTextView.text = data[0].user_comment
            }catch let error{
                print(error)
            }
        }))
        pickVersionAlert.view.addConstraint(NSLayoutConstraint(item: pickVersionAlert.view,
                                                               attribute: .height,
                                                               relatedBy: .equal,
                                                               toItem: nil,
                                                               attribute: .notAnAttribute,
                                                               multiplier: 1,
                                                               constant: 250))
        present(pickVersionAlert, animated: true, completion: nil)
        
        AppDelegate.makeFirebaseLog(eventName: "VersioningDemoViewController",
                                    eventTitle: "chooseiOSVersion-func",
                                    eventDesrciption: "iOS version \(self.iOSVersions[picker.selectedRow(inComponent: 0)]) chosen. Data loaded from CoreData.")
    }
    @IBAction func moreInfo(_ sender: UIButton) {
        AppDelegate.makeFirebaseLog(eventName: "VersioningDemoViewController",
                                    eventTitle: "moreInfo-func",
                                    eventDesrciption: "moreInfo clicked and info will get loaded from Web.")
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension VersioningDemoViewController: UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return iOSVersions.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return iOSVersions[row]
    }
    
}

//
//  HomeDevicesViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 20/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit
import CocoaLumberjack
class HomeDevicesViewController: UIViewController {
    
    var homeAccessories = [HMAccessory]()
    var homeManager = HMHomeManager()
    
    @IBOutlet weak var devicesTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        devicesTableView.dataSource = self
        devicesTableView.delegate = self
        DDLog.add(DDTTYLogger.sharedInstance)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension HomeDevicesViewController: HMAccessoryDelegate{
    func accessory(_ accessory: HMAccessory, service: HMService, didUpdateValueFor characteristic: HMCharacteristic) {
        devicesTableView.reloadData()
        DDLogInfo("Characteristic updated.")
    }
}
extension HomeDevicesViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return homeAccessories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "homeDeviceCell", for: indexPath)
        cell.textLabel?.text = homeAccessories[indexPath.row].name
        homeAccessories[indexPath.row].delegate = self
        let services = homeAccessories[indexPath.row].services
        for service in services{
            for item in service.characteristics{
                let characteristic = item as HMCharacteristic
                DDLogVerbose("value \(String(describing: characteristic.value)) : \(String(describing: characteristic.metadata))")
                
                if characteristic.metadata?.format == HMCharacteristicMetadataFormatBool{
                    if let status = characteristic.value as? Bool{
                        if status{
                            cell.detailTextLabel?.text = "ON"
                        }else{
                            cell.detailTextLabel?.text = "OFF"
                        }
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let room: HMRoom = homeManager.primaryHome?.rooms.first else {
            fatalError()
        }
        var char = HMCharacteristic()
        let services = homeAccessories[indexPath.row].services
        for service in services{
            for item in service.characteristics{
                let characteristic = item as HMCharacteristic
                DDLogDebug("value \(String(describing: characteristic.value)) : \(String(describing: characteristic.metadata))")
                
                if characteristic.metadata?.format == HMCharacteristicMetadataFormatBool{
                    if let _ = characteristic.value as? Bool{
                        char = characteristic
                    }
                }
            }
        }
        let actionsAlert = UIAlertController(title: homeAccessories[indexPath.row].name, message: nil, preferredStyle: .actionSheet)
        actionsAlert.addAction(UIAlertAction(title: "Change status", style: .default, handler: { action in
            let alert = UIAlertController(title: "Change Status", message: nil, preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "ON", style: .default, handler: { action in
                char.writeValue(NSNumber(value: true), completionHandler: { error in
                    if error != nil{
                        print(error!)
                    }else{
                        DDLogInfo("Status changed.")
                    }
                })
            }))
            alert.addAction(UIAlertAction(title: "OFF", style: .destructive, handler: { action in
                char.writeValue(NSNumber(value: false), completionHandler: { error in
                    if error != nil{
                        print(error!)
                    }else{
                        DDLogInfo("Status changed.")
                    }
                })
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }))
        actionsAlert.addAction(UIAlertAction(title: "Remove from Living room", style: .default, handler: { action in
            self.homeManager.primaryHome?.removeAccessory(self.homeAccessories[indexPath.row], completionHandler: { error in
                if error != nil{
                    print(error!)
                }else{
                    DDLogInfo("Removed successfully from \(room.name)")
                }
            })
        }))
        actionsAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionsAlert, animated: true, completion: nil)
    }
}

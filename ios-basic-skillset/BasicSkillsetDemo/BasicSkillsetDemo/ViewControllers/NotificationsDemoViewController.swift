//
//  NotificationsDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 12/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import UserNotifications
class NotificationsDemoViewController: UIViewController {

    @IBOutlet weak var greetingsLabel: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(changeGreetingLabelText(notification:)), name: NSNotification.Name(rawValue: "greetings"), object: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        UNUserNotificationCenter.current().delegate = self
    }

    @IBAction func addLocalNotification(_ sender: UIButton) {
        UNUserNotificationCenter.current().getNotificationSettings { (notificationSettings) in
            switch notificationSettings.authorizationStatus {
                case .notDetermined:
                    UNUserNotificationCenter.current().requestAuthorization(options: [.alert,.sound,.badge],
                                                                            completionHandler: {granted , error in
                                                                                if granted{
                                                                                    self.scheduleLocalNotification()
                                                                                }else{
                                                                                    print("Not granted")
                                                                                }
                                                                            })
                case .authorized:
                    self.scheduleLocalNotification()
                    
                case .denied:
                    print("Application Not Allowed to Display Notifications")
            }
        }
    }
    private func scheduleLocalNotification(){
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()
        
        // Configure Notification Content
        notificationContent.title = "Notifications Demo"
        notificationContent.subtitle = "Local Notifications"
        notificationContent.body = "This is local notifications demo where one notification will be triggered in 10 sec."
        
        // Add Trigger
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 10.0, repeats: false)
        
        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "basic_skillset_local_notification", content: notificationContent, trigger: notificationTrigger)
        
        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }
    
    @IBAction func sendGreetings(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "greetings"), object: self, userInfo: ["greeting" : "Hello there!!!"])
    }
    
    @objc func changeGreetingLabelText(notification: Notification){
        if let _ = notification.object as? NotificationsDemoViewController{
            if let info = notification.userInfo{
                greetingsLabel.text = info["greeting"] as? String
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NotificationsDemoViewController: UNUserNotificationCenterDelegate{
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.badge])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        if response.notification.request.identifier == "basic_skillset_local_notification"{
            print(response.notification.request.content)
        }
        completionHandler()
    }
}

//
//  FirebaseDataDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 21/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import GoogleSignIn

class FirebaseDataDemoViewController: UIViewController {

    @IBOutlet weak var greetingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.hidesBackButton = true
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(gotToBack))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Sign Out", style: .done, target: self, action: #selector(signOut))
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let user = Auth.auth().currentUser {
            greetingLabel.text = "Welcome \((user.displayName)!)"
        }
    }
    @objc func gotToBack(){
        performSegue(withIdentifier: "alreadySignedInGoTo", sender: self)
    }

    @objc func signOut(){
        do{
            try Auth.auth().signOut()
            performSegue(withIdentifier: "goToSignIn", sender: self)
        }catch let error{
            print(error.localizedDescription)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

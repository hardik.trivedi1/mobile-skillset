//
//  ContainerDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 13/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class ContainerDemoViewController: UIViewController {

    weak var labelContainerDelegate: LabelContainerDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func incrementValue(_ sender: UIButton) {
        labelContainerDelegate?.incrementValueInLabel()
    }
    
    @IBAction func decrementValue(_ sender: UIButton) {
        labelContainerDelegate?.decrementValueInLabel()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? LabelContainerViewController{
            labelContainerDelegate = vc
        }
    }
    

}


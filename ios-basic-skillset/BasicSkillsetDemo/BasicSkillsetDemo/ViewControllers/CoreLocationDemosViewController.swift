//
//  CoreLocationDemosViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 18/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
class CoreLocationDemosViewController: UIViewController {

    @IBOutlet weak var mapView: MKMapView!
    let geocoder = CLGeocoder()
    var currentLocation = CLLocationCoordinate2D()
    var prevLocation = CLLocationCoordinate2D()
    
    var geotifications = [Location]()
    var locationManager: CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        mapView.delegate = self
        mapView.isZoomEnabled = true
        setUpMapForAddingGeoFence()
        setUpMapForHeading()
    }

    func setUpMapForAddingGeoFence(){
        let longTapGesture = UILongPressGestureRecognizer(target: self, action: #selector(setupMarkerAndAdd(recognizer:)))
        longTapGesture.minimumPressDuration = 1.0
        mapView.addGestureRecognizer(longTapGesture)
    }
    @objc func setupMarkerAndAdd(recognizer: UIGestureRecognizer){
        if recognizer.state == .began{
            return
        }
        let touchPoint = recognizer.location(in: mapView)
        let center = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        mapView.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = center
        mapView.addAnnotation(annotation)
    }
    
    func setUpMapForHeading(){
        if CLLocationManager.headingAvailable(){
            locationManager.headingFilter = 1
            locationManager.startUpdatingHeading()
        }
    }
    func showAlert(withTitle title: String,message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func region(withLocation location: Location) -> CLCircularRegion {
        let region = CLCircularRegion(center: location.coordinates, radius: CLLocationDistance(location.area), identifier: location.name)
        region.notifyOnEntry = true
        region.notifyOnExit = true
        return region
    }
    
    func startMonitoring(location: Location) {
        if !CLLocationManager.isMonitoringAvailable(for: CLCircularRegion.self) {
            showAlert(withTitle:"Error", message: "Geofencing is not supported on this device!")
            return
        }
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            showAlert(withTitle:"Warning", message: "Your geotification is saved but will only be activated once you grant us permission to access the device location.")
        }
        let region = self.region(withLocation: location)
        locationManager.startMonitoring(for: region)
        
        let circle = MKCircle(center: location.coordinates, radius: CLLocationDistance(location.area))
        mapView.add(circle)
    }
    
    func stopMonitoring(location: Location) {
        for region in locationManager.monitoredRegions {
            guard let circularRegion = region as? CLCircularRegion, circularRegion.identifier == location.name else { continue }
            locationManager.stopMonitoring(for: circularRegion)
        }
        for overlay in mapView.overlays{
            if overlay.coordinate.latitude == location.coordinates.latitude &&
                overlay.coordinate.longitude == location.coordinates.longitude{
                mapView.remove(overlay)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CoreLocationDemosViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            print(location)
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            currentLocation = center
            mapView.setRegion(region, animated: true)
            
            for annotation in mapView.annotations{
                if annotation.coordinate.latitude == prevLocation.latitude &&
                    annotation.coordinate.longitude == prevLocation.longitude{
                    mapView.removeAnnotation(annotation)
                }
            }
            
            let annotation = MKPointAnnotation()
            annotation.coordinate = center
            
            mapView.addAnnotation(annotation)
            
            prevLocation = currentLocation
        }
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier \(region!.identifier)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region, message: "Entered")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region, message: "Left")
        }
    }
    
    func handleEvent(forRegion: CLRegion, message: String){
        if UIApplication.shared.applicationState == .active{
            showAlert(withTitle: "GeoFence Triggered", message: message)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateHeading newHeading: CLHeading) {
        print(newHeading.trueHeading)
    }
}
extension CoreLocationDemosViewController: MKMapViewDelegate{

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "location"
        
        if annotation is Location {
            var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            
            if annotationView == nil {
                annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
                annotationView!.canShowCallout = true
                let btn = UIButton(type: .detailDisclosure)
                annotationView!.rightCalloutAccessoryView = btn
            } else {
                annotationView!.annotation = annotation
            }
            
            return annotationView
        }
        return nil
    }
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let location = CLLocation(latitude: (view.annotation?.coordinate.latitude)!,
                                  longitude: (view.annotation?.coordinate.longitude)!)
        geocoder.reverseGeocodeLocation(location, completionHandler: { (placemarks,error) in
            if let placemark = placemarks?.first{
                if self.currentLocation.latitude == location.coordinate.latitude &&
                    self.currentLocation.longitude == location.coordinate.longitude{
                    let currentAddrAlert = UIAlertController(title: "Your Current Location", message: placemark.compactAddress!, preferredStyle: .alert)
                    currentAddrAlert.addAction(UIAlertAction(title: "Okay", style: .default, handler: nil))
                    self.present(currentAddrAlert, animated: true, completion: nil)
                    print(placemark.compactAddress!)
                }else{
                    let locationToBeMonitored = Location()
                    locationToBeMonitored.name = "Geofence"
                    locationToBeMonitored.area = 100
                    locationToBeMonitored.coordinates = location.coordinate
                    let currentAddrAlert = UIAlertController(title: "Your Current Location", message: placemark.compactAddress!, preferredStyle: .actionSheet)
                    currentAddrAlert.addAction(UIAlertAction(title: "Add Geofence", style: .default, handler: {action in
                        self.startMonitoring(location: locationToBeMonitored)
                    }))
                    currentAddrAlert.addAction(UIAlertAction(title: "Remove Geofence", style: .destructive, handler: {action in
                        self.stopMonitoring(location: locationToBeMonitored)
                    }))
                    currentAddrAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    self.present(currentAddrAlert, animated: true, completion: nil)
                    print(placemark.compactAddress!)
                }
            }
        })
    print(view.reuseIdentifier!)
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if let circleOverlay = overlay as? MKCircle {
            let circleRenderer = MKCircleRenderer(overlay: circleOverlay)
            circleRenderer.fillColor = .black
            circleRenderer.alpha = 0.1
            
            return circleRenderer
        }
        return MKOverlayRenderer()
    }
}
class Location{
    var name = String()
    var area = Int()
    var coordinates: CLLocationCoordinate2D!
}
extension CLPlacemark {
    
    var compactAddress: String? {
        if let name = name {
            var result = name
            
            if let street = thoroughfare {
                result += ", \(street)"
            }
            
            if let city = locality {
                result += ", \(city)"
            }
            
            if let country = country {
                result += ", \(country)"
            }
            
            return result
        }
        
        return nil
    }
    
}

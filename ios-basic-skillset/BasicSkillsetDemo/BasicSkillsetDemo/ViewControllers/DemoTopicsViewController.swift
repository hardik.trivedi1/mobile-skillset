//
//  ViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Firebase
import AFNetworking
import Social

class DemoTopicsViewController: UIViewController {

    @IBOutlet weak var notificationsButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange({ status in
            print("\(AFStringFromNetworkReachabilityStatus(status))")
        })
        AFNetworkReachabilityManager.shared().startMonitoring()
        NotificationCenter.default.addObserver(self, selector: #selector(changeLabelText(notification:)), name: NSNotification.Name(rawValue: "greetings"), object: nil)
        notificationsButton.addObserver(self, forKeyPath: "notiButton", options: [.new,.old], context: nil)
        
        AppDelegate.makeFirebaseLog(eventName: "DemoTopicsViewController", eventTitle: "DemoTopics", eventDesrciption: "On viewController where All topics of app are listed.")
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "notiButton"{
            Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(resetButton), userInfo: nil, repeats: false)
        }
    }
    
    @IBAction func composeFacebookPost(_ sender: UIButton) {
        if let vc = SLComposeViewController(forServiceType: SLServiceTypeFacebook) {
            vc.setInitialText("Look at this great picture!")
            vc.add(URL(string: "https://www.hackingwithswift.com"))
            present(vc, animated: true)
        }
    }
    
    @objc func resetButton(){
        notificationsButton.setTitle("Notifications", for: .normal)
    }
    
    @IBAction func showAFNetworking(_ sender: UIButton) {
        performSegue(withIdentifier: "ShowAFNetworking", sender: self)
    }
    @objc func changeLabelText(notification: Notification){
        print("Inside")
        if let _ = notification.object as? NotificationsDemoViewController{
            if let info = notification.userInfo{
                notificationsButton.setTitle(info["greeting"] as? String, for: .normal)
                print("Notified")
            }
        }
    }
    
    @IBAction func alreadySignedIn(segue: UIStoryboardSegue){
        
    }
}


//
//  MostViewedTabViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 08/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class MostViewedTabViewController: UIViewController {

    @IBOutlet weak var dateShowLabel: UILabel!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var sliderDataShowingLabel: UILabel!
    @IBOutlet weak var stepperDataShowingLabel: UILabel!
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var dayOrNightLabel: UILabel!
    
    var sliderData = "0.0"{
        didSet{
            sliderDataShowingLabel.text = "Slider value : \(sliderData)"
        }
    }
    var stepperData = "0"{
        didSet{
            stepperDataShowingLabel.text = "Count : \(stepperData)"
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        datePicker.datePickerMode = .date
        sliderDataShowingLabel.text = "Slider value : \(sliderData)"
    }
  
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if sender.isOn{
            view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }else{
            view.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        stepperDataShowingLabel.text = ""
        stepperData = String(sender.value)
    }
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        sliderDataShowingLabel.text = ""
        sliderData = String(sender.value)
    }
    @IBAction func doneChoosingDate(_ sender: UIButton) {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd,yyyy"
        dateShowLabel.text = dateFormat.string(from: datePicker.date)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  PhotoCollectionViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 06/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Alamofire

class PhotoCollectionViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    @IBOutlet weak var collectioView: UICollectionView!
    
    var photoURLs = [String](){
        didSet{
            collectioView.reloadData()
            AppDelegate.makeFirebaseLog(eventName: "PhotosCollectionViewController",
                                        eventTitle: "photoURLs-didSet",
                                        eventDesrciption: "All photos are downloaded and set.")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.makeFirebaseLog(eventName: "PhotoCollectionViewController",
                                    eventTitle: "PhotosCollection",
                                    eventDesrciption: "On photos collection,downloading photos.")
        
        collectioView.dataSource = self
        collectioView.delegate = self
        loadCollectionData()
    }
    func loadCollectionData(){
        Alamofire.request("https://api.unsplash.com/photos/?client_id=f27331d0163425ef472bded1bf3281bad75b8748ccf71f53d58d91d5d8f80dc4&per_page=30").responseJSON { response in
            if let json = response.result.value {
                if let photoArray = json as? [[String:Any]]{
                    for eachPhoto in photoArray{
                        let actualLink = eachPhoto["urls"] as! [String:Any]
                        self.photoURLs.append(actualLink["regular"] as! String)
                    }
                }
            }
        }
        
        AppDelegate.makeFirebaseLog(eventName: "PhotosCollectionViewController",
                                    eventTitle: "loadCollectionData-func",
                                    eventDesrciption: "Data for photos loaded.")
    }
    // MARK: - Collection view methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 30
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectioView.dequeueReusableCell(withReuseIdentifier: "CollectionViewCell", for: indexPath) as! PhotoViewCollectionViewCell
                if photoURLs.count > 0{
                    cell.setPhoto(from: photoURLs[indexPath.row])
                }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

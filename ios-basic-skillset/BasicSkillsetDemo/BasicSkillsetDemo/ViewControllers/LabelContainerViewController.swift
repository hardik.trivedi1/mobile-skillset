//
//  LabelContainerViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 13/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class LabelContainerViewController: UIViewController {

    var number: Int = 0{
        didSet{
            dataShowingLabel.text = "Count : \(number)"
        }
    }
    @IBOutlet weak var dataShowingLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        dataShowingLabel.text = "Count : 0"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension LabelContainerViewController: LabelContainerDelegate{
    func incrementValueInLabel() {
        number += 1
    }
    
    func decrementValueInLabel() {
        number -= 1
    }
    
    
}

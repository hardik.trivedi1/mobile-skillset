//
//  MoreInfoWebViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 08/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import WebKit
class MoreInfoWebViewController: UIViewController {

    var pageLoadingProgressView: UIProgressView!
    @IBOutlet weak var webView: WKWebView!
    
    @IBOutlet var barButtons: [UIBarButtonItem]!
    override func viewDidLoad() {
        super.viewDidLoad()
        initProgressView()
        webView.load(URLRequest(url: URL(string: "https://en.wikipedia.org/wiki/IOS_version_history")!))
        webView.addObserver(self, forKeyPath: #keyPath(WKWebView.estimatedProgress), options: .new, context: nil)
        navigationController?.setToolbarHidden(false, animated: true)
        toolbarItems = barButtons
        
        AppDelegate.makeFirebaseLog(eventName: "MoreInfoWebViewController",
                                    eventTitle: "More Info for iOS Versions",
                                    eventDesrciption: "Opening from wikipedia.")
    }
    private func initProgressView(){
        let navBounds = navigationController?.navigationBar.bounds
        pageLoadingProgressView = UIProgressView(frame: CGRect(x: 0,
                                                               y: (navBounds?.height)! - 2,
                                                               width: (navBounds?.width)!,
                                                               height: 2))
        navigationController?.navigationBar.addSubview(pageLoadingProgressView)
        pageLoadingProgressView.progressViewStyle = .bar
        pageLoadingProgressView.progress = 0.0
    }
    @IBAction func goBack(_ sender: UIBarButtonItem) {
        if webView.canGoBack{
            webView.goBack()
        }
    }
    @IBAction func goForward(_ sender: UIBarButtonItem) {
        if webView.canGoForward{
            webView.goForward()
        }
    }
    @IBAction func refreshWebView(_ sender: UIBarButtonItem) {
        pageLoadingProgressView.isHidden = false
        webView.load(URLRequest(url: webView.url!))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MoreInfoWebViewController: WKUIDelegate{
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        view = webView
    }
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            pageLoadingProgressView.setProgress(Float(webView.estimatedProgress), animated: true)
            if webView.estimatedProgress == 1.0{
                pageLoadingProgressView.isHidden = true
            }else{
                pageLoadingProgressView.isHidden = false
            }
        }
    }
    
}

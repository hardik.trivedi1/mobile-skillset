//
//  BackgroundModesViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 13/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class BackgroundModesViewController: UIViewController {

    var  backgroundTask: UIBackgroundTaskIdentifier = UIBackgroundTaskInvalid
    
    var number: Int = 0{
        didSet{
            switch UIApplication.shared.applicationState {
            case .active:
                dataShowingLabel.text = "Count : " + String(number)
            case .background:
                print("Count : " + String(number))
            case .inactive:
                break
            }
        }
    }
    var timer: Timer?
    @IBOutlet weak var dataShowingLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        dataShowingLabel.text = "Count : " + String(number)
    }
    
    @IBAction func startProcess(_ sender: UIButton) {
        timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(updateNumber), userInfo: nil, repeats: true)
        registerBackgroundTask()
    }
    @IBAction func stopProcess(_ sender: UIButton) {
        timer?.invalidate()
        timer = nil
        endBackgroundTask()
    }
    
    private func registerBackgroundTask(){
        backgroundTask = UIApplication.shared.beginBackgroundTask(expirationHandler: { [weak self] in
            self?.endBackgroundTask()
            })
    }
    private func endBackgroundTask(){
        UIApplication.shared.endBackgroundTask(backgroundTask)
        backgroundTask = UIBackgroundTaskInvalid
        print("Task ended.")
    }
    @objc func updateNumber(){
        number = number + 1
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  AFNetworkingViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 18/09/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import AFNetworking

class AFNetworkingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        createDownloadTask()
    }

    private func createDownloadTask(){
        let configurations = URLSessionConfiguration.default
        let manager = AFURLSessionManager(sessionConfiguration: configurations)
        let url = URL(string: "https://koenig-media.raywenderlich.com/uploads/2016/12/fake.png")
        let request = URLRequest(url: url!)
        let task = manager.downloadTask(with: request, progress: { progress in
            print("\(progress)")
        }, destination: nil, completionHandler: { (response,url,error) in
            if error == nil{
                print(response)
            }else{
                print(error?.localizedDescription ?? "Error")
            }
        })
        task.resume()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

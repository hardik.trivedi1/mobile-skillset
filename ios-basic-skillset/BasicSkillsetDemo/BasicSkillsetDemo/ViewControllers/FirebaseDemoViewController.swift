//
//  FirebaseDemoViewController.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 21/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FirebaseAuth
import SVProgressHUD

class FirebaseDemoViewController: UIViewController {

    
    @IBOutlet weak var signInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self

        Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user != nil{
                self.performSegue(withIdentifier: "FirebaseSignedIn", sender: self)
            }else{
                SVProgressHUD.showInfo(withStatus: "Please Sign-In.")
            }
        })
    }


    @IBAction func signInGoogle(_ sender: GIDSignInButton) {

    }
    
    @IBAction func goToSignIn(segue: UIStoryboardSegue){
        
    }
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
   
}
extension FirebaseDemoViewController: GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error.localizedDescription)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        print(credential.description)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
        }
    }
    
    
}
extension FirebaseDemoViewController: GIDSignInUIDelegate{
    
}

//
//  Coordinate2D.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation

class Coordinate2D: NSObject{
    var x: Int = 0
    var y: Int!
    init(x: Int,y: Int){
        self.x = x
        self.y = y
    }
    static func +(first: Coordinate2D,second: Coordinate2D) -> Coordinate2D{
        return Coordinate2D(x: first.x + second.x,
                            y: first.y + second.y)
    }
    deinit {
        print("Coordinate (x: \(x),y: \(y!)) deallocated.")
    }
}

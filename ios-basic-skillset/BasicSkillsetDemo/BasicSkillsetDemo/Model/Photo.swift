//
//  Photo.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 06/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation

struct Photo: Codable{
    var raw: String
    var full: String
    var regular: String
    var small: String
    var medium: String
    var large: String
    var thumb: String
}


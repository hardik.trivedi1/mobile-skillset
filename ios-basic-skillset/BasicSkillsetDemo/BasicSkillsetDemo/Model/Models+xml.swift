//
//  Models+xml.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 07/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation

class Info{
    var whatsNew =  String()
    var userComment = String()
    var iOS = Version()
    var swift = Version()
}
class Version{
    var version = String()
    var releaseDate = String()
}

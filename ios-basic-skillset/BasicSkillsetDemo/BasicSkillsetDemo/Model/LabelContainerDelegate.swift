//
//  LabelContainerDelegate.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 13/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation

protocol LabelContainerDelegate: class {
    func incrementValueInLabel()
    func decrementValueInLabel()
}

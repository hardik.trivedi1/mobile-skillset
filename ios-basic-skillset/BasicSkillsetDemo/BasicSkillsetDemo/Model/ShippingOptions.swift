//
//  ShippingOptions.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation

struct ShippingOptions: OptionSet {
    let rawValue: Int
    
    static let nextDay    = ShippingOptions(rawValue: 1 << 0)
    static let secondDay  = ShippingOptions(rawValue: 1 << 1)
    static let priority   = ShippingOptions(rawValue: 1 << 2)
    static let standard   = ShippingOptions(rawValue: 1 << 3)
}

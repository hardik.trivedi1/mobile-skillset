//
//  AppDelegate.swift
//  BasicSkillsetDemo
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CoreData
import CoreLocation
import UserNotifications
import Firebase
import FirebaseAuth
import GoogleSignIn
import Fabric
import Crashlytics
import AFNetworking

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    var infos = [Info]()
    var tagName = String()
    var whatsNew =  String()
    var userComment = String()
    var versionI = String()
    var releaseDateI = String()
    var versionS = String()
    var releaseDateS = String()
    
    var locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool{
        AFNetworkReachabilityManager.shared().setReachabilityStatusChange({ status in
            print("You're offline!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        })
        AFNetworkReachabilityManager.shared().startMonitoring()
        let defaults = UserDefaults.standard
        let isPreloaded = defaults.bool(forKey: "isPreloaded")
        if !isPreloaded {
            preloadCoreData()
            defaults.set(true, forKey: "isPreloaded")
        }
        readData()
        
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: "id-basicskillsetdemo",
            AnalyticsParameterItemName: "BasicSkillsetDemo",
            AnalyticsParameterContentType: "Demo app"])
        
        Fabric.with([Crashlytics.self,Answers.self])
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
        
        if (urlComponents?.queryItems?.count)! > 0{
            let urlItems = (urlComponents?.queryItems)! as [NSURLQueryItem]
            if url.scheme == "basicdemo"{
                for item in urlItems{
                    print("Item name : " + item.name + "\tItem Value : " + item.value!)
                }
            }
        }
        return GIDSignIn.sharedInstance().handle(url,                                                 sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: [:])
    }
    
    class func makeFirebaseLog(eventName name: String,eventTitle title: String, eventDesrciption description: String){
        Analytics.logEvent(name, parameters: [
            "title": title,
            "description": description])
    }
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "BasicSkillsetDemo")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func preloadCoreData(){
        if let path = Bundle.main.url(forResource: "version_info", withExtension: "xml"){
            if let parser = XMLParser(contentsOf: path){
                parser.delegate = self
                parser.parse()
                let context = persistentContainer.viewContext
                for tempInfo in infos{
                    let info = VersionInfo(context: context)
                    info.user_comment = tempInfo.userComment
                    info.whats_new = tempInfo.whatsNew
                    let ios = VersionIos(context: context)
                    let swift = VersionSwift(context: context)
                    ios.version = tempInfo.iOS.version
                    ios.release_date = tempInfo.iOS.releaseDate
                    info.ios = ios
                    swift.version = tempInfo.swift.version
                    swift.release_date = tempInfo.swift.releaseDate
                    info.swift = swift
                    
                    do{
                        try context.save()
                    }catch{
                        print("Error")
                    }
                }
            }
        }
    }
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    func addData(){
        let context = persistentContainer.viewContext
                let info = VersionInfo(context: context)
                info.user_comment = "Hello there!"
                info.whats_new = "With this release, Apple did not drop support for any iOS devices. Therefore, iOS 9 was supported on the iPhone 4S onwards, iPod Touch (5th generation) onwards, the iPad 2 onwards, and the iPad Mini (1st generation) onwards."
                let ios = VersionIos(context: context)
                let swift = VersionSwift(context: context)
                ios.version = "iOS 9"
                ios.release_date = "Jun 8,2015"
                info.ios = ios
                swift.version = "Swift 4.1"
                swift.release_date = "Mar 29,2017"
                info.swift = swift
        
                do{
                    try context.save()
                }catch{
                    print("Error")
                }
    }
    func readData(){
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Version_info")
        do{
            let data = try context.fetch(request) as! [VersionInfo]
            print(String(data.count))
            for obj in data {
                
                print(obj)
            }
        }catch{
            print("Fetch error")
        }
    }
}
// MARK: - XML parsing things here
var ios = false
var swift = false
extension AppDelegate: XMLParserDelegate{
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        tagName =  elementName
        if elementName == "info"{
            whatsNew = String()
            userComment = String()
        }
        if elementName == "ios"{
            versionI = String()
            releaseDateI = String()
            ios = true
        }
        if elementName == "swift"{
            versionS = String()
            releaseDateS = String()
            swift = true
        }
    }
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "info"{
            let info = Info()
            info.whatsNew = whatsNew
            info.userComment = userComment
            info.iOS.version = versionI
            info.iOS.releaseDate = releaseDateI
            info.swift.version = versionS
            info.swift.releaseDate = releaseDateS
            infos.append(info)
        }
    }
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let data = string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        if !data.isEmpty{
            if tagName == "whats_new"{
                whatsNew += data
            }else if tagName == "user_comment"{
                userComment += data
            }else if tagName == "version"{
                if ios{
                    versionI += data
                }
                if swift{
                    versionS += data
                }
            }else if tagName == "release_date"{
                if ios{
                    releaseDateI += data
                    ios = false
                }
                if swift{
                    releaseDateS += data
                    swift = false
                }
            }
        }
    }
}
extension AppDelegate: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        if region is CLCircularRegion {
            handleEvent(forRegion: region)
        }
    }
    
    func handleEvent(forRegion region: CLRegion){
        print("Geofence triggered!!")
        if UIApplication.shared.applicationState == .active {
            
        } else {
            let notificationContent = UNMutableNotificationContent()
            
            // Configure Notification Content
            notificationContent.title = "Region Monitoring Demo"
            notificationContent.body = "Geofence triggered."
 
            // Create Notification Request
            let notificationRequest = UNNotificationRequest(identifier: "basic_skillset_local_notification", content: notificationContent, trigger: nil)
            
            // Add Request to User Notification Center
            UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                if let error = error {
                    print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                }
            }
        }
    }
}
extension AppDelegate: GIDSignInDelegate{
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print(error)
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
        print(credential.description)
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print(user.profile.email + " has disconected.")
    }
}

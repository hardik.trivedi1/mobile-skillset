//
//  BasicSkillsetDemoUITests.swift
//  BasicSkillsetDemoUITests
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import XCTest

class BasicSkillsetDemoUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testButtonActions() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        app.buttons["Swift Demos"].tap()
        app.buttons["Checkout delivery modes"].tap()
        app.alerts["Delivery Modes"].buttons["Okay"].tap()
        app.navigationBars["BasicSkillsetDemo.SwiftDemosView"].buttons["Basic Skillset Demos"].tap()
        app.buttons["UICollectionView Demo"].tap()
        app.otherElements.containing(.navigationBar, identifier:"BasicSkillsetDemo.PhotoCollectionView").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.swipeUp()
        app.collectionViews.children(matching: .cell).element(boundBy: 13).children(matching: .other).element.swipeDown()
        app.navigationBars["BasicSkillsetDemo.PhotoCollectionView"].buttons["Basic Skillset Demos"].tap()
        app.buttons["ContainerView Demo"].tap()
        
        let incrementButton = app.buttons["Increment"]
        incrementButton.tap()
        incrementButton.tap()
        
        let decrementButton = app.buttons["Decrement"]
        decrementButton.tap()
        decrementButton.tap()
        app.navigationBars["BasicSkillsetDemo.ContainerDemoView"].buttons["Basic Skillset Demos"].tap()
        app.buttons["UITabBar Demo"].tap()
        
        let app2 = app
        app2/*@START_MENU_TOKEN@*/.buttons["White"]/*[[".segmentedControls.buttons[\"White\"]",".buttons[\"White\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app2/*@START_MENU_TOKEN@*/.buttons["Green"]/*[[".segmentedControls.buttons[\"Green\"]",".buttons[\"Green\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app2/*@START_MENU_TOKEN@*/.buttons["Saffron"]/*[[".segmentedControls.buttons[\"Saffron\"]",".buttons[\"Saffron\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        app.tabBars.buttons["History"].tap()
        
        let element = app.otherElements.containing(.navigationBar, identifier:"UITabBar").children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element/*@START_MENU_TOKEN@*/.swipeLeft()/*[[".swipeUp()",".swipeLeft()"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/
        element.swipeLeft()
        app.navigationBars["UITabBar"].buttons["Basic Skillset Demos"].tap()
        
    }
    
}

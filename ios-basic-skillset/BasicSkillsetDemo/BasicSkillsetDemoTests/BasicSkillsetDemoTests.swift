//
//  BasicSkillsetDemoTests.swift
//  BasicSkillsetDemoTests
//
//  Created by Hardik Trivedi on 05/06/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import XCTest
import Alamofire
@testable import BasicSkillsetDemo

class BasicSkillsetDemoTests: XCTestCase {
    
    var one: Coordinate2D!
    var two: Coordinate2D!
    var controllerUT : PhotoCollectionViewController!
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        one = Coordinate2D(x: 1, y: 1)
        two = Coordinate2D(x: 2, y: 2)
        controllerUT = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "photoVC") as! PhotoCollectionViewController
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        one = nil
        two = nil
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testCoordinateSum(){
        let sum = one + two
        let shouldAnswer = Coordinate2D(x: 3, y: 3)
        XCTAssertEqual(sum.x, shouldAnswer.x, "Sum is not performing adequetly.")
    }
    
    func testAlamofireRequests(){
        let promise = expectation(description: "Status code: 200")
        Alamofire.request("https://api.unsplash.com/photos/?client_id=f27331d0163425ef472bded1bf3281bad75b8748ccf71f53d58d91d5d8f80dc4&per_page=30").responseJSON { response in
            if response.response?.statusCode == 200{
                promise.fulfill()
            }
            else{
                XCTFail("Status code: \(String(describing: response.response?.statusCode))")
            }
        }
        waitForExpectations(timeout: 10, handler: nil)
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            self.controllerUT.loadCollectionData()
        }
    }
    
}

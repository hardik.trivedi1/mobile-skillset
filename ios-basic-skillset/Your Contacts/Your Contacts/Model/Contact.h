//
//  Contact.h
//  Your Contacts
//
//  Created by Hardik Trivedi on 11/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Contact : NSObject

@property (nonatomic) NSData *profilePic;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *companyName;
@property (nonatomic) NSString *homePhoneNumber;
@property (nonatomic) NSString *workPhoneNumber;
@property (nonatomic) NSString *email;
@property (nonatomic) NSString *ringtone;
@property (nonatomic) NSString *street;
@property (nonatomic) NSString *city;
@property (nonatomic) NSString *state;
@property (nonatomic) NSString *pinCode;
@property (nonatomic) NSString *country;

-(instancetype) initWithImage:(NSData*)profilePic
                    firstName:(NSString*)firstName
                     lastName:(NSString*)lastName
                      comapny:(NSString*)companyName
                    homePhone:(NSString*)homePhoneNumber
                    workPhone:(NSString*)workPhoneNumber
                        email:(NSString*)email
                     ringtone:(NSString*)ringtone
                       street:(NSString*)street
                         city:(NSString*)city
                        state:(NSString*)state
                      pinCode:(NSString*)pinCode
                      country:(NSString*)country;
@end

//
//  Contact.m
//  Your Contacts
//
//  Created by Hardik Trivedi on 11/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "Contact.h"

@implementation Contact

-(instancetype) initWithImage:(NSData*)profilePic
                        firstName:(NSString*)firstName
                        lastName:(NSString*)lastName
                        comapny:(NSString*)companyName
                        homePhone:(NSString*)homePhoneNumber
                        workPhone:(NSString*)workPhoneNumber
                        email:(NSString*)email
                        ringtone:(NSString*)ringtone
                        street:(NSString*)street
                        city:(NSString*)city
                        state:(NSString*)state
                        pinCode:(NSString*)pinCode
                        country:(NSString*)country{
    _profilePic = profilePic;
    _firstName = firstName;
    _lastName = lastName;
    _companyName = companyName;
    _homePhoneNumber = homePhoneNumber;
    _workPhoneNumber = workPhoneNumber;
    _email = email;
    _ringtone = ringtone;
    _street = street;
    _city = city;
    _state = state;
    _pinCode = pinCode;
    _country = country;
    
    return self;
}
@end

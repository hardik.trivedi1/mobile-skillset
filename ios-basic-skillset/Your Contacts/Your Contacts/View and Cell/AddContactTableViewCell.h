//
//  AddContactTableViewCell.h
//  Your Contacts
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ProfilePicAndNamesView;

@interface AddContactTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet ProfilePicAndNamesView *profilePicAndNamesView;
@property (weak, nonatomic) IBOutlet UITextField *homePhoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *workPhoneNumberTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *streetTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *stateTextField;
@property (weak, nonatomic) IBOutlet UITextField *pinCodeTextField;


@end

//
//  TextImageButton.swift
//  Your Contacts
//
//  Created by Hardik Trivedi on 29/05/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class TextImageButton: UIView {

    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var imageButton: UIButton!
    @IBOutlet weak var textButton: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("TextImageButton", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    

}

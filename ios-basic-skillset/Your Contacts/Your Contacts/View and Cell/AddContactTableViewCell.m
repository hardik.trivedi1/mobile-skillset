//
//  AddContactTableViewCell.m
//  Your Contacts
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "AddContactTableViewCell.h"

@implementation AddContactTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  ProfilePicAndNamesView.swift
//  Your Contacts
//
//  Created by Hardik Trivedi on 29/05/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

@objc class ProfilePicAndNamesView: UIView {

    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak public var profilePicImageView: UIImageView!
    
    @IBOutlet weak var editImageButton: UIButton!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var companyNameTextField: UITextField!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit(){
        Bundle.main.loadNibNamed("ProfilePicAndNamesView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleHeight,.flexibleWidth]
    }
    override func draw(_ rect: CGRect) {
        profilePicImageView.layer.cornerRadius = profilePicImageView.frame.size.width / 2
        profilePicImageView.clipsToBounds = true
    }
    
    @IBAction func editProfilePic(_ sender: UIButton) {
        print("Inside view :: Edit button")
    }
    
}

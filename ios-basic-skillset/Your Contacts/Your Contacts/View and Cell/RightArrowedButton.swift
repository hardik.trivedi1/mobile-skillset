//
//  RightArrowedButton.swift
//  Your Contacts
//
//  Created by Hardik Trivedi on 22/05/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class RightArrowedButton: UIButton {

    
    override func draw(_ rect: CGRect) {
        // Drawing code
        let image = UIImageView(frame: CGRect(
            x: rect.size.width - 10,
            y: 8,
            width: 10,
            height: rect.size.height - 16))
        image.image = UIImage(imageLiteralResourceName: "RightArrow")
        
        self.addSubview(image)
    }
 

}

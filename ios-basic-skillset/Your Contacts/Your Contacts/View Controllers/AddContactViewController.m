//
//  AddContactViewController.m
//  Your Contacts
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "AddContactViewController.h"
#import "AddContactTableViewCell.h"
#import "Your_Contacts-Swift.h"
#import "AppDelegate.h"

@interface AddContactViewController ()
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) UIImageView *profileImageView;
@property (weak,nonatomic) UIButton *editProfilePicButton;
@property (weak, nonatomic) UITextField *firstNameTextField;
@property (weak, nonatomic) UITextField *lastNameTextField;
@property (weak, nonatomic) UITextField *companyNameTextField;
@property (weak, nonatomic) UITextField *homePhoneNumberTextField;
@property (weak, nonatomic) UITextField *workPhoneNumberTextField;
@property (weak, nonatomic) UITextField *emailTextField;
@property (weak, nonatomic) UITextField *streetTextField;
@property (weak, nonatomic) UITextField *cityTextField;
@property (weak, nonatomic) UITextField *stateTextField;
@property (weak, nonatomic) UITextField *pinCodeTextField;

@end

@implementation AddContactViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _tableView.dataSource = self;
    _tableView.delegate = self;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (_contact == nil){
        NSLog(@"Add Contact");
        self.navigationItem.title = @"Add Contact";
    }else{
        NSLog(@"Edit Contact");
        
        self.navigationItem.title = @"Edit Contact";
        
    }
}
- (IBAction)saveContact:(UIBarButtonItem *)sender {
    NSManagedObjectContext *context = [self managedObjectContext];
    
    if (_contact == nil){
        // Create a new managed object
        NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"Contacts" inManagedObjectContext:context];
        [newContact setValue:UIImagePNGRepresentation(_profileImageView.image) forKey:@"profile_image"];
        [newContact setValue:self.firstNameTextField.text forKey:@"first_name"];
        [newContact setValue:_lastNameTextField.text forKey:@"last_name"];
        [newContact setValue:_homePhoneNumberTextField.text forKey:@"home_phone"];
        [newContact setValue:_workPhoneNumberTextField.text forKey:@"work_phone"];
        [newContact setValue:_emailTextField.text forKey:@"email"];
        [newContact setValue: @"default" forKey:@"ringtone"];
        [newContact setValue:_companyNameTextField.text forKey:@"company"];
        [newContact setValue:_streetTextField.text forKey:@"street"];
        [newContact setValue:_cityTextField.text forKey:@"city"];
        [newContact setValue:_stateTextField.text forKey:@"state"];
        [newContact setValue:_pinCodeTextField.text forKey:@"pincode"];
        [newContact setValue: @"India" forKey:@"country"];
        
        NSError *error = nil;
        // Save the object to persistent store
        if (![context save:&error]) {
            NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Contacts"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"home_phone == %@ && email == %@",_contact.homePhoneNumber,_contact.email];
        request.predicate = predicate;
        NSArray *data = [context executeFetchRequest: request error:nil];
        if (data.count >= 1){
            NSManagedObject *objectToUpdate = data[0];
            [objectToUpdate setValue:UIImageJPEGRepresentation(_profileImageView.image, 1) forKey:@"profile_image"];
            [objectToUpdate setValue:_firstNameTextField.text forKey:@"first_name"];
            [objectToUpdate setValue:_lastNameTextField.text forKey:@"last_name"];
            [objectToUpdate setValue:_companyNameTextField.text forKey:@"company"];
            [objectToUpdate setValue:_homePhoneNumberTextField.text forKey:@"home_phone"];
            [objectToUpdate setValue:_workPhoneNumberTextField.text forKey:@"work_phone"];
            [objectToUpdate setValue:_emailTextField.text forKey:@"email"];
            [objectToUpdate setValue: @"default" forKey:@"ringtone"];
            [objectToUpdate setValue:_companyNameTextField.text forKey:@"company"];
            [objectToUpdate setValue:_streetTextField.text forKey:@"street"];
            [objectToUpdate setValue:_cityTextField.text forKey:@"city"];
            [objectToUpdate setValue:_stateTextField.text forKey:@"state"];
            [objectToUpdate setValue:_pinCodeTextField.text forKey:@"pincode"];
            [objectToUpdate setValue: @"India" forKey:@"country"];
            
            NSError *error = nil;
            // Save the object to persistent store
            if (![context save:&error]) {
                NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
            }
            [self performSegueWithIdentifier:@"ContactUpdated" sender:self];
        }
    }
}
- (IBAction)cancelAdding:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

#pragma mark - CoreData methods

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ContactUpdated"]){
        AddContactViewController *vc = segue.destinationViewController;
        vc.contact = [[Contact alloc] initWithImage:UIImageJPEGRepresentation(_profileImageView.image, 1)
                                          firstName:_firstNameTextField.text
                                           lastName:_lastNameTextField.text
                                            comapny:_companyNameTextField.text
                                          homePhone:_homePhoneNumberTextField.text
                                          workPhone:_workPhoneNumberTextField.text
                                              email:_emailTextField.text
                                           ringtone:@"Default"
                                             street:_streetTextField.text
                                               city:_cityTextField.text
                                              state:_stateTextField.text
                                            pinCode:_pinCodeTextField.text
                                            country:@"India"];
    }
}

#pragma mark - TableView methods

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    AddContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddContactProfileAndName" forIndexPath:indexPath];
    [self setVars:cell];
    if (_contact != nil){
        _profileImageView.image = [[UIImage alloc] initWithData:_contact.profilePic];
        _firstNameTextField.text = _contact.firstName;
        _lastNameTextField.text = _contact.lastName;
        _companyNameTextField.text = _contact.companyName;
        _homePhoneNumberTextField.text = _contact.homePhoneNumber;
        _workPhoneNumberTextField.text = _contact.workPhoneNumber;
        _emailTextField.text = _contact.email;
        _streetTextField.text = _contact.street;
        _cityTextField.text = _contact.city;
        _stateTextField.text = _contact.state;
        _pinCodeTextField.text = _contact.pinCode;
    }
    return cell;
}

-(void) setVars: (AddContactTableViewCell*) cell{
    _profileImageView = cell.profilePicAndNamesView.profilePicImageView;
    _editProfilePicButton = cell.profilePicAndNamesView.editImageButton;
    [_editProfilePicButton addTarget:self
                              action:@selector(editProfilePic)
                    forControlEvents:UIControlEventTouchUpInside];
    _firstNameTextField = cell.profilePicAndNamesView.firstNameTextField;
    _lastNameTextField = cell.profilePicAndNamesView.lastNameTextField;
    _companyNameTextField = cell.profilePicAndNamesView.companyNameTextField;
    _homePhoneNumberTextField = cell.homePhoneNumberTextField;
    _workPhoneNumberTextField = cell.workPhoneNumberTextField;
    _emailTextField = cell.emailTextField;
    _streetTextField = cell.streetTextField;
    _cityTextField = cell.cityTextField;
    _stateTextField = cell.stateTextField;
    _pinCodeTextField = cell.pinCodeTextField;
}
- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(void) editProfilePic{
    NSURL *imgUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://images.unsplash.com/photo-1431794062232-2a99a5431c6c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=13b58b0343d8efc06a88c55e843f624f&auto=format&fit=crop&w=750&q=80"]];
    
    dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
    dispatch_async(q, ^{
        /* Fetch the image from the server... */
        NSData *data = [NSData dataWithContentsOfURL:imgUrl];
        UIImage *img = [[UIImage alloc] initWithData:data];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [_profileImageView setImage:img];
        });
    });
}

@end

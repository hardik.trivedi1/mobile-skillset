//
//  AddContactViewController.h
//  Your Contacts
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "Contact.h"
@interface AddContactViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic) Contact *contact;
@end

//
//  ContactsListViewController.h
//  Your Contacts
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactsListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@end

//
//  ContactDetailViewController.m
//  Your Contacts
//
//  Created by Hardik Trivedi on 11/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "ContactDetailViewController.h"
#import "AddContactViewController.h"
#import "Your_Contacts-Swift.h"
#import <CoreData/CoreData.h>

@interface ContactDetailViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *profilePicImageView;
@property (weak, nonatomic) IBOutlet UILabel *contactNameLabel;

@property (weak, nonatomic) IBOutlet TextImageButton *callImageButton;
@property (weak, nonatomic) IBOutlet TextImageButton *messageImageButton;
@property (weak, nonatomic) IBOutlet TextImageButton *videoCallImageButton;
@property (weak, nonatomic) IBOutlet TextImageButton *emailImageButton;
@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@end

@implementation ContactDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [_callImageButton.imageButton setImage:[UIImage imageNamed:@"callButtonIcon"] forState:UIControlStateNormal];
    [_callImageButton.textButton setTitle:@"Call" forState:UIControlStateNormal];
    [_messageImageButton.imageButton setImage:[UIImage imageNamed:@"messageButtonIcon"] forState:UIControlStateNormal];
    [_messageImageButton.textButton setTitle:@"Message" forState:UIControlStateNormal];
    [_videoCallImageButton.imageButton setImage:[UIImage imageNamed:@"videoCallButtonIcon"] forState:UIControlStateNormal];
    [_videoCallImageButton.textButton setTitle:@"Video" forState:UIControlStateNormal];
    [_emailImageButton.imageButton setImage:[UIImage imageNamed:@"emailButtonIcon"] forState:UIControlStateNormal];
    [_emailImageButton.textButton setTitle:@"email" forState:UIControlStateNormal];
    
    _profilePicImageView.image = [[UIImage alloc] initWithData:_contact.profilePic];
    _contactNameLabel.text = [NSString stringWithFormat:@"%@ %@",_contact.firstName,_contact.lastName];
    _numberLabel.text = _contact.homePhoneNumber;
    _emailLabel.text = _contact.email;
}
- (IBAction)editContact:(UIButton *)sender {
    [self performSegueWithIdentifier:@"EditContact" sender:self];
}
- (IBAction)deleteContact:(UIButton *)sender {
    UIAlertController *deleteAlert = [UIAlertController alertControllerWithTitle:@"Delete Contact" message:@"Do you want to delete this contact?" preferredStyle:UIAlertControllerStyleAlert];
    [deleteAlert addAction:[UIAlertAction actionWithTitle:@"Delete" style:UIAlertActionStyleDestructive handler:^(UIAlertAction* action){
        NSManagedObjectContext *context = [self managedObjectContext];
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"Contacts"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"home_phone == %@ && email == %@",_contact.homePhoneNumber,_contact.email];
        request.predicate = predicate;
        NSBatchDeleteRequest *deleteRequest = [[NSBatchDeleteRequest alloc] initWithFetchRequest:request];
        [context executeRequest:deleteRequest error:nil];
        [self performSegueWithIdentifier:@"ContactDeleted" sender:self];
    }]];
    [deleteAlert addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil]];
    [self presentViewController:deleteAlert animated:YES completion:nil];
}

-(IBAction)conatctUpdated:(UIStoryboardSegue*) segue{
    
}

#pragma mark - CoreData methods

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"EditContact"]){
        AddContactViewController *vc = segue.destinationViewController;
        vc.contact = _contact;
    }
}


@end

//
//  ContactsListViewController.m
//  Your Contacts
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "ContactsListViewController.h"
#import "Contact.h"
#import "ContactDetailViewController.h"
#import <CoreData/CoreData.h>
@interface ContactsListViewController ()
@property (weak, nonatomic) IBOutlet UITableView *contactsTableView;
@property (nonatomic) NSMutableArray<Contact*> *contacts;
@end

@implementation ContactsListViewController

#pragma ViewControllers methods

- (void)viewDidLoad {
    [super viewDidLoad];
    _contacts = [[NSMutableArray<Contact*> alloc] init];
    
    _contactsTableView.dataSource = self;
    _contactsTableView.delegate = self;
    
    
    [_contactsTableView setTableFooterView:[[UIView alloc] init]];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self loadDataForList];
}

- (void) loadDataForList{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Contacts"];
    NSArray *data = [managedObjectContext executeFetchRequest:fetchRequest error:nil];
    if ([data count] == 0){
        NSLog(@"No data available.");
    }else{
        [_contacts removeAllObjects];
        for (NSObject *object in data) {
            [_contacts addObject:[[Contact alloc] initWithImage:[object valueForKey:@"profile_image"]
                                                      firstName:[object valueForKey:@"first_name"]
                                                       lastName:[object valueForKey:@"last_name"]
                                                        comapny:[object valueForKey:@"company"]
                                                      homePhone:[object valueForKey:@"home_phone"]
                                                      workPhone:[object valueForKey:@"work_phone"]
                                                          email:[object valueForKey:@"email"]
                                                       ringtone:[object valueForKey:@"ringtone"]
                                                         street:[object valueForKey:@"street"]
                                                           city:[object valueForKey:@"city"]
                                                          state:[object valueForKey:@"state"]
                                                        pinCode:[object valueForKey:@"pincode"]
                                                        country:[object valueForKey:@"country"]]];
        }
    }
    [_contactsTableView reloadData];
}

#pragma mark - CoreData methods

- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    return context;
}

#pragma TableView methods
- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactListCell" forIndexPath:indexPath];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ %@",_contacts[indexPath.row].firstName,_contacts[indexPath.row].lastName];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _contacts.count;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self performSegueWithIdentifier:@"ShowContact" sender:self];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier  isEqual: @"ShowContact"]){
        ContactDetailViewController *vc = segue.destinationViewController;
        NSInteger index = _contactsTableView.indexPathForSelectedRow.row;
        vc.contact = [[Contact alloc] initWithImage:_contacts[index].profilePic
                                          firstName:_contacts[index].firstName
                                           lastName:_contacts[index].lastName
                                            comapny:_contacts[index].companyName
                                          homePhone:_contacts[index].homePhoneNumber
                                          workPhone:_contacts[index].workPhoneNumber
                                              email:_contacts[index].email
                                           ringtone:_contacts[index].ringtone
                                             street:_contacts[index].street
                                               city:_contacts[index].city
                                              state:_contacts[index].state
                                            pinCode:_contacts[index].pinCode
                                            country:_contacts[index].country];
    }
}

-(IBAction)conatctDeleted:(UIStoryboardSegue*) segue{
    
}

@end

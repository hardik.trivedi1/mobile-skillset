//
//  ContactDetailViewController.h
//  Your Contacts
//
//  Created by Hardik Trivedi on 11/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface ContactDetailViewController : UIViewController

@property (nonatomic) Contact *contact;
@end

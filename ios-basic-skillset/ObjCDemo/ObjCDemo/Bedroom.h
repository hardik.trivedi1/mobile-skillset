//
//  Bedroom.h
//  ObjCDemo
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
//@class HotTub;
#import "ObjCDemo-Swift.h"

typedef NS_ENUM(NSInteger,Direction){
    North,
    South,
    East,
    West
};
@interface Bedroom : NSObject

@property (nonatomic) BOOL hasPrivateBath;
@property (nonatomic) Direction directionWindowFaces;
@property (nonatomic) HotTub *hotTub;
@end

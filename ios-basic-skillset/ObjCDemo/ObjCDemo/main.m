//
//  main.m
//  ObjCDemo
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#include "House.h"
#import "ObjCDemo-Swift.h"
#define ADDRESS "123 Hudson Bay"


@interface NSString(GreetMessage)
+(NSString *) putGreetMessage:(NSString*) name;
@end

@implementation NSString(GreetMessage)

+(NSString *) putGreetMessage:(NSString*) name{
    return [NSString stringWithFormat:@"Hello %@",name];
}

@end

#pragma mark - Dynamic binding

@interface Square:NSObject {
    float area;
}

- (void)calculateAreaOfSide:(CGFloat)side;
- (void)printArea;
@end

@implementation Square
- (void)calculateAreaOfSide:(CGFloat)side {
    area = side * side;
}

- (void)printArea {
    NSLog(@"The area of square is %f",area);
}

@end

@interface Rectangle:NSObject {
    float area;
}

- (void)calculateAreaOfLength:(CGFloat)length andBreadth:(CGFloat)breadth;
- (void)printArea;
@end

@implementation  Rectangle

- (void)calculateAreaOfLength:(CGFloat)length andBreadth:(CGFloat)breadth {
    area = length * breadth;
}

- (void)printArea {
    NSLog(@"The area of Rectangle is %f",area);
}

@end

#pragma mark - Main method

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        House *house = [[House alloc] init];
        NSMutableString *address = [[NSMutableString alloc] initWithString:@ADDRESS];
        house.address = address;
        house.firstBedroom = [[Bedroom alloc] init];
        house.firstBedroom.hasPrivateBath = true;
        house.firstBedroom.hotTub = [[HotTub alloc] initWithTemprature:50];
        NSLog(@"Address : %@\nFirst bedroom hotTub temp: %d",house.address,(int)house.firstBedroom.hotTub.temprature);
        [house.firstBedroom.hotTub heatUp];
        NSLog(@"Address : %@\nFirst Bedroom hotTub Temp After heatUp: %d",house.address,(int)house.firstBedroom.hotTub.temprature);
        struct Family f;
        f.familyType = @"Indian";
        f.numberOfMembers = 4;
        house.family = f;
        NSLog(@"Family\nType : %@\nMembers : %d",house.family.familyType,house.family.numberOfMembers);
        NSLog(@"Today's date : %s",__DATE__);
        
        NSNumber *a = [NSNumber numberWithFloat:10.5];
        NSNumber *b = [NSNumber numberWithFloat:10.0];
        NSNumber *result = [house multiplyA:a withB:b];
        NSString *resultString = [result stringValue];
        NSLog(@"The product is %@",resultString);
        
        NSLog(@"%@", [NSString putGreetMessage:@"Hardik"]);
        
        Square *square = [[Square alloc]init];
        [square calculateAreaOfSide:10.0];
        
        Rectangle *rectangle = [[Rectangle alloc]init];
        [rectangle calculateAreaOfLength:10.0 andBreadth:5.0];
        
        NSArray *shapes = [[NSArray alloc]initWithObjects: square, rectangle,nil];
        id object1 = [shapes objectAtIndex:0];
        [object1 printArea];
        
        id object2 = [shapes objectAtIndex:1];
        [object2 printArea];
        
        NSCalendar *calendar = [NSCalendar calendarWithIdentifier:NSCalendarIdentifierIndian];
        
    }
    return 0;
}


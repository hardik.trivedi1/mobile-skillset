//
//  HotTub.swift
//  ObjCDemo
//
//  Created by Hardik Trivedi on 10/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation

class HotTub: NSObject{
    public var temprature: Int
    public init(withTemprature temp: Int){
        self.temprature = temp
    }
    func heatUp(){
        temprature += 20
    }
    func coolDown(){
        temprature -= 20
    }
}

//
//  House.h
//  ObjCDemo
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Bedroom.h"

struct Family{
    __unsafe_unretained NSString* familyType;
    int numberOfMembers;
};

@interface House : NSObject

@property (nonatomic,copy) NSString *address;
@property (nonatomic,readonly) int numberOfBedrooms;
@property (nonatomic) BOOL hasHotTub;

@property (nonatomic) Bedroom *firstBedroom;
@property (nonatomic) Bedroom *secondBedroom;
@property (nonatomic) struct Family family;

- (NSNumber *)multiplyA:(NSNumber *)a withB:(NSNumber *)b;
@end

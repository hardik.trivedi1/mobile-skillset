//
//  House.m
//  ObjCDemo
//
//  Created by Hardik Trivedi on 09/07/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

#import "House.h"


@interface House()

@property (nonatomic,readwrite) int numberOfBedrooms;
@end

@implementation House

- (instancetype) initWithAddress: (NSString *) address{
    self = [super init];
    if(self){
        _address = [address copy];
        _numberOfBedrooms = 2;
        _hasHotTub = false;
    }
    return self;
}

- (NSNumber *)multiplyA:(NSNumber *)a withB:(NSNumber *)b {
    float number1 = [a floatValue];
    float number2 = [b floatValue];
    float product = number1 * number2;
    NSNumber *result = [NSNumber numberWithFloat:product];
    return result;
}
@end

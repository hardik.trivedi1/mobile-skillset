//
// Created by hardik on 19/4/18.
//

#include <android_native_app_glue.h>
#include <android/log.h>
#include <android/sensor.h>


void android_main(struct android_app* state){

    __android_log_print(ANDROID_LOG_WARN,"TestNativeActivity","Activity started from test.c");

    ASensorManager* sensorManager;
    const ASensor* magnetoSensor;
    ASensorEventQueue* sensorEventQueue;

    sensorManager = ASensorManager_getInstance();
    magnetoSensor = ASensorManager_getDefaultSensor(sensorManager,
                                                                 ASENSOR_TYPE_MAGNETIC_FIELD);
    sensorEventQueue = ASensorManager_createEventQueue(sensorManager,
                                                              state->looper, LOOPER_ID_USER, NULL, NULL);

    if (magnetoSensor != NULL) {
        ASensorEvent event;
        __android_log_print(ANDROID_LOG_WARN,"TestNativeActivity","Magnetic sensor is here but no data!!");
        while (ASensorEventQueue_getEvents(sensorEventQueue,
                                           &event, 1) > 0) {
            __android_log_print(ANDROID_LOG_WARN,"TestNativeActivity","Data : %d",event.magnetic.status);
        }
    }
    else
        __android_log_print(ANDROID_LOG_WARN,"TestNativeActivity","No magnetic sensor here!!!!!!");

}
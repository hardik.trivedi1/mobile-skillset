package com.volansys.dagger2;

import javax.inject.Singleton;
import dagger.Component;

/**
 * MyComponent interface, annotated with @Component so it provides interface to SharedPrefModule class
 * and MainActivity class.
 * @author Hardik Trivedi
 * @since 05-04-2018
 * @version 1.0
 */
@Singleton
@Component(modules = {SharedPrefModule.class})
public interface MyComponent {
    /**
     * This method is responsible for injecting dependencies of MainActivity on SharedPrefModule to
     * MainActivity class.
     * @param activity
     */
    void inject(MainActivity activity);
}

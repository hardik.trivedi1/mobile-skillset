package com.volansys.dagger2;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import javax.inject.Singleton;
import dagger.Module;
import dagger.Provides;

/**
 * SharedPrefModule class, annotated as @Module, so this class provides dependencies.
 * @author Hardik Trivedi
 * @since 05-04-2018
 * @version 1.0
 */
@Module
public class SharedPrefModule {
    private Context context;

    public SharedPrefModule(Context context) {
        this.context = context;
    }

    @Singleton
    @Provides
    public Context provideContext() {
        return context;
    }

    /**
     * This method is annotated with @Singleton so returns single instance of SharedPreferences and
     * annotated with @Provides so this method provides dependency.
     * @param context
     * @return SharedPreferences object
     */
    @Singleton
    @Provides
    public SharedPreferences provideSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }
}

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');
const nodemailer = require('nodemailer');
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();

const gmailEmail = functions.config().gmail.email;
const gmailPassword = functions.config().gmail.password;
const mailTransport = nodemailer.createTransport({
  service: 'gmail',
  auth: {
    user: gmailEmail,
    pass: gmailPassword,
  },
});

exports.sendWelcomeMail = functions.database.ref('/users/{phoneNumber}/email')
    .onWrite(async (change, context) => {
      
    const email = change.after.val();
    const phoneNumber = context.params.phoneNumber;
    var password = '';
    await admin.database().ref('/users/'+ phoneNumber +'/password').once('value').then(function(snap){
    	password = snap.val();
    });
    
    // For sending email------------------------------------
    const mailOptions = {
    	from: '"Helios Inc." <support@helios.in>',
    	to: email,
  	};
  	mailOptions.subject = 'Welcome to Our Community';
  	mailOptions.text = 'You are whole-heartedly welcome to our community.\nPlease save your login details:\n'+
  	'UserID: ' + phoneNumber + '\nPassword: ' + password  +
  	 '\nStay tuned with us for more updates.\n\nFrom,\nHelios Support Team';
  	  
  	try {
  	   await mailTransport.sendMail(mailOptions);
  	} catch(error) {
  	   console.error('There was an error while sending the email:', error);
  	}
  	//------------------------------------------------------

  	// For sending SMS -------------------------------------
  	const TWILIO_NUMBER = '+1 256 291 1847';
  	var twilio = require('twilio')(
  		'ACcd07985123ede28d78cc269aea7083f9',
  		'85e3acf3add8fee66054e2a1e5eb3661'
	);
	await twilio.messages.create({
    	from: TWILIO_NUMBER,
    	to: phoneNumber,
   		body: 'You are whole-heartedly welcome to our community.\n\nPlease save your login details:\n'+
  		'UserID: ' + phoneNumber + '\nPassword: ' + password  +
  	 	'\nStay tuned with us for more updates.\n\nFrom,\nHelios Support Team'
  	})
  	.then((message) => console.log(message.sid))
  	.catch((error) => {
  		console.log(error);
  		return;
  	});
    console.log('Message sent.');

  	
  	//------------------------------------------------------
  	return null;
  });
  

package com.volansys.roomdatabase;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * StudentDao class, used to query on database entities.
 * @author Hardik Trivedi
 * @since 10-04-2018
 * @version 1.0
 */

@Dao
public interface StudentDao {

    @Query("SELECT * FROM student")
    List<Student> getAll();

    @Query("SELECT * FROM student WHERE name IN (:name)")
    List<Student> getStudentByName(String name);

    @Query("SELECT * FROM student WHERE enrollmentNo IN (:id)")
    Student getStudentById(int id);

    @Insert
    void insertStudent(Student... students);

    @Delete
    void deleteStudent(Student student);
}

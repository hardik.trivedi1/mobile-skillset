package com.volansys.roomdatabase;

import android.app.AlertDialog;
import android.arch.persistence.room.Room;
import android.database.sqlite.SQLiteConstraintException;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText mINumberEditText,mINameEditText,mIBranchEditText;
    private EditText mQNumberEditText,mQNameEditText;
    private Button mInsertButton,mSearchByIdButton,mDeleteButton,mSearchByNameButton,mRetrieveAllButton;

    private AppDatabase mDatabase;

    private Student tempStudent;
    private List<Student> students;
    private String tempStudentsData = "",insertMessage = "";

    final Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case 0:
                    Toast.makeText(MainActivity.this, insertMessage, Toast.LENGTH_SHORT).show();
                    mINumberEditText.setText("");
                    mINameEditText.setText("");
                    mIBranchEditText.setText("");
                    break;

                case 1:
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Student Info")
                            .setMessage("Enrollment no.: " + tempStudent.getEnrollmentNo() +
                                        "\nName : " + tempStudent.getName() +
                                        "\nBranch : " + tempStudent.getBranch())
                            .create().show();
                    mQNumberEditText.setText("");
                    break;

                case 2:
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Student Info")
                            .setMessage(tempStudentsData)
                            .create().show();
                    mQNameEditText.setText("");
                    break;

                case 3:
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Student Info")
                            .setMessage(tempStudentsData)
                            .create().show();
                    break;

                case 4:
                    Toast.makeText(MainActivity.this, "Student deleted.", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabase = Room.databaseBuilder(getApplicationContext(),AppDatabase.class,"student-db").build();

        initialization();

        clickEvents();
    }

    private void initialization() {
        mINumberEditText = findViewById(R.id.editText_enroll);
        mINameEditText = findViewById(R.id.editText_name);
        mIBranchEditText = findViewById(R.id.editText_branch);
        mQNumberEditText = findViewById(R.id.editText_q_id);
        mQNameEditText = findViewById(R.id.editText_q_name);
        mInsertButton = findViewById(R.id.button_insert);
        mSearchByIdButton = findViewById(R.id.button_search_id);
        mDeleteButton = findViewById(R.id.button_delete);
        mSearchByNameButton = findViewById(R.id.button_search_name);
        mRetrieveAllButton = findViewById(R.id.button_get_all);
    }

    private void clickEvents(){
        mInsertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Student student = new Student();
                        student.setEnrollmentNo(Integer.parseInt(mINumberEditText.getText().toString()));
                        student.setName(mINameEditText.getText().toString());
                        student.setBranch(mIBranchEditText.getText().toString());
                        try {
                            mDatabase.studentDao().insertStudent(student);
                            insertMessage = "Student added successfully.";
                        }
                        catch (SQLiteConstraintException e){
                            insertMessage = "Student data on this enrollment number already exists!!!";
                        }
                        handler.sendEmptyMessage(0);
                    }
                }).start();
            }
        });

        mSearchByIdButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        tempStudent = mDatabase.studentDao()
                                .getStudentById(Integer.parseInt(mQNumberEditText.getText().toString()));
                        handler.sendEmptyMessage(1);
                    }
                }).start();
            }
        });

        mSearchByNameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        students = mDatabase.studentDao().getStudentByName(mQNameEditText.getText().toString());
                        for (Student student: students) {
                            tempStudentsData = tempStudentsData + "\nEnrollment no. : " + student.getEnrollmentNo() +
                                    "\nName : " + student.getName() +
                                    "\nBranch : " + student.getBranch();
                        }
                        handler.sendEmptyMessage(2);
                    }
                }).start();
            }
        });

        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mDatabase.studentDao()
                                .deleteStudent(mDatabase.studentDao()
                                                        .getStudentById(Integer.parseInt(mQNumberEditText.getText()
                                                                                                        .toString())));
                        handler.sendEmptyMessage(4);
                    }
                }).start();
            }
        });

        mRetrieveAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        students = mDatabase.studentDao().getAll();
                        for (Student student: students) {
                            tempStudentsData = tempStudentsData + "\nEnrollment no. : " + student.getEnrollmentNo() +
                                    "\nName : " + student.getName() +
                                    "\nBranch : " + student.getBranch();
                        }
                        handler.sendEmptyMessage(3);
                    }
                }).start();
            }
        });
    }
}

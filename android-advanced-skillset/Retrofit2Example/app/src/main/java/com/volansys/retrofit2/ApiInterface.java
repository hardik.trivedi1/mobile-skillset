package com.volansys.retrofit2;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HEAD;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by hardik on 5/4/18.
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("/scripts/insertStudent.php")
    Call<String> insertStudentInfo(@Field("rollno") String rollNo,
                           @Field("name") String name,
                           @Field("email") String email);

    @GET("/scripts/getStudentsByName.php")
    Call<List<StudentInfo>> retrieveStudentInfo(@Query("rollno") String rollNo);

    @GET("/images/image4.jpg")
    Call<ResponseBody> getImage();

    @GET("/posts/1")
    Call<PostsInfo> getPost();

    @Multipart
    @POST("/posts/")
    Call<ResponseBody> insertPost(@Part("userId") RequestBody userId,
                                  @Part("id") RequestBody id,
                                  @Part("title") RequestBody title,
                                  @Part("body") RequestBody body);

    @PUT("/posts/1")
    Call<ResponseBody> updatePost(@Body RequestBody requestBody);

    @DELETE("/posts/1")
    Call<ResponseBody> deletePost();

    @Headers({"cache-control:public, max-age=64000"})
    @HEAD("/posts/")
    Call<Void> getHeader(@Header("User-Agent") String userAgent);
}

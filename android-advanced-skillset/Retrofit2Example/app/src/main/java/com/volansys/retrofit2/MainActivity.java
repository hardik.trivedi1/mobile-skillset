package com.volansys.retrofit2;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private String mRetrievedData;
    private Button mGetStudentInfoButton;
    private Button mPostStudentInfoButton;
    private Button mGetImageButton;

    private Button mGetButton,mPostButton,mPutButton,mDeleteButton,mHeadButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGetStudentInfoButton = findViewById(R.id.button_retrofit2_get);
        mGetStudentInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText queryEditText = new EditText(MainActivity.this);
                queryEditText.setHint("Enter number here...");
                queryEditText.setLayoutParams(new RelativeLayout.LayoutParams(100, 50));
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Find student by number")
                        .setView(queryEditText)
                        .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                retrieveDataByRetrofit2(queryEditText.getText().toString());
                            }
                        })
                        .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
            }
        });

        mPostStudentInfoButton = findViewById(R.id.button_retrofit2_post);
        mPostStudentInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View customView = LayoutInflater.from(MainActivity.this)
                        .inflate(R.layout.custom_dialog_view, null, false);
                new AlertDialog.Builder(MainActivity.this)
                        .setView(customView)
                        .setPositiveButton("Insert", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                insertStudentByRetrofit2(customView);
                            }
                        })
                        .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
            }
        });

        mGetImageButton = findViewById(R.id.button_retrofit2_image);
        mGetImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RetrofitClient.getClient().create(ApiInterface.class).getImage().enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        ImageView imageView = new ImageView(MainActivity.this);
                        imageView.setMaxHeight(1000);
                        imageView.setMaxWidth(1000);
                        imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

                        imageView.setImageBitmap(BitmapFactory.decodeStream(response.body().byteStream()));


                        new AlertDialog.Builder(MainActivity.this)
                                .setView(imageView)
                                .create().show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(MainActivity.this,
                               t.getMessage(),
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        mGetButton = findViewById(R.id.button_get_2_4);
        mGetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RetrofitClient.getClient24().create(ApiInterface.class).getPost().enqueue(new Callback<PostsInfo>() {
                    @Override
                    public void onResponse(Call<PostsInfo> call, Response<PostsInfo> response) {
                        if(response.code() == 200){
                            PostsInfo temp = response.body();
                            new AlertDialog.Builder(MainActivity.this)
                                    .setMessage("UserId : " + temp.getUserId() +
                                                "\nId : " + temp.getId() +
                                                "\nTitle : " + temp.getTitle() +
                                                "\nBody : " + temp.getBody())
                                    .create().show();
                        }
                    }

                    @Override
                    public void onFailure(Call<PostsInfo> call, Throwable t) {

                    }
                });
            }
        });
        mPostButton = findViewById(R.id.button_post_2_4);
        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestBody userId = RequestBody.create(MediaType.parse("text/plain"),"1"),
                        id = RequestBody.create(MediaType.parse("text/plain"),"1"),
                        title = RequestBody.create(MediaType.parse("text/plain"),"Hardik's Story"),
                        body = RequestBody.create(MediaType.parse("text/plain"),"Nothing");

                   RetrofitClient.getClient24().create(ApiInterface.class)
                           .insertPost(userId,id,title,body)
                           .enqueue(new Callback<ResponseBody>() {
                       @Override
                       public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                           Toast.makeText(MainActivity.this, "Added.\nCode : " + response.code(), Toast.LENGTH_SHORT).show();
                       }

                       @Override
                       public void onFailure(Call<ResponseBody> call, Throwable t) {
                           Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                       }
                   });

            }
        });
        mPutButton = findViewById(R.id.button_put_2_4);
        mPutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("userId","1");
                    jsonObject.put("id","1");
                    jsonObject.put("title","Hardik's story");
                    jsonObject.put("body","Nothing.");
                    final RequestBody requestBody = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
                    RetrofitClient.getClient24().create(ApiInterface.class).updatePost(requestBody).enqueue(new Callback<ResponseBody>() {
                        @Override
                        public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                            Toast.makeText(MainActivity.this, "Updated.\nCode : " + response.code(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onFailure(Call<ResponseBody> call, Throwable t) {
                            Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                catch (JSONException e){}
            }
        });
        mDeleteButton = findViewById(R.id.button_delete_2_4);
        mDeleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RetrofitClient.getClient24().create(ApiInterface.class).deletePost().enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        Toast.makeText(MainActivity.this, "Deleted.\nCode : " + response.code(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        mHeadButton = findViewById(R.id.button_head_2_4);
        mHeadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               RetrofitClient.getClient24().create(ApiInterface.class).getHeader("public,max-age=64000").enqueue(new Callback<Void>() {
                   @Override
                   public void onResponse(Call<Void> call, Response<Void> response) {
                       new AlertDialog.Builder(MainActivity.this)
                               .setMessage(response.headers().toString())
                               .create().show();
                   }

                   @Override
                   public void onFailure(Call<Void> call, Throwable t) {
                       Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                   }
               });
            }
        });
    }

    private void insertStudentByRetrofit2(View rootView) {
        final EditText rollnoEditText = rootView.findViewById(R.id.editText_rollno);
        final EditText nameEditText = rootView.findViewById(R.id.editText_name);
        final EditText emailEditText = rootView.findViewById(R.id.editText_email);

        RetrofitClient.getClient().create(ApiInterface.class).insertStudentInfo(rollnoEditText.getText().toString(),
                nameEditText.getText().toString(),
                emailEditText.getText().toString()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.code() == 200)
                    Toast.makeText(MainActivity.this, "Student added successfully.", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Student is not added.", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void retrieveDataByRetrofit2(final String queryNo) {
       mRetrievedData = "";
       RetrofitClient.getClient().create(ApiInterface.class).retrieveStudentInfo(queryNo).enqueue(new Callback<List<StudentInfo>>() {
           @Override
           public void onResponse(Call<List<StudentInfo>> call, Response<List<StudentInfo>> response) {
               if (response.code() == 200) {
                   for (StudentInfo student : response.body().subList(0,response.body().size()))
                       mRetrievedData = mRetrievedData +
                               "\nRoll no.: " +
                               queryNo +
                               "\nName : " +
                               student.getName() +
                               "\nEmail : " +
                               student.getEmail();
                   new AlertDialog.Builder(MainActivity.this)
                           .setTitle("Inquired data")
                           .setMessage(mRetrievedData)
                           .create().show();
               }
           }

           @Override
           public void onFailure(Call<List<StudentInfo>> call, Throwable t) {
               Log.w("MAinActivity","Call failed."+ t.getMessage());
           }
       });
    }
}

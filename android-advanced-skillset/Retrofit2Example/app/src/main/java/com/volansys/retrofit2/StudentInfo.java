package com.volansys.retrofit2;

/**
 * StudentInfo class used as POJO model in retrofit operations.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 05-04-2018
 */
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentInfo {

    @SerializedName("rollNo")
    @Expose
    private String rollNo;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("email")
    @Expose
    private String email;

    public String getRollNo(){
        return rollNo;
    }
    public void setRollNo(String rollNo){
        this.rollNo = rollNo;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }
}

package com.volansys.rxjava;

import rx.Observable;
import rx.subjects.PublishSubject;

/**
 * DemoObservable, acts as Observable for app
 * @author Hardik Trivedi
 * @since 10-04-2018
 * @version 1.0
 */

public class DemoObservable {
    private static DemoObservable INSTANCE = null;

    private PublishSubject<String> subject = PublishSubject.create();

    public static DemoObservable getInstance(){
        if(INSTANCE == null){
            INSTANCE = new DemoObservable();
        }
        return INSTANCE;
    }

    public void setString(String data){
        subject.onNext(data);
    }

    public Observable<String> getStringObservable() {
        return subject;
    }
}

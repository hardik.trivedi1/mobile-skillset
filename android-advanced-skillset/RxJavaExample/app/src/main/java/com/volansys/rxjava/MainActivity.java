package com.volansys.rxjava;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.IOException;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    private Button startButton,stopButton,setButton,checkButton;
    private EditText editText;
    private TextView textView;
    Observer<String> myObserver;
    int mCounter = 0;
    Subscription subscription;
    boolean isSubscribed = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //createObserver();

       setContentView(R.layout.activity_main);

       createObserver();
       startButton = findViewById(R.id.startButton);
       stopButton = findViewById(R.id.stopButton);
       editText = findViewById(R.id.editText);
       textView = findViewById(R.id.textView);
       setButton = findViewById(R.id.button_set);
       checkButton = findViewById(R.id.button_check);
       startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startButton.setEnabled(false);
                stopButton.setEnabled(true);

                subscription = Observable.just(editText.getText().toString())
                        .subscribeOn(Schedulers.io())
                        .map(new Func1<String, String>() {
                            @Override
                            public String call(String morphedString) {
                                if (!isSubscribed)
                                    throw new RuntimeException();
                                return mCounter++ + ". " + morphedString;
                            }
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(myObserver);
                isSubscribed = true;
                editText.setEnabled(true);
            }
        });


       editText.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

           }

           @Override
           public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
               subscription = Observable.just(charSequence.toString())
                       .subscribeOn(Schedulers.io())
                       .map(new Func1<String, String>() {
                           @Override
                           public String call(String morphedString) {
                               if (!isSubscribed)
                                   throw new RuntimeException();
                               return mCounter++ + ". " + morphedString;
                           }
                       })
                       .observeOn(AndroidSchedulers.mainThread())
                       .subscribe(myObserver);
           }

           @Override
           public void afterTextChanged(Editable editable) {

           }
       });

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!subscription.isUnsubscribed())
                    subscription.unsubscribe();
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
                editText.setEnabled(false);
                isSubscribed = false;
            }
        });

        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DemoObservable.getInstance().setString("Hello User!!!");
            }
        });

        checkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,AnotherActivity.class));
            }
        });

        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();
            }
        };
        DemoObservable.getInstance().getStringObservable()
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String s) {
                        return s + "I'm back.";
                    }
                })
                .subscribe(observer);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (!subscription.isUnsubscribed())
            subscription.unsubscribe();
    }

    private void createObserver() {
        myObserver = new Observer<String>() {

            @Override
            public void onCompleted() {
                //startButton.setEnabled(true);
                //stopButton.setEnabled(false);
            }

            @Override
            public void onError(Throwable e) {
                Toast.makeText(MainActivity.this,
                        "You have unsubscribed!!",
                        Toast.LENGTH_LONG).show();
                editText.setEnabled(false);
                startButton.setEnabled(true);
                stopButton.setEnabled(false);
            }

            @Override
            public void onNext(String string) {
                textView.setText(string);
            }
        };

    }
}

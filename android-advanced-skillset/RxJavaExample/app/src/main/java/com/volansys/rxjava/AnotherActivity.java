package com.volansys.rxjava;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import rx.Observer;
import rx.Scheduler;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class AnotherActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);

        final TextView textView = findViewById(R.id.textView_checking_text);
        Observer<String> observer = new Observer<String>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                textView.setText(s);
            }
        };
        DemoObservable.getInstance().getStringObservable()
                .map(new Func1<String, String>() {
                    @Override
                    public String call(String s) {
                        return s + "I'm back.";
                    }
                })
                .subscribe(observer);
        DemoObservable.getInstance().setString("Hii");
    }
}

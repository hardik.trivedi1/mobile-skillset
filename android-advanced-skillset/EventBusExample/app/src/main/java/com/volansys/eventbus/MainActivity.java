package com.volansys.eventbus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class MainActivity extends AppCompatActivity {

    private Button mCheckGreetButton;
    private TextView mGreetTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        EventBus.getDefault().register(this);
        mCheckGreetButton = findViewById(R.id.button_check_greet);
        mGreetTextView = findViewById(R.id.textView_greet_text);
        mCheckGreetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,CheckGreetingsActivity.class));
            }
        });

        getSupportFragmentManager().beginTransaction().add(R.id.container,new TestFragment()).commit();
    }

    @Subscribe(threadMode = ThreadMode.MAIN,priority = 1)
    public void onEvent(String data){
        mGreetTextView.setText(data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}

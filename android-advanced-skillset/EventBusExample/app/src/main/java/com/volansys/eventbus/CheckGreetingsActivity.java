package com.volansys.eventbus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class CheckGreetingsActivity extends AppCompatActivity {

    private EditText mNameEditText;
    private Button mGreetButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_greetings);

        mNameEditText = findViewById(R.id.editText_name);
        mGreetButton = findViewById(R.id.button_greet);

        mGreetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(CheckGreetingsActivity.this,
                        "Hello " + mNameEditText.getText().toString() + "\nCheck greetings.",
                        Toast.LENGTH_SHORT).show();
                EventBus.getDefault().post("Hello " + mNameEditText.getText().toString());
                finish();
            }
        });
    }


}

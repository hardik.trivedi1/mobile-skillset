package com.volansys.eventbus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class TestFragment extends Fragment {

    private EditText mTakeNameEditText;
    private Button mGreetInActivityButton;
    private TextView mGreetTextView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_test, container, false);
        mTakeNameEditText = (EditText) rootView.findViewById(R.id.editText_f_name);
        mGreetInActivityButton = (Button) rootView.findViewById(R.id.button_f_greet);
        mGreetTextView = rootView.findViewById(R.id.textView_f_greet_text);
        mGreetInActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EventBus.getDefault().post("Hello " + mTakeNameEditText.getText().toString());
                mTakeNameEditText.setText("");
            }
        });
        EventBus.getDefault().register(this);
        return rootView;
    }

    @Subscribe
    public void onEvent(String data){
        mGreetTextView.setText(data);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
    }
}

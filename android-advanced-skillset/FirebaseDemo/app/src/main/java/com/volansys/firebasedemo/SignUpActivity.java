package com.volansys.firebasedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SignUpActivity extends AppCompatActivity {

    private EditText mNameEditText, mEmailEditText, mPasswordEditText;
    private Button mSignUpButton;
    private DatabaseReference mRef;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        mRef = FirebaseDatabase.getInstance().getReference("users");
        mNameEditText = (EditText) findViewById(R.id.editText_name);
        mEmailEditText = (EditText) findViewById(R.id.editText_email);
        mPasswordEditText = (EditText) findViewById(R.id.editText_pswd);

        mSignUpButton = (Button) findViewById(R.id.button_signup);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doSignUp();
            }
        });
    }

    private void doSignUp(){
        String phoneNumber = getIntent().getStringExtra("PhoneNumber");
        mRef.child(phoneNumber).child("name").setValue(mNameEditText.getText().toString().trim());
        mRef.child(phoneNumber).child("email").setValue(mEmailEditText.getText().toString().trim());
        mRef.child(phoneNumber).child("password").setValue(mPasswordEditText.getText().toString().trim());
    }
}

package com.volansys.livedata;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import java.util.List;

/**
 * Created by hardik on 10/4/18.
 */

public class ListViewModel extends ViewModel {
    private MutableLiveData<List<Student>> students;

    public MutableLiveData<List<Student>> getStudents() {
        if(students == null)
            students = new MutableLiveData<List<Student>>();
        return students;
    }
}

package com.volansys.livedata;

import android.app.AlertDialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.arch.persistence.room.Room;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText mINumberEditText,mINameEditText,mIBranchEditText;
    private Button mInsertButton;
    private ListView mListView;
    private AppDatabase mDatabase;
   // private ListViewModel mModel;
    private ArrayList<String> studentsList;
    private ArrayAdapter<String> adapter;
    private List<Student> mStudentsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabase = Room.databaseBuilder(getApplicationContext(),AppDatabase.class,"student-db")
                .allowMainThreadQueries()
                .build();

        mINumberEditText = findViewById(R.id.editText_enroll);
        mINameEditText = findViewById(R.id.editText_name);
        mIBranchEditText = findViewById(R.id.editText_branch);
        mInsertButton = findViewById(R.id.button_insert);
        mListView = findViewById(R.id.listView);

        ///mModel = ViewModelProviders.of(this).get(ListViewModel.class);
        final Observer<List<Student>> nameObserver = new Observer<List<Student>>() {
            @Override
            public void onChanged(@Nullable final List<Student> students) {
                mStudentsList = students;
                studentsList = new ArrayList<>();
                adapter = new ArrayAdapter<String>(MainActivity.this,R.layout.support_simple_spinner_dropdown_item,studentsList);
                for(Student student : students)
                    studentsList.add("Number : " + student.getEnrollmentNo() +
                                    "\nName : " + student.getName() +
                                    "\nBranch : " + student.getBranch());
                mListView.setAdapter(adapter);
            }
        };
        //mModel.getStudents().observe(this,nameObserver)
        mDatabase.studentDao().getAll().observe(MainActivity.this,nameObserver);
        mInsertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                        Student student = new Student();
                        student.setEnrollmentNo(Integer.parseInt(mINumberEditText.getText().toString()));
                        student.setName(mINameEditText.getText().toString());
                        student.setBranch(mIBranchEditText.getText().toString());
                        mDatabase.studentDao().insertStudent(student);

            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int pos, long l) {
                TextView textView = (TextView) view;
                final String id = textView.getText().toString().split("\n")[0].split(":")[1].trim();
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Do you wish to delete this item?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, final int i) {
                                mDatabase.studentDao().deleteStudent(mDatabase.studentDao().getStudentById(Integer.parseInt(id)));
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
            }
        });

    }
}

package com.volansys.livedata;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * AppDatabase abstract class, acts as database for app.
 * @author Hardik Trivedi
 * @since 10-04-2018
 * @version 1.0
 */

@Database(entities = {Student.class},version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract StudentDao studentDao();
}

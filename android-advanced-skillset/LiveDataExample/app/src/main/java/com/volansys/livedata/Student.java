package com.volansys.livedata;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Student class, acts as a entity in database
 * @author Hrdik Trivedi
 * @since 10-04-2018
 * @version 1.0
 */

@Entity
public class Student {

    @PrimaryKey
    private int enrollmentNo;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "branch")
    private String branch;

    public int getEnrollmentNo() {
        return enrollmentNo;
    }

    public void setEnrollmentNo(int enrollmentNo) {
        this.enrollmentNo = enrollmentNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }
}

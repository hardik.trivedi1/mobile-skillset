package com.volansys.mqttdemo;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.SystemClock;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

/**
 * RenderVibrations class is used for representing
 * Vibration sensor's data graphically in UI using
 * android graphics libraries.
 * @author Hardik Tirvedi
 * @version 1.0
 * @since 09-03-2018
 */

public class RenderVibrations {
    private SurfaceView mWaveSurface,mThresoldSurface;
    private SurfaceHolder mWaveSurfaceHolder;
    private DrawThread mDrawThread;
    private Paint mPaint;
    private Canvas mCanvas;
    private int mWaveSurfaceHeight, mWaveSurfaceWidth;
    private boolean mShouldDraw = false;
    private int mDeltaX;
    private int x, y, mLastX, mLastY;
    private Timer mDrawTimer = new Timer();
    private PainterThread mPainterThread;

    public RenderVibrations() {
        x = 0;
        mDrawTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                mShouldDraw = true;
            }
        }, 0, 80);

        mPaint = new Paint();
        mPainterThread = new PainterThread();
        mPainterThread.start();
    }

    public void setWaveSurface(SurfaceView currentSurfaceView) {

        mWaveSurface = currentSurfaceView;

        mWaveSurfaceHolder = mWaveSurface.getHolder();
        mWaveSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mCanvas = holder.lockCanvas();
                mCanvas.drawColor(Color.WHITE);
                holder.unlockCanvasAndPost(mCanvas);
                mWaveSurfaceWidth = mWaveSurface.getWidth();
                mWaveSurfaceHeight = mWaveSurface.getHeight();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {

            }
        });
        y = mWaveSurfaceHeight / 2;
        mLastY = y;
    }

    public void drawWave(final int currentVibrations){
        mPainterThread.drawWavePattern(currentVibrations);
    }

    public void resetWaveSurface(){
        SurfaceHolder holder;
        holder = mWaveSurfaceHolder;
        mCanvas = holder.lockCanvas();
        mCanvas.drawColor(Color.WHITE);
        holder.unlockCanvasAndPost(mCanvas);
    }

    public void setThresoldSurface(SurfaceView thresoldSurface){
        mThresoldSurface = thresoldSurface;
        mThresoldSurface.setZOrderOnTop(true);
        mThresoldSurface.setZOrderMediaOverlay(true);
        SurfaceHolder holder = mThresoldSurface.getHolder();
        holder.setFormat(PixelFormat.TRANSPARENT);
        holder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder surfaceHolder) {
                mCanvas = surfaceHolder.lockCanvas();

                mPaint.setColor(Color.rgb(255, 153, 0));
                mPaint.setStrokeWidth(5);
                mPaint.setStyle(Paint.Style.STROKE);
                mCanvas.drawRect(0f,0f, mThresoldSurface.getWidth(), mThresoldSurface.getHeight(),mPaint);

                mPaint.setColor(Color.GRAY);
                mPaint.setStrokeWidth(0.5f);
                for(int i=0;i<mThresoldSurface.getHeight();i++){
                    if(i%49 == 0) {
                        mCanvas.drawLine(0f, i, mThresoldSurface.getWidth(), i, mPaint);
                    }
                }
                for (int i=0;i<mThresoldSurface.getWidth();i++){
                    if(i%62 == 0)
                        mCanvas.drawLine(i,0f,i,mThresoldSurface.getHeight(),mPaint);
                }

                surfaceHolder.unlockCanvasAndPost(mCanvas);
            }

            @Override
            public void surfaceChanged(SurfaceHolder surfaceHolder, int i0, int i1, int i2) {

            }

            @Override
            public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

            }
        });


    }

    public void drawThresold(int thresold){
        mCanvas = mThresoldSurface.getHolder().lockCanvas();
        mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        mPaint.setColor(Color.RED);
        mCanvas.drawLine(0f,
                mThresoldSurface.getWidth() - thresold - 80,
                mThresoldSurface.getWidth(),
                mThresoldSurface.getWidth() - thresold - 80
                ,
                mPaint);

        mPaint.setColor(Color.GRAY);
        mPaint.setStrokeWidth(0.5f);
        for(int i=0;i<mThresoldSurface.getHeight();i++){
            if(i%49 == 0) {
                mCanvas.drawLine(0f, i, mThresoldSurface.getWidth(), i, mPaint);
            }
        }
        for (int i=0;i<mThresoldSurface.getWidth();i++){
            if(i%62 == 0)
                mCanvas.drawLine(i,0f,i,mThresoldSurface.getHeight(),mPaint);
        }

        mPaint.setColor(Color.rgb(255, 153, 0));
        mPaint.setStrokeWidth(5);
        mPaint.setStyle(Paint.Style.STROKE);
        mCanvas.drawRect(0f,0f, mThresoldSurface.getWidth(), mThresoldSurface.getHeight(),mPaint);
        mThresoldSurface.getHolder().unlockCanvasAndPost(mCanvas);
    }

    private class DrawThread extends Thread {

        @Override
        public void run() {
            mCanvas = null;
            mPaint.setColor(Color.BLACK);
            mPaint.setStrokeWidth(5);
            mPaint.setAntiAlias(true);
            mPaint.setStyle(Paint.Style.STROKE);
            try {
                if (x > mWaveSurfaceWidth) {
                    x = 0;
                }
                mCanvas = mWaveSurfaceHolder.lockCanvas(new Rect(x - mDeltaX, 0, x + 10, mWaveSurfaceHeight));
                mCanvas.drawColor(Color.WHITE);
                mCanvas.drawLine(x - mDeltaX, mLastY, x, y, mPaint);
                mLastY = y;
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (mCanvas != null) {
                    mWaveSurfaceHolder.unlockCanvasAndPost(mCanvas);
                }
            }
        }

    }

    private class PainterThread extends Thread{
        private int vibrations;
        private Random random = new Random();
        @Override
        public void run() {
            while(true){
                try {
                    drawPoint((int) SystemClock.elapsedRealtime() / 8, random.nextInt(vibrations));
                }
                catch (IllegalArgumentException e){}
            }
        }

        public void drawWavePattern(int vibrations){
            this.vibrations = vibrations;
        }
    }

    private void drawPoint(int drawX, int drawY) {
        if (mShouldDraw) {
            y = mWaveSurfaceHeight - drawY;
            mDeltaX = drawX - mLastX;
            mLastX = drawX;
            x += mDeltaX;
            mShouldDraw = false;
            mDrawThread = new DrawThread();
            mDrawThread.start();
        }
    }


}
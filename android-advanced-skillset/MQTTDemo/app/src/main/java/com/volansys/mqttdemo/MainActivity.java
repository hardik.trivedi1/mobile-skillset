package com.volansys.mqttdemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MainActivity extends AppCompatActivity {

    MqttHelper helper;
    final String serverUri = "tcp://m12.cloudmqtt.com:14752";
    final String subscriptionTopic = "testTopic";
    final String username = "drnedwni";
    final String password = "IeMqj97jHY7G";

    private TextView mMessageTextView;
    private Button mPublishMessageButton;
    private EditText mTakeMessageEditText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMessageTextView = findViewById(R.id.textView_message);
        mPublishMessageButton = findViewById(R.id.button_publish_msg);
        mTakeMessageEditText = findViewById(R.id.editText_msg);
        mPublishMessageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                helper.publishToTopic(subscriptionTopic,mTakeMessageEditText.getText().toString());
                mTakeMessageEditText.setText("");
            }
        });

        helper = new MqttHelper(this,serverUri,username,password,subscriptionTopic);
        helper.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                mMessageTextView.append(message.toString());
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });
    }

}

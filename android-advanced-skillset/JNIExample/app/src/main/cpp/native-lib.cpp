#include <jni.h>
#include <string>
#include <android/log.h>

extern "C"
JNIEXPORT jstring

JNICALL
Java_com_volansys_jniexample_MainActivity_stringFromJNI(
        JNIEnv *env,
        jobject /* this */) {
    std::string hello = "Calculate Average";
    return env->NewStringUTF(hello.c_str());
}

extern "C"
JNIEXPORT jdouble

JNICALL
Java_com_volansys_jniexample_MainActivity_getAverage(JNIEnv *env,
                                                     jobject,
                                                     jdouble n1,
                                                     jdouble n2){
    return ((n1 + n2) / 2.0);
}

extern "C"
JNIEXPORT void
JNICALL Java_com_volansys_jniexample_MainActivity_putLog(JNIEnv *env,
                                                        jobject thisObj){
    jclass mainActivityClass = (*env).FindClass("com/volansys/jniexample/MainActivity");
    jmethodID getLogDataId = env -> GetMethodID(mainActivityClass,"getLogData","()Ljava/lang/String;");
    jobject mainActivityObject = env -> CallObjectMethod(thisObj,getLogDataId);
    const char* str = (*env).GetStringUTFChars((jstring) mainActivityObject, NULL);
    __android_log_print(ANDROID_LOG_WARN, "From native-lib.cpp","%s",str);
}

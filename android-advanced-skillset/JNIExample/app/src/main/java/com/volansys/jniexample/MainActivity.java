package com.volansys.jniexample;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // Used to load the 'native-lib' library on application startup.
    static {
        System.loadLibrary("native-lib");
    }

    private Button mAverageButton;
    private EditText mNumber1EditText,mNumber2EditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        mNumber1EditText = findViewById(R.id.editText_n1);
        mNumber2EditText = findViewById(R.id.editText_n2);
        mAverageButton = findViewById(R.id.button_avg);
        mAverageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final double number1 = Double.parseDouble(mNumber1EditText.getText().toString()),
                        number2 = Double.parseDouble(mNumber2EditText.getText().toString());
                new AlertDialog.Builder(MainActivity.this)
                        .setMessage("Average of " + mNumber1EditText.getText().toString() +
                                    " and " + mNumber2EditText.getText().toString() +
                                    " is " + getAverage(number1,number2))
                        .create().show();
                putLog();
            }
        });
    }

    public String getLogData(){
        return "Method : getLogData(), from : MainActivity.class";
    }
    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();
    public native double getAverage(double n1,double n2);
    public native void putLog();
}

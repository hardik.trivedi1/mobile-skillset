//
//  AccessoryCell.swift
//  SweetHome
//
//  Created by Hardik Trivedi on 30/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit

class AccessoryCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    var delegate: AccessoryCellDelegate?
    var indexPath: IndexPath!
    @IBOutlet weak var statusLabel: UILabel!
    
    override func draw(_ rect: CGRect) {
        indicator.isHidden = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cellImageTapped(_:))))
        self.layer.cornerRadius = 10.0
    }
    
    @objc func cellImageTapped(_ sender: UITapGestureRecognizer){
        print("Tapped from Cell")
        delegate?.didTapImage(on: self)
    }
}
protocol AccessoryCellDelegate {
    func didTapImage(on cell: AccessoryCell)
}

//
//  AccessoryDetailViewController.swift
//  SweetHome
//
//  Created by Hardik Trivedi on 31/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit

class AccessoryDetailViewController: UIViewController {

    @IBOutlet weak var accessoryDetailTableView: UITableView!
    var homeMananager: HMHomeManager!
    var home: HMHome!
    var accessory: HMAccessory!
    var basicDetails = [String?]()
    let names = ["Name","UUID","Model","Category","Manufacturer"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accessoryDetailTableView.allowsMultipleSelection = false
        accessoryDetailTableView.dataSource = self
        accessoryDetailTableView.delegate = self
        print("\(accessory.isBlocked),\(accessory.isReachable)")
        accessory.delegate = self
        self.navigationItem.title = accessory.name
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonTapped(_:)))]
    }
    
    @objc func deleteButtonTapped(_ sender: UIBarButtonItem){
        let deleteAlert = UIAlertController(title: "Delete \(accessory.name)", message: "Do you want to delete this Accessory. All the configuration associated with it will also be deleted.", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.home.removeAccessory(self.accessory, completionHandler: { (error) in
                if error == nil{
                    print("Accessory removed.")
                    self.performSegue(withIdentifier: "didAccessoryDelete", sender: self)
                }else{
                    print((error?.localizedDescription)!)
                }
            })
        }))
        present(deleteAlert, animated: true, completion: nil)
    }
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    

}
extension AccessoryDetailViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        basicDetails = [accessory.name,
                             accessory.uniqueIdentifier.uuidString,
                             accessory.model,
                             accessory.category.localizedDescription,
                             accessory.manufacturer]
        
        return accessory.services.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "Basic Details"
        }
        return accessory.services[section].name
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 5
        }else{
            return accessory.services[section].characteristics.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccessoryDetailCell", for: indexPath)
        cell.layer.cornerRadius = 10.0
        if indexPath.section == 0{
            cell.textLabel?.text = names[indexPath.row]
            cell.detailTextLabel?.text = basicDetails[indexPath.row]
        }else{
            accessory.services[indexPath.section].characteristics[indexPath.row].enableNotification(true) { (error) in
                if error != nil{
                    print((error?.localizedDescription)!)
                }
            }
            cell.textLabel?.text = "\(String(describing:accessory.services[indexPath.section].characteristics[indexPath.row].metadata?.manufacturerDescription ?? "Key"))"
            cell.detailTextLabel?.text = "\(String(describing:accessory.services[indexPath.section].characteristics[indexPath.row].value ?? "None"))"
        }
        return cell
    }
    
    
}
extension AccessoryDetailViewController: HMAccessoryDelegate{
    
    func accessory(_ accessory: HMAccessory, service: HMService, didUpdateValueFor characteristic: HMCharacteristic) {
        accessoryDetailTableView.reloadData()
    }
}


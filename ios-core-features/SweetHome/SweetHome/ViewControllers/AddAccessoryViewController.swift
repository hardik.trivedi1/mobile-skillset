//
//  AddAccessoryViewController.swift
//  SweetHome
//
//  Created by Hardik Trivedi on 08/02/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit

class AddAccessoryViewController: UIViewController {

    @IBOutlet weak var newAccessoriesTableView: UITableView!
    var discoveredAccessories = [HMAccessory]()
    var accessoryBrowser: HMAccessoryBrowser!
    var home: HMHome!
    var room: HMRoom!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accessoryBrowser = HMAccessoryBrowser()
        accessoryBrowser.delegate = self
        newAccessoriesTableView.dataSource = self
        newAccessoriesTableView.delegate = self
        newAccessoriesTableView.allowsMultipleSelection = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        discoveredAccessories = []
        indicator.startAnimating()
        indicator.hidesWhenStopped = true
        self.accessoryBrowser.startSearchingForNewAccessories()
      
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        accessoryBrowser.stopSearchingForNewAccessories()
        indicator.stopAnimating()
        super.viewWillDisappear(animated)
    }
    
    @IBAction func didTapDoneButton(_ sender: UIButton) {
        
    }
    
    @IBAction func didTapCancelButton(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
}
extension AddAccessoryViewController: UITableViewDataSource, UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Discovered Accessories"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return discoveredAccessories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddAccessoryCell", for: indexPath)
        cell.textLabel?.text = discoveredAccessories[indexPath.row].name
        cell.detailTextLabel?.text = discoveredAccessories[indexPath.row].manufacturer
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let accessory = discoveredAccessories[indexPath.row]
        print(accessory.name + " \(accessory.isBlocked)")
        indicator.stopAnimating()
        
        home.addAccessory(accessory) { (error) in
            if error == nil{
                self.home.assignAccessory(accessory, to: self.room) { (error) in
                    if error == nil{
                        print("Accessory added")
                        //self.newAccessoriesTableView.reloadData()
                    }else{
                        print((error?.localizedDescription)!)
                    }

                }
            }else{
                print((error?.localizedDescription)!)
            }
        }
        
    }
    
}

extension AddAccessoryViewController: HMAccessoryBrowserDelegate{
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didFindNewAccessory accessory: HMAccessory) {
        print(accessory.name)
        discoveredAccessories.append(accessory)
        indicator.stopAnimating()
        newAccessoriesTableView.reloadData()
        
    }
    
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didRemoveNewAccessory accessory: HMAccessory) {
        var index = 0
        for acc in discoveredAccessories{
            if acc == accessory{
                discoveredAccessories.remove(at: index)
                newAccessoriesTableView.reloadData()
            }
            index += 1
        }
    }
}

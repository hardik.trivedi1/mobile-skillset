//
//  RoomsViewController.swift
//  SweetHome
//
//  Created by Hardik Trivedi on 31/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit

class RoomsViewController: UIViewController {

    @IBOutlet weak var roomsCollectionView: UICollectionView!
    var home: HMHome!
    var rooms = [HMRoom]()
    var homeManager: HMHomeManager!
    var collectionViewCellWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.rooms = self.home.rooms
            self.roomsCollectionView.reloadData()
        }
    }
    
    private func setUpCollectionView(){
        roomsCollectionView.dataSource = self
        roomsCollectionView.delegate = self
        roomsCollectionView.allowsMultipleSelection = false
        let numberOfCellInRow : CGFloat =  UIDevice.current.userInterfaceIdiom == .pad ? 4 : 2
        collectionViewCellWidth = (UIScreen.main.bounds.size.width-(20.0 * (numberOfCellInRow+1))) / numberOfCellInRow
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collectionViewCellWidth, height: collectionViewCellWidth)
        layout.sectionInset = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20.0
        roomsCollectionView.setCollectionViewLayout(layout, animated: false)
        
        self.navigationItem.title = home.name
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped(_:))),                                                   UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonTapped(_:)))]
    }
    
    @objc func addButtonTapped(_ sender: UIBarButtonItem){
        let addRoomAlert = UIAlertController(title: "Add Room", message: "Give room name to add in \(self.home.name)", preferredStyle: .alert)
        addRoomAlert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "Room name here..."
        })
        addRoomAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            self.home.addRoom(withName: addRoomAlert.textFields![0].text!, completionHandler: { (room, error) in
                if error == nil{
                    if let _ = room{
                        self.rooms.append(room!)
                        self.roomsCollectionView.reloadData()
                    }else{
                        print("Room is not added.")
                    }
                }else{
                    print((error?.localizedDescription)!)
                }
            })
        }))
        addRoomAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        self.present(addRoomAlert, animated: true, completion: nil)
    }
    
    @objc func deleteButtonTapped(_ sender: UIBarButtonItem){
        let deleteAlert = UIAlertController(title: "Delete \(home.name)", message: "Do you want to delete this Home. All the configuration associated with it will also be deleted.", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.homeManager.removeHome(self.home, completionHandler: { (error) in
                if error != nil{
                    print((error?.localizedDescription)!)
                }else{
                    print("Home deleted.")
                    self.performSegue(withIdentifier: "didHomeDelete", sender: self)
                }
            })
        }))
        present(deleteAlert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowAccessories"{
            if let vc = segue.destination as? AccessoriesViewController{
                vc.homeManager = homeManager
                vc.home = home
                vc.room = rooms[roomsCollectionView.indexPathsForSelectedItems![0].row]
            }
        }
    }
    
    @IBAction func didRoomDelete(_ segue: UIStoryboardSegue){
        
    }
}
extension RoomsViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rooms.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RoomCell", for: indexPath) as! HomeCell
        cell.layer.cornerRadius = 10.0
        cell.imageView.image = #imageLiteral(resourceName: "Room")
        cell.label.text = rooms[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowAccessories", sender: self)
    }
    
    
}

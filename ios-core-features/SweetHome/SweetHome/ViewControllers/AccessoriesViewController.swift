//
//  AccessoriesViewController.swift
//  SweetHome
//
//  Created by Hardik Trivedi on 30/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit

class AccessoriesViewController: UIViewController {

    var homeManager: HMHomeManager!
    var collectionViewCellWidth: CGFloat = 0
    var home: HMHome!
    var room: HMRoom!
    var accessories = [HMAccessory]()
    
    var accessoryBrowser: HMAccessoryBrowser!
    @IBOutlet weak var accessoriesCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        accessoryBrowser = HMAccessoryBrowser()
        accessoryBrowser.delegate = self
        setUpCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.async {
            self.accessories = self.room.accessories
//            self.rooms = self.home.rooms
//            for room in self.rooms{
//                for accessory in room.accessories{
//                    print("\n\n\nName: \(accessory.name), Type: \(accessory.category), isReachable: \(accessory.isReachable), Room: \(accessory.room?.name)")
//                    for service in accessory.services{
//                        print("\nService Name: \(service.name), Type: \(service.serviceType)")
//                        for char in service.characteristics{
//                            print("\nvalue: \(char.value), Metadata: \(char.metadata)")
//                        }
//                    }
//                }
//            }
            self.accessoriesCollectionView.reloadData()
        }
    }
    
    private func setUpCollectionView(){
        accessoriesCollectionView.allowsMultipleSelection = false
        accessoriesCollectionView.dataSource = self
        accessoriesCollectionView.delegate = self
        let numberOfCellInRow : CGFloat =  UIDevice.current.userInterfaceIdiom == .pad ? 4 : 2
        collectionViewCellWidth = (UIScreen.main.bounds.size.width-(20.0 * (numberOfCellInRow+1))) / numberOfCellInRow
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collectionViewCellWidth, height: collectionViewCellWidth)
        layout.sectionInset = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20.0
        accessoriesCollectionView.setCollectionViewLayout(layout, animated: true)
        self.navigationItem.title = room.name
        self.navigationItem.rightBarButtonItems = [UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped(_:))),
        UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(deleteButtonTapped(_:)))]
    }
    
    @objc func addButtonTapped(_ sender: UIBarButtonItem){
        
        let addAlert = UIAlertController(title: "Add Accessory", message: "Start to discover Accessories available for pairing.", preferredStyle: .alert)
        addAlert.addAction(UIAlertAction(title: "Start", style: .default, handler: { (action) in
            //self.accessoryBrowser.startSearchingForNewAccessories()
            self.performSegue(withIdentifier: "AddAccessory", sender: self)
            
        }))
        addAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        present(addAlert, animated: true, completion: nil)
    }
    
    @objc func deleteButtonTapped(_ sender: UIBarButtonItem){
        let deleteAlert = UIAlertController(title: "Delete \(room.name)", message: "Do you want to delete this Room. All the configuration associated with it will also be deleted.", preferredStyle: .alert)
        deleteAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        deleteAlert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            self.home.removeRoom(self.room, completionHandler: { (error) in
                if error == nil{
                    print("Room deleted.")
                    self.performSegue(withIdentifier: "didRoomDelete", sender: self)
                }else{
                    print((error?.localizedDescription)!)
                }
            })
        }))
        present(deleteAlert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowAccessoryDetail"{
            if let vc = segue.destination as? AccessoryDetailViewController{
                vc.homeMananager = homeManager
                vc.home = home
                vc.accessory = accessories[accessoriesCollectionView.indexPathsForSelectedItems![0].row]
            }
        }else if segue.identifier == "AddAccessory"{
            if let vc = segue.destination as? AddAccessoryViewController{
                vc.home = home
                vc.room = room
            }
        }
    }

    @IBAction func didAccessoryDelete(_ segue: UIStoryboardSegue){
        
    }
}

extension AccessoriesViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return accessories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AccessoryCell", for: indexPath) as! AccessoryCell
        cell.indexPath = indexPath
        cell.delegate = self
        accessories[indexPath.row].delegate = self
        if accessories[indexPath.row].category.categoryType == HMAccessoryCategoryTypeLightbulb{
            for service in accessories[indexPath.row].services{
                for char in service.characteristics{
                        char.enableNotification(true) { (error) in
                            if error != nil{
                                print((error?.localizedDescription)!)
                            }
                        }
                        if char.metadata?.manufacturerDescription == "Power State"{
                            if let status = char.value as? Bool{
                                cell.imageView.image = status ? #imageLiteral(resourceName: "OpenBulb") : #imageLiteral(resourceName: "ClosedBulb")
                                cell.statusLabel.text = status ? "ON" : "OFF"
                            }else{
                                cell.imageView.image = #imageLiteral(resourceName: "ClosedBulb")
                                cell.statusLabel.text = "OFF"
                            }
                        }
                }
            }
        }else if accessories[indexPath.row].category.categoryType == HMAccessoryCategoryTypeSensor{
            for service in accessories[indexPath.row].services{
                for char in service.characteristics{
                    char.enableNotification(true) { (error) in
                        if error != nil{
                            print((error?.localizedDescription)!)
                        }
                    }
                    if char.metadata?.manufacturerDescription == "Motion Detected"{
                        cell.imageView.isUserInteractionEnabled = false
                        if let status = char.value as? Bool{
                            cell.imageView.image = status ? #imageLiteral(resourceName: "OpenMotion") : #imageLiteral(resourceName: "ClosedMotion")
                            cell.statusLabel.text = status ? "Motion" : "No Motion"
                        }else{
                            cell.imageView.image = #imageLiteral(resourceName: "ClosedMotion")
                            cell.statusLabel.text = "No Motion"
                        }
                    }
                }
            }
        }
        cell.label.text = accessories[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowAccessoryDetail", sender: self)
    }
}
extension AccessoriesViewController: HMAccessoryBrowserDelegate{
    func accessoryBrowser(_ browser: HMAccessoryBrowser, didFindNewAccessory accessory: HMAccessory) {
        print(accessory.name + (accessory.manufacturer ?? " NG"))
        home.addAccessory(accessory) { (error) in
            if error == nil{
                self.home.assignAccessory(accessory, to: self.room) { (error) in
                    if error == nil{
                        print("Accessory added")
                        self.accessoriesCollectionView.reloadData()
                    }else{
                        print((error?.localizedDescription)!)
                    }

                }
            }else{
                print((error?.localizedDescription)!)
            }
        }
    }
}
extension AccessoriesViewController: HMAccessoryDelegate{
    func accessory(_ accessory: HMAccessory, service: HMService, didUpdateValueFor characteristic: HMCharacteristic) {
        accessoriesCollectionView.reloadData()
    }
}
extension AccessoriesViewController: AccessoryCellDelegate{
    func didTapImage(on cell: AccessoryCell) {
        if accessories[cell.indexPath.row].category.categoryType == HMAccessoryCategoryTypeLightbulb{
            for service in accessories[cell.indexPath.row].services{
                for char in service.characteristics{
                    if char.metadata?.manufacturerDescription == "Power State"{
                        if let status = char.value as? Bool{
                            char.writeValue(!status) { (error) in
                                if error == nil{
                                    print("Value changed.")
                                    self.accessoriesCollectionView.reloadData()
                                }else{
                                    print((error?.localizedDescription)!)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//
//  HomesViewController.swift
//  SweetHome
//
//  Created by Hardik Trivedi on 30/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import HomeKit

class HomesViewController: UIViewController {

    @IBOutlet weak var homeCollectionView: UICollectionView!
    let homeManager = HMHomeManager()
    var homes = [HMHome]()
    var collectionViewCellWidth: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            self.homes = self.homeManager.homes
            self.homeCollectionView.reloadData()
        }
        
    }
    
    private func setUpCollectionView(){
        homeCollectionView.dataSource = self
        homeCollectionView.delegate = self
        homeCollectionView.allowsMultipleSelection = false
        let numberOfCellInRow : CGFloat =  UIDevice.current.userInterfaceIdiom == .pad ? 4 : 2
        collectionViewCellWidth = (UIScreen.main.bounds.size.width-(20.0 * (numberOfCellInRow+1))) / numberOfCellInRow
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: collectionViewCellWidth, height: collectionViewCellWidth)
        layout.sectionInset = UIEdgeInsets(top: 20.0, left: 20.0, bottom: 20.0, right: 20.0)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20.0
        homeCollectionView.setCollectionViewLayout(layout, animated: false)
        
    }
    
    @IBAction func didTapAddHomeButton(_ sender: UIBarButtonItem) {
        let addHomeAlert = UIAlertController(title: "Add Home", message: "Give name of Home", preferredStyle: .alert)
        addHomeAlert.addTextField { (textField) in
            textField.placeholder = "Home Name here ..."
        }
        addHomeAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            let homeName = addHomeAlert.textFields?[0].text
            self.homeManager.addHome(withName: homeName!, completionHandler: { (home, error) in
                if error == nil{
                    if let addedHome = home{
                        self.homes.append(addedHome)
                        self.homeCollectionView.reloadData()
                    }else{
                        print("Home is not added.")
                    }
                }else{
                    print((error?.localizedDescription)!)
                }
            })
        }))
        addHomeAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
            
        }))
        present(addHomeAlert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowRooms"{
            if let vc = segue.destination as? RoomsViewController{
                vc.home = homes[homeCollectionView.indexPathsForSelectedItems![0].row]
                vc.homeManager = self.homeManager
            }
        }
    }
    
    @IBAction func didHomeDelete(_ segue: UIStoryboardSegue){
        
    }
}
extension HomesViewController: UICollectionViewDelegate,UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return homes.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.layer.cornerRadius = 10.0
        cell.imageView.image = #imageLiteral(resourceName: "Home")
        cell.label.text = homes[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowRooms", sender: self)
    }
    
    
}

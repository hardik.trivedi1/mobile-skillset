//
//  Task.swift
//  RealmDemo
//
//  Created by Hardik Trivedi on 03/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class Task: Object{
    @objc dynamic var name = ""
    @objc dynamic var createdAt = NSDate()
    @objc dynamic var notes = ""
    @objc dynamic var isCompleted = false
}

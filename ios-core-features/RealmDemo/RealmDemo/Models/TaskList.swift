//
//  TaskList.swift
//  RealmDemo
//
//  Created by Hardik Trivedi on 04/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import Foundation
import RealmSwift

class TaskList: Object{
    @objc dynamic var name = ""
    @objc dynamic var createdAt = NSDate()
    let tasks = List<Task>()
}

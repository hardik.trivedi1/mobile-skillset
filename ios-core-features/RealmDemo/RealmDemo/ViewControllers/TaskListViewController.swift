//
//  TaskListViewController.swift
//  RealmDemo
//
//  Created by Hardik Trivedi on 04/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class TaskListViewController: UIViewController {

    @IBOutlet weak var taskListstableView: UITableView!
    @IBOutlet weak var sortingTypeSegment: UISegmentedControl!
    var isEditingMode = false
    var taskLists: Results<TaskList>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpTaskLists()
        _ = taskLists.observe { (change) in
            let uiRealm = try! Realm()
            self.taskLists = uiRealm.objects(TaskList.self)
            self.taskLists = self.sortingTypeSegment.selectedSegmentIndex == 0 ? self.taskLists.sorted(byKeyPath: "name") : self.taskLists.sorted(byKeyPath: "createdAt" ,ascending: false)
            self.taskListstableView.setEditing(false, animated: true)
            self.taskListstableView.reloadData()
        }
    }
    
    func saveTasksData(taskList: TaskList){
        do{
            let uiRealm = try Realm()
            try uiRealm.write {
                uiRealm.add([taskList])
                try uiRealm.commitWrite()
            }
        }catch let error{
            print(error.localizedDescription)
        }
    }
    
    func setUpTaskLists(){
        let uiRealm = try! Realm()
        taskLists = uiRealm.objects(TaskList.self)
        taskLists = sortingTypeSegment.selectedSegmentIndex == 0 ? taskLists.sorted(byKeyPath: "name") : taskLists.sorted(byKeyPath: "createdAt" ,ascending: false)
        taskListstableView.setEditing(false, animated: true)
        taskListstableView.reloadData()
    }
    
    @IBAction func sortingTypeChanged(_ sender: UISegmentedControl) {
        if sender.selectedSegmentIndex == 0{
            taskLists = taskLists.sorted(byKeyPath: "name")
        }
        else{
            taskLists = taskLists.sorted(byKeyPath: "createdAt", ascending:false)
        }
        taskListstableView.reloadData()
    }
    
    @IBAction func editBarButtonTapped(_ sender: UIBarButtonItem) {
        isEditingMode = !isEditingMode
        taskListstableView.setEditing(isEditingMode, animated: true)
    }
    
    @IBAction func addBarButtonTapped(_ sender: UIBarButtonItem) {
        let addTaskAlert = UIAlertController(title: "Add Task List", message: nil, preferredStyle: .alert)
        addTaskAlert.addTextField { (textField) in
            textField.placeholder = "Task List Name"
        }
        addTaskAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            let list = TaskList()
            list.name = addTaskAlert.textFields?[0].text ?? ""
            list.createdAt = NSDate()
            self.saveTasksData(taskList: list)
            self.setUpTaskLists()
        }))
        addTaskAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(addTaskAlert, animated: true, completion: nil)
    }
    
    func displayEditAlert(with taskList: TaskList){
        let editTaskAlert = UIAlertController(title: "Edit Task List", message: nil, preferredStyle: .alert)
        editTaskAlert.addTextField { (textField) in
            textField.text = taskList.name
        }
        editTaskAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            try! Realm().write {
                taskList.name = editTaskAlert.textFields?[0].text ?? ""
            }
            self.setUpTaskLists()
        }))
        editTaskAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(editTaskAlert, animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowTasks"{
            if let vc = segue.destination as? TasksTableViewController{
                vc.taskList = taskLists![(taskListstableView.indexPathForSelectedRow?.row)!]
            }
        }
    }
    

}

extension TaskListViewController: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskLists.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Your Task Lists"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskListCell", for: indexPath)
        cell.textLabel?.text = taskLists[indexPath.row].name
        cell.detailTextLabel?.text = "\(taskLists[indexPath.row].tasks.count) " + "\(taskLists[indexPath.row].tasks.count == 0 || taskLists[indexPath.row].tasks.count == 1 ? "Task" : "Tasks" )"
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let uiRealm = try! Realm()
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.destructive, title: "Delete") { (deleteAction, indexPath) -> Void in
            
            //Deletion will go here
            
            let listToBeDeleted = self.taskLists[indexPath.row]
            try! uiRealm.write({ () -> Void in
                uiRealm.delete(listToBeDeleted)
                self.setUpTaskLists()
            })
        }
        let editAction = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Edit") { (editAction, indexPath) -> Void in
            
            // Editing will go here
            let listToBeUpdated = self.taskLists[indexPath.row]
            self.displayEditAlert(with: listToBeUpdated)
            
        }
        return [deleteAction, editAction]

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "ShowTasks", sender: self)
    }
}

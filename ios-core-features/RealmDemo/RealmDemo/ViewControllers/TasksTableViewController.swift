//
//  TasksTableViewController.swift
//  RealmDemo
//
//  Created by Hardik Trivedi on 04/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import RealmSwift

class TasksTableViewController: UITableViewController {

    var taskList: TaskList!
    var openTasks: Results<Task>!
    var completedTasks: Results<Task>!
    var isEditingMode = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Tasks"
         self.navigationItem.rightBarButtonItem = self.editButtonItem
        self.navigationItem.rightBarButtonItems = [ UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addButtonTapped(sender:))),UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(editButtonTapped(sender:)))]
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setUpTasksList()
    }
    func setUpTasksList(){
        completedTasks = self.taskList.tasks.filter("isCompleted = true")
        openTasks = self.taskList.tasks.filter("isCompleted = false")
        self.tableView.reloadData()
    }
    
    @objc func addButtonTapped(sender: UIBarButtonItem){
        let addTaskAlert = UIAlertController(title: "Add Task List", message: nil, preferredStyle: .alert)
        addTaskAlert.addTextField { (textField) in
            textField.placeholder = "Task Name"
        }
        addTaskAlert.addTextField { (textField) in
            textField.placeholder = "Notes"
        }
        addTaskAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            let task = Task()
            task.name = addTaskAlert.textFields?[0].text ?? ""
            task.notes = addTaskAlert.textFields?[1].text ?? ""
            task.createdAt = NSDate()
            task.isCompleted = false
            
            try! Realm().write {
                self.taskList.tasks.append(task)
                try! Realm().add(task)
                try! Realm().commitWrite()
                self.setUpTasksList()
            }
        }))
        addTaskAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(addTaskAlert, animated: true, completion: nil)
    }
    
    @objc func editButtonTapped(sender: UIBarButtonItem){
        isEditingMode = !isEditingMode
        self.tableView.setEditing(isEditingMode, animated: true)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Open Tasks" : "Completed Tasks"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return openTasks != nil ? openTasks.count : 0
        }else if section == 1{
            return completedTasks != nil ? completedTasks.count : 0
        }
        return 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath)
        cell.textLabel?.text = indexPath.section == 0 ? openTasks[indexPath.row].name : completedTasks[indexPath.row].name
        return cell
    }
 
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let uiRealm = try! Realm()
        let deleteAction = UITableViewRowAction(style: UITableViewRowAction.Style.destructive, title: "Delete") { (deleteAction, indexPath) -> Void in
            
            let taskToBeDeleted = indexPath.section == 0 ? self.openTasks[indexPath.row] : self.completedTasks[indexPath.row]
            try! uiRealm.write({ () -> Void in
                uiRealm.delete(taskToBeDeleted)
                self.setUpTasksList()
            })
        }
        let editAction = UITableViewRowAction(style: UITableViewRowAction.Style.normal, title: "Edit") { (editAction, indexPath) -> Void in
            let taskToBeUpdated = indexPath.section == 0 ? self.openTasks[indexPath.row] : self.completedTasks[indexPath.row]
            self.displayEditAlert(with: taskToBeUpdated)
            
        }
        return [deleteAction, editAction]
        
    }
    
    func displayEditAlert(with task: Task){
        let editTaskAlert = UIAlertController(title: "Edit Task", message: nil, preferredStyle: .alert)
        editTaskAlert.addTextField { (textField) in
            textField.text = task.name
        }
        editTaskAlert.addTextField { (textField) in
            textField.text = task.notes
        }
        editTaskAlert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            try! Realm().write {
                task.name = editTaskAlert.textFields?[0].text ?? ""
                task.notes = editTaskAlert.textFields?[1].text ?? ""
            }
            self.setUpTasksList()
        }))
        editTaskAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(editTaskAlert, animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        try! Realm().write {
            if indexPath.section == 0{
                openTasks[indexPath.row].isCompleted = true
            }else{
                completedTasks[indexPath.row].isCompleted = false
            }
            try! Realm().commitWrite()
            setUpTasksList()
        }
        
    }
}

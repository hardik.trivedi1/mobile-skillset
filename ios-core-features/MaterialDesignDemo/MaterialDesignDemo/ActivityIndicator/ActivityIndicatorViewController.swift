//
//  ActivityIndicatorViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 27/12/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialActivityIndicator
import MaterialComponents.MaterialActivityIndicator_ColorThemer

class ActivityIndicatorViewController: UIViewController {

    @IBOutlet weak var showActivityButton: UIButton!
    var activityIndicator: MDCActivityIndicator!
    var anotherIndicator: MDCActivityIndicator!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator = MDCActivityIndicator(frame: CGRect(x: showActivityButton.frame.minX, y: showActivityButton.frame.maxY + 48, width: showActivityButton.frame.width, height: 100.0))
        activityIndicator.sizeToFit()
        activityIndicator.cycleColors = [.blue, .red, .green, .yellow]
        self.view.addSubview(activityIndicator)
        
        anotherIndicator = MDCActivityIndicator(frame: CGRect(x: showActivityButton.frame.minX * 1.5, y: showActivityButton.frame.maxY + 48, width: showActivityButton.frame.width, height: 100.0))
        anotherIndicator.sizeToFit()
        anotherIndicator.indicatorMode = .determinate
        anotherIndicator.progress = 0.0
        self.view.addSubview(anotherIndicator)
        
        
        let colorScheme = MDCSemanticColorScheme()
        colorScheme.primaryColor = #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1)
        colorScheme.secondaryColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        MDCActivityIndicatorColorThemer.applySemanticColorScheme(colorScheme, to: anotherIndicator)
    }
    
    @IBAction func showIndicatorClicked(_ sender: UIButton) {
        activityIndicator.startAnimating()
        anotherIndicator.startAnimating()
        print("Activity started...")
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(5), execute: {
            self.activityIndicator.stopAnimating()
            print("Activity stopped.")
        })
        var timePassed = 0.0
        var progress: Float = 0.0
        Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { (timer) in
            timePassed += 1.0
            progress += 0.25
            print("Activity in-progress with time: \(timePassed)")
            self.anotherIndicator.setProgress(progress, animated: true)
            if timePassed == 5{
                self.anotherIndicator.stopAnimating()
                timer.invalidate()
            }
        }
    }
}

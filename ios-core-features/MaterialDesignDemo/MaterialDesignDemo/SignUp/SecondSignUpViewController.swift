//
//  SecondSignUpViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 21/12/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Foundation
import MaterialComponents.MaterialSlider
class SecondSignUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let slider = MDCSlider(frame: CGRect(x: 8, y: 100, width: self.view.frame.width - 48, height: 27))
        slider.addTarget(self,
                         action: #selector(didChangeSliderValue(senderSlider:)),
                         for: .valueChanged)
        slider.isStatefulAPIEnabled = true
        slider.inkColor = #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1)
        slider.isThumbHollowAtStart = true
        slider.thumbElevation = ShadowElevation(12)
        slider.setThumbColor(.red, for: .selected)
        self.view.addSubview(slider)
    }
    
    
    @objc func didChangeSliderValue(senderSlider:MDCSlider) {
        print("Did change slider value to: %@",(senderSlider.value * 100))
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

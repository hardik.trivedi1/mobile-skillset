//
//  FirstSignUpViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 21/12/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import MaterialComponents
import MaterialComponents.MaterialTextFields_ColorThemer
import MaterialComponents.MaterialButtons
import MaterialComponents.MaterialColorScheme
class FirstSignUpViewController: UIViewController {

    @IBOutlet weak var nameTextField: MDCTextField!
    @IBOutlet weak var emailTextField: MDCTextField!
    @IBOutlet weak var phoneTextField: MDCTextField!
    @IBOutlet weak var nextButton: MDCButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nameController = MDCTextInputControllerOutlined(textInput: nameTextField)
        nameController.isFloatingEnabled = true
        nameController.placeholderText = "Full Name"
        nameController.floatingPlaceholderActiveColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        nameController.floatingPlaceholderNormalColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        nameController.mdc_adjustsFontForContentSizeCategory = true
        
        let emailController = MDCTextInputControllerOutlined(textInput: emailTextField)
        emailTextField.keyboardType = .emailAddress
        emailController.isFloatingEnabled = true
        emailController.placeholderText = "Email"
        emailController.floatingPlaceholderActiveColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        emailController.floatingPlaceholderNormalColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        emailController.mdc_adjustsFontForContentSizeCategory = true
        
        let phoneController = MDCTextInputControllerOutlined(textInput: phoneTextField)
        nameTextField.keyboardType = .phonePad
        phoneController.isFloatingEnabled = true
        phoneController.placeholderText = "Phone"
        phoneController.floatingPlaceholderActiveColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        phoneController.floatingPlaceholderNormalColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        phoneController.mdc_adjustsFontForContentSizeCategory = true
        
        nextButton.setElevation(ShadowElevation(6), for: .normal)
        nextButton.setElevation(ShadowElevation(12), for: .highlighted)
        nextButton.setTitle("Next", for: .normal)
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


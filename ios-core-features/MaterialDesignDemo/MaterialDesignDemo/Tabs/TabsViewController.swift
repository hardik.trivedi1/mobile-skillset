//
//  TabsViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 24/10/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTabs
import MaterialComponents.MaterialTabs_ColorThemer

class TabsViewController: UIViewController {

    @IBOutlet weak var textLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        textLabel.text = "Home"
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setUpTabs()
    }
    
    private func setUpTabs(){
        let tabBar = MDCTabBar(frame: CGRect(x: 0, y: (navigationController?.navigationBar.bounds.height)! + 48, width: view.frame.width, height: view.frame.height))
        let tabBarItem1 = UITabBarItem(title: "Recents", image: #imageLiteral(resourceName: "home"), tag: 0);
        tabBarItem1.badgeValue = "10"
        
        tabBar.items = [
            tabBarItem1,
            UITabBarItem(title: "Favorites", image: UIImage(named: "favourite"), tag: 1),
        ]
        tabBar.itemAppearance = .titledImages
        tabBar.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        tabBar.sizeToFit()
        tabBar.backgroundColor = .gray
        tabBar.tintColor = .green
        tabBar.alignment = .justified
        tabBar.delegate = self
        
        view.addSubview(tabBar)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension TabsViewController: MDCTabBarDelegate{
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        print("You cilcked \(item.title!)")
        textLabel.text = "You cilcked \(item.title!)"
    }
}

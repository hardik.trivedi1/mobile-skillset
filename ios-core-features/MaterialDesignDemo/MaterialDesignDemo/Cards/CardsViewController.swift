//
//  CardsViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 26/12/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialCards
import MaterialComponents.MaterialCards_ColorThemer

class CardsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let card = MDCCard()
        
        card.frame = CGRect(x: 0, y: self.navigationController?.navigationBar.frame.height ?? 48.0, width: self.view.frame.width, height: 128.0)
//        let imageView = UIImageView(frame: card.frame)
//        imageView.image = #imageLiteral(resourceName: "home")
//        card.addSubview(imageView)
        let label = UILabel(frame: card.frame)
        label.text = "Hey There!"
        card.addSubview(label)
        let colorScheme = MDCSemanticColorScheme()
        MDCCardsColorThemer.applySemanticColorScheme(colorScheme, to: card)
        
        self.view.addSubview(card)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 20/12/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialDialogs

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func dialogsButtonClicked(_ sender: UIButton) {
        
        let alertController = MDCAlertController(title: "Test Message", message: "This is test message for dialog.")
        let action = MDCAlertAction(title:"OK") { (action) in
            print("OK from dialog")
        }
        alertController.addAction(action)
        
        let colorScheme = MDCSemanticColorScheme()
        MDCAlertColorThemer.applySemanticColorScheme(colorScheme, to: alertController)
        
        present(alertController, animated:true, completion:nil)
    }
    
}


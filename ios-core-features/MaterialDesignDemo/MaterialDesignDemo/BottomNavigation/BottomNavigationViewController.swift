//
//  ViewController.swift
//  MaterialDesignDemo
//
//  Created by Hardik Trivedi on 24/10/18.
//  Copyright © 2018 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Foundation
import MaterialComponents.MaterialBottomNavigation_ColorThemer
import MaterialComponents.MaterialColorScheme

class BottomNavigationViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    
    var colorScheme = MDCSemanticColorScheme()
    
    // Create a bottom navigation bar to add to a view.
    let bottomNavBar = MDCBottomNavigationBar()
    
    func commonBottomNavigationTypicalUseSwiftExampleInit() {
        view.backgroundColor = .lightGray
        view.addSubview(bottomNavBar)
        
        // Always show bottom navigation bar item titles.
        bottomNavBar.titleVisibility = .always
        
        // Cluster and center the bottom navigation bar items.
        bottomNavBar.alignment = .centered
        
        // Add items to the bottom navigation bar.
        let tabBarItem1 = UITabBarItem(title: "Home", image: UIImage(named: "home"), tag: 0)
        tabBarItem1.badgeValue = "10"
        tabBarItem1.accessibilityValue = "10 unread messages"
        let tabBarItem2 =
            UITabBarItem(title: "Messages", image: UIImage(named: "message"), tag: 1)
        let tabBarItem3 =
            UITabBarItem(title: "Favorites", image: UIImage(named: "favourite"), tag: 2)
        bottomNavBar.items = [ tabBarItem1, tabBarItem2, tabBarItem3 ]
        
        // Select a bottom navigation bar item.
        bottomNavBar.selectedItem = tabBarItem1;
        
       
        
    }
    
    func layoutBottomNavBar() {
        let size = bottomNavBar.sizeThatFits(view.bounds.size)
        var bottomNavBarFrame = CGRect(x: 0,
                                       y: view.bounds.height - size.height,
                                       width: size.width,
                                       height: size.height)
        if #available(iOS 11.0, *) {
            bottomNavBarFrame.size.height += view.safeAreaInsets.bottom
            bottomNavBarFrame.origin.y -= view.safeAreaInsets.bottom
        }
        bottomNavBar.frame = bottomNavBarFrame
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        layoutBottomNavBar()
        commonBottomNavigationTypicalUseSwiftExampleInit()
        bottomNavBar.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
         label.text = "You are in Home."
    }
    @IBAction func closeIt(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
}

extension BottomNavigationViewController: MDCBottomNavigationBarDelegate{
    func bottomNavigationBar(_ bottomNavigationBar: MDCBottomNavigationBar, didSelect item: UITabBarItem) {
        print("Item clicked : \(item.title!)")
        label.text = "You clicked : \(item.title!)"
    }
}


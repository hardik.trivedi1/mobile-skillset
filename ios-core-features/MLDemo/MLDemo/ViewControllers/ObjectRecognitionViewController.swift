//
//  ObjectRecognitionViewController.swift
//  MLDemo
//
//  Created by Hardik Trivedi on 08/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import CoreML
import Photos

class ObjectRecognitionViewController: UIViewController,UINavigationControllerDelegate  {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var inferenceLabel: UILabel!
    
    var model: Inceptionv3!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Object Recognition"
        self.navigationItem.rightBarButtonItems =
            [ UIBarButtonItem(barButtonSystemItem: .camera, target: self, action: #selector(cameraButtonTapped(_:))),
              UIBarButtonItem(title: "Library", style: .plain, target: self, action: #selector(libraryButtonTapped(_:)))]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        model = Inceptionv3()
    }
    
    @objc func cameraButtonTapped(_ sender: UIBarButtonItem){
        if !UIImagePickerController.isSourceTypeAvailable(.camera) {
            print("No Camera Available.")
            return
        }
        
        let cameraPicker = UIImagePickerController()
        cameraPicker.delegate = self
        cameraPicker.sourceType = .camera
        cameraPicker.allowsEditing = false
        
        present(cameraPicker, animated: true)
    }
    
    @objc func libraryButtonTapped(_ sener: UIBarButtonItem){
        checkPermission { (permitted) in
            if permitted{
                let picker = UIImagePickerController()
                picker.allowsEditing = false
                picker.delegate = self
                picker.sourceType = .photoLibrary
                self.present(picker, animated: true)
            }else{
                print("No permission to use Library")
            }
        }
    }
    
    func analyzeImage(image: CVPixelBuffer){
        guard let prediction = try? model.prediction(image: image) else {
            return
        }
        
        inferenceLabel.text = "I think this is a \(prediction.classLabel)."
    }
}
extension ObjectRecognitionViewController: UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        inferenceLabel.text = "Analyzing Image..."
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            return
        }
        
        UIGraphicsBeginImageContextWithOptions(CGSize(width: 299, height: 299), true, 2.0)
        image.draw(in: CGRect(x: 0, y: 0, width: 299, height: 299))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer : CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(newImage.size.width), Int(newImage.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard (status == kCVReturnSuccess) else {
            return
        }
        
        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pixelData, width: Int(newImage.size.width), height: Int(newImage.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue) //3
        
        context?.translateBy(x: 0, y: newImage.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        UIGraphicsPushContext(context!)
        newImage.draw(in: CGRect(x: 0, y: 0, width: newImage.size.width, height: newImage.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        imageView.image = newImage
        analyzeImage(image: pixelBuffer!)
    }
    
    func checkPermission(completion: @escaping (Bool)->Void) {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            completion(true)
        case .notDetermined:
            PHPhotoLibrary.requestAuthorization({(newStatus) in
                if newStatus == PHAuthorizationStatus.authorized {
                    completion(true)
                }
            })
            case .restricted:
                completion(false)
            case .denied:
                completion(false)
            }
        }
}

//
//  SignUpViewController.swift
//  FirebaseDemo
//
//  Created by Hardik Trivedi on 13/02/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseDatabase
import SVProgressHUD

class SignUpViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var phoneNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func didTapSignUpButton(_ sender: UIButton) {
        SVProgressHUD.show(withStatus: "Signing up...")
        let ref = Database.database().reference()
        ref.child("users/\(phoneNumber)/name").setValue(nameTextField.text)
        ref.child("users/\(phoneNumber)/email").setValue(emailTextField.text)
        ref.child("users/\(phoneNumber)/password").setValue(passwordTextField.text)
        SVProgressHUD.dismiss {
            self.performSegue(withIdentifier: "PhoneVerified", sender: self)
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

    }
    
}

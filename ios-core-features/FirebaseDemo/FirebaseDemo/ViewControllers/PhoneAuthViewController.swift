//
//  PhoneAuthViewController.swift
//  FirebaseDemo
//
//  Created by Hardik Trivedi on 12/02/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAuth
import SVProgressHUD

class PhoneAuthViewController: UIViewController {

    @IBOutlet weak var phoneNumberTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.showInfo(withStatus: "Please use country code in Phone number.\nExample, '+919876543210'")
    }
    
    @IBAction func didTapNextButton(_ sender: UIButton) {
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumberTextfield.text!, uiDelegate: nil) { (verificationID, error) in
            if error == nil {
                UserDefaults.standard.set(verificationID, forKey: "verificationID")
                self.performSegue(withIdentifier: "VerifyPhoneNumber", sender: self)
            }else{
                print((error?.localizedDescription)!)
            }
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
    }
    

}

//
//  HomeViewController.swift
//  FirebaseDemo
//
//  Created by Hardik Trivedi on 12/02/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import SVProgressHUD
import FirebaseAuth

class HomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func phoneNumberAuthTapped(_ sender: UIButton) {
        
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    }
    
    @IBAction func phoneVerified(_ segue: UIStoryboardSegue){
        SVProgressHUD.showInfo(withStatus: "Phone Number verified successfully.")
//        let firebaseAuth = Auth.auth()
//        do {
//            try firebaseAuth.signOut()
//        } catch let signOutError as NSError {
//            print ("Error signing out: %@", signOutError)
//        }
    }
}

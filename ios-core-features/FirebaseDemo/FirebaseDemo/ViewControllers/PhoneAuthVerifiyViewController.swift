//
//  PhoneAuthVerifiyViewController.swift
//  FirebaseDemo
//
//  Created by Hardik Trivedi on 12/02/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import FirebaseAuth

class PhoneAuthVerifiyViewController: UIViewController {

    @IBOutlet weak var otpTextField: UITextField!
    var phoneNumber = ""
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func didTapVerifyButton(_ sender: UIButton) {
        let verificationID = UserDefaults.standard.string(forKey: "verificationID")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: otpTextField.text!)
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error == nil {
                print((authResult?.user.phoneNumber) ?? "Success")
                self.phoneNumber = (authResult?.user.phoneNumber) ?? "+919876543210"
                self.performSegue(withIdentifier: "ShowSignUp", sender: self)
            }else{
                print((error?.localizedDescription)!)
            }
        }
    }
    
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowSignUp"{
            if let vc = segue.destination as? SignUpViewController{
                vc.phoneNumber = phoneNumber
            }
        }
    }
    

}

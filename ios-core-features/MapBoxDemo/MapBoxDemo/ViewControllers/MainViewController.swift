//
//  MainViewController.swift
//  MapBoxDemo
//
//  Created by Hardik Trivedi on 11/01/19.
//  Copyright © 2019 Volansys Technologies Pvt Ltd. All rights reserved.
//

import UIKit
import Mapbox
import MapboxDirections
import MapboxCoreNavigation
import MapboxNavigation
import Floaty
import IQKeyboardManagerSwift
import MapboxGeocoder
import SVProgressHUD

class MainViewController: UIViewController {

    var currenLocation: CLLocationCoordinate2D!
    var chosenLocation: CLLocationCoordinate2D!
    var mapView: MGLMapView!
    var searchTextField: UITextField!
    var searchTableView: UITableView!
    var places = [GeocodedPlacemark]()
    let kZoomLevel = 14.5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        IQKeyboardManager.shared.enable = true
        setUpMapView()
        setUpFloatingButtons()
        setUpSearchThings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func showDirections(from source: CLLocationCoordinate2D, to specifiedLocation: CLLocationCoordinate2D){
        let origin = Waypoint(coordinate: source, name: "Origin")
        let destination = Waypoint(coordinate: specifiedLocation, name: "Destination")

        let options = NavigationRouteOptions(waypoints: [origin, destination])
        
        Directions.shared.calculate(options) { (waypoints, routes, error) in
            guard let route = routes?.first else { return }
            
            let viewController = NavigationViewController(for: route)
            viewController.showsReportFeedback = false
            viewController.showsEndOfRouteFeedback = false
            viewController.delegate = self
//            if simulationIsEnabled {
//                navigationController.routeController.locationManager = SimulatedLocationManager(route: route)
//            }
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func setUpMapView(){
        mapView = MGLMapView(frame: view.bounds)
        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        currenLocation = CLLocationCoordinate2D(latitude: 59.31, longitude: 18.06)
        mapView.setCenter(currenLocation, zoomLevel: kZoomLevel, animated: false)
        view.addSubview(mapView)
        
        mapView.attributionButton.isHidden = true
        mapView.locationManager.delegate = self
        
        mapView.showsUserLocation = true
        mapView.setUserTrackingMode(.follow, animated: true)
        
        
        mapView.locationManager.requestAlwaysAuthorization()
        mapView.locationManager.startUpdatingLocation()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(handleMapTap(sender:)))
        //singleTap.addTarget(self, action: #selector(handleMapTap(sender:)))
        //singleTap.senderView = mapView
        for recognizer in mapView.gestureRecognizers! where recognizer is UITapGestureRecognizer {
            singleTap.require(toFail: recognizer)
        }
        mapView.addGestureRecognizer(singleTap)
    }
    
    func setUpFloatingButtons(){
        let floaty = Floaty()
        floaty.buttonImage = #imageLiteral(resourceName: "Floaty")
        floaty.buttonColor = .white
        floaty.addItem("My Location", icon: #imageLiteral(resourceName: "GPS"), handler: { item in
            self.mapView.locationManager.startUpdatingLocation()
            self.mapView.setCenter(self.currenLocation, zoomLevel: self.kZoomLevel, animated: true)
        })
        floaty.addItem("Directions", icon: #imageLiteral(resourceName: "Directions"), handler: { item in
            if self.chosenLocation != nil{
                self.showDirections(from: self.currenLocation, to: self.chosenLocation)
            }else{
                SVProgressHUD.showInfo(withStatus: "Please provide another location..!!!")
            }
        })
        floaty.addItem("Google Maps", icon: #imageLiteral(resourceName: "GoogleMaps"), handler: { item in
            if self.chosenLocation != nil{
                if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
                    UIApplication.shared.open(URL(string:
                        "comgooglemaps://?center=\(self.chosenLocation.latitude),\(self.chosenLocation.longitude)&zoom=\(self.kZoomLevel)&views=traffic&daddr=\(self.chosenLocation.latitude),\(self.chosenLocation.longitude)")!,
                                              options: [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly : "com.volansys.mapboxdemo"],
                                              completionHandler: { (success) in
                                                if success{
                                                    print("Successful redirection to Google Maps App..!!")
                                                }
                    })
                } else {
                    print("Can not open comgooglemaps://, opening in browser...");
                    UIApplication.shared.open(URL(string: "http://maps.google.com/maps?f=d&daddr=\(self.chosenLocation.latitude),\(self.chosenLocation.longitude)&saddr=\(self.currenLocation.latitude),\(self.currenLocation.longitude)&zoom=\(self.kZoomLevel)&nav=1")!,
                                              options: [UIApplication.OpenExternalURLOptionsKey.universalLinksOnly : "com.volansys.mapboxdemo"],
                                              completionHandler: { (success) in
                                                if success{
                                                    print("Successful redirection to browser..!!")
                                                }
                    })
                }
            }else{
                print("Please provide another location..!!!")
            }
        })
        view.addSubview(floaty)
    }

    func setUpSearchThings(){
        searchTextField = UITextField()
        searchTextField.placeholder = "Search here ..."
        searchTextField.borderStyle = .roundedRect
        searchTextField.addTarget(self, action: #selector(textDidChange(of:)), for: .editingChanged)
        mapView.addSubview(searchTextField)
        
        searchTextField.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: searchTextField, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: searchTextField, attribute: NSLayoutConstraint.Attribute.centerY, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.centerY, multiplier: 0.2, constant: 0).isActive = true
        NSLayoutConstraint(item: searchTextField, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1, constant: 16).isActive = true
        NSLayoutConstraint(item: searchTextField, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1, constant: -16).isActive = true
        NSLayoutConstraint(item: searchTextField, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1, constant: 48).isActive = true
        
        searchTableView = UITableView()
        searchTableView.isHidden = true
        searchTableView.tableFooterView = UIView()
        searchTableView.allowsSelection = true
        searchTableView.dataSource = self
        searchTableView.delegate = self
        mapView.addSubview(searchTableView)
        
        searchTableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint(item: searchTableView, attribute: NSLayoutConstraint.Attribute.centerX, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.centerX, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: searchTableView, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: searchTextField, attribute: NSLayoutConstraint.Attribute.bottom, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: searchTableView, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1, constant: 16).isActive = true
        NSLayoutConstraint(item: searchTableView, attribute: NSLayoutConstraint.Attribute.right, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.right, multiplier: 1, constant: -16).isActive = true
        NSLayoutConstraint(item: searchTableView, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: mapView, attribute: NSLayoutConstraint.Attribute.height, multiplier: 0.4, constant: 0).isActive = true
    }
    
    @objc @IBAction func handleMapTap(sender: UITapGestureRecognizer) {
        //print(sender.senderView is MGLMapView)
        if !searchTextField.isEditing{
            // Convert tap location (CGPoint) to geographic coordinate (CLLocationCoordinate2D).
            let tapPoint: CGPoint = sender.location(in: mapView)
            let tapCoordinate: CLLocationCoordinate2D = mapView.convert(tapPoint, toCoordinateFrom: nil)
            print("You tapped at: \(tapCoordinate.latitude), \(tapCoordinate.longitude)")
            
            // Create an array of coordinates for our polyline, starting at the center of the map and ending at the tap coordinate.
            //let coordinates: [CLLocationCoordinate2D] = [mapView.centerCoordinate, tapCoordinate]
            
            // Remove any existing polyline(s) from the map.
            if mapView.annotations?.count != nil, let existingAnnotations = mapView.annotations {
                mapView.removeAnnotations(existingAnnotations)
            }
            
            let tappedLocation = MGLPointAnnotation()
            tappedLocation.coordinate = tapCoordinate
            chosenLocation = tapCoordinate
            tappedLocation.title = "Your Lcoation"
            mapView.addAnnotation(tappedLocation)
        }else{
            print("Selection from list of places")
        }
        
    }
    
    @objc func textDidChange(of textField: UITextField){
        let options = ForwardGeocodeOptions(query: textField.text!)
        
        options.focalLocation = CLLocation(latitude: currenLocation.latitude, longitude: currenLocation.longitude)
        options.allowedScopes = [.all]
        options.maximumResultCount = 10
        
        let task = Geocoder.shared.geocode(options) { (placemarks, attribution, error) in
            if error == nil, let placeMarks = placemarks{
                self.places = placeMarks
                self.searchTableView.reloadData()
            }else{
                print(error?.localizedDescription ?? "Error in places")
            }
        }
        task.resume()
        searchTableView.isHidden = textField.text?.count == 0
    }
    
    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
 

}
extension MainViewController: MGLLocationManagerDelegate{
    func locationManager(_ manager: MGLLocationManager, didUpdate locations: [CLLocation]) {
        if let location = locations.last{
            currenLocation = location.coordinate
            mapView.setCenter(location.coordinate, zoomLevel: kZoomLevel, animated: true)
        }else{
            SVProgressHUD.showInfo(withStatus: "GPS is off.\nLocation is not available.")
        }
    }
    
    func locationManager(_ manager: MGLLocationManager, didUpdate newHeading: CLHeading) {
        
    }
    
    func locationManagerShouldDisplayHeadingCalibration(_ manager: MGLLocationManager) -> Bool {
        return false
    }
    
    func locationManager(_ manager: MGLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
}
extension MainViewController: NavigationViewControllerDelegate{
    func navigationViewController(_ navigationViewController: NavigationViewController, didArriveAt waypoint: Waypoint) -> Bool {
        let alert = UIAlertController(title: "Arrived.", message: "You arrived at your location.\n(\(String(describing: waypoint.name ?? "Destination")))", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            navigationViewController.dismiss(animated: true, completion: nil)
        }))
        navigationViewController.present(alert, animated: true, completion: nil)
        
        return false
    }
    
}

extension MainViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return places.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: nil)
        cell.textLabel?.text = places[indexPath.row].name
        cell.detailTextLabel?.text = places[indexPath.row].qualifiedName
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Remove any existing polyline(s) from the map.
        if mapView.annotations?.count != nil, let existingAnnotations = mapView.annotations {
            mapView.removeAnnotations(existingAnnotations)
        }
        let tappedLocation = MGLPointAnnotation()
        tappedLocation.coordinate = (places[indexPath.row].location?.coordinate)!
        chosenLocation = places[indexPath.row].location?.coordinate
        tappedLocation.title = places[indexPath.row].qualifiedName
        mapView.addAnnotation(tappedLocation)
        mapView.setCenter(chosenLocation, zoomLevel: kZoomLevel, animated: true)
        searchTextField.text = ""
        self.view.endEditing(true)
        tableView.isHidden = true
    }
}

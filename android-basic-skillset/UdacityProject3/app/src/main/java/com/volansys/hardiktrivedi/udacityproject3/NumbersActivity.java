package com.volansys.hardiktrivedi.udacityproject3;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class NumbersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_numbers);

        //String words[] = {"one","two","three","four","five","six","seven","eight","nine","ten"};
        ArrayList<Word> word=new ArrayList<>();

        word.add(new Word("one","lutti"));
        word.add(new Word("two","ottiko"));
        word.add(new Word("three","tolukosu"));
        word.add(new Word("one","lutti"));
        word.add(new Word("one","lutti"));
        word.add(new Word("one","lutti"));
        word.add(new Word("one","lutti"));
        word.add(new Word("one","lutti"));
        word.add(new Word("one","lutti"));
        word.add(new Word("one","lutti"));
        WordAdapter itemsAdapter = new WordAdapter(this, word);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);
    }
}

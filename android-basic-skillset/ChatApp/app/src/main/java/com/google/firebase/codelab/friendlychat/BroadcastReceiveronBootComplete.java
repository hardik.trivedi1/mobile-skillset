package com.google.firebase.codelab.friendlychat;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by hardik on 17/1/18.
 */

public class BroadcastReceiveronBootComplete extends BroadcastReceiver {
    @Override

    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED)
                || intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_REPLACED)
                || intent.getAction().equalsIgnoreCase(Intent.ACTION_PACKAGE_ADDED)) {

            Intent serviceIntent = new Intent(context, MyFirebaseMessagingService.class);

            context.startService(serviceIntent);

        }

    }
}

package com.volansys.hardiktrivedi.broadcastreceivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Button mGenerateCustomBroadcastButton;
    private Button mGenerateLocalBroadcastButton;
    private TextView mMessageTextView;
    private LocalBroadcastManager mLocalBroadcastManager;

    @Override
    protected void onResume(){
        super.onResume();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Intent.ACTION_BATTERY_CHANGED);
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addAction(getPackageName() + ".CUSTOM_EVENT");

        SystemBroadcastReceiver systemBroadcastReceiver = new SystemBroadcastReceiver();
        registerReceiver(systemBroadcastReceiver,intentFilter);

        IntentFilter localIntentFilter = new IntentFilter();
        localIntentFilter.addAction("LOCAL_EVENT_SAMPLE_1");
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        mLocalBroadcastManager.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mMessageTextView.setText("Local event fired using LocalBroadcastManager");
            }
        }, localIntentFilter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mGenerateCustomBroadcastButton = findViewById(R.id.custom_broadcast_button);
        mMessageTextView = findViewById(R.id.message_text_view);
        mGenerateLocalBroadcastButton = findViewById(R.id.local_broadcast_button);

        mMessageTextView.setText(getIntent().getStringExtra("message"));

        mGenerateCustomBroadcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendBroadcast(new Intent(getPackageName() + ".CUSTOM_EVENT"));
            }
        });

        mGenerateLocalBroadcastButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent localBroadcastIntent = new Intent("LOCAL_EVENT_SAMPLE_1");
                mLocalBroadcastManager.sendBroadcast(localBroadcastIntent);
            }
        });


    }
}

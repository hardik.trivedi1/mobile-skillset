package com.volansys.hardiktrivedi.broadcastreceivers;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by hardik on 29/1/18.
 */

public class SystemBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Action is " + intent.getAction(), Toast.LENGTH_SHORT).show();
        if (intent.getAction()
                .equals("com.volansys.hardiktrivedi.broadcastreceivers.CUSTOM_EVENT")) {
            TextView temp = ((Activity) context).findViewById(R.id.message_text_view);
            temp.setText("Custom event fired.");
        }
        else if(intent.getAction().equals(Intent.ACTION_AIRPLANE_MODE_CHANGED)) {
            if (context instanceof Activity) {
                TextView temp = ((Activity) context).findViewById(R.id.message_text_view);
                temp.setText("Airplane mode changed.");
            } else
                context.startActivity(new Intent(context, MainActivity.class)
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("message", "Airplane mode changed."));
        }
    }
}

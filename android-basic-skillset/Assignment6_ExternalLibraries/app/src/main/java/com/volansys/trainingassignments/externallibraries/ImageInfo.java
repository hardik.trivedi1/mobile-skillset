package com.volansys.trainingassignments.externallibraries;

/**
 * Created by hardik on 8/2/18.
 */

public class ImageInfo {
    String image;

    public void setImage(String image){
        this.image = image;
    }
    public String getImage(){
        return image;
    }
}

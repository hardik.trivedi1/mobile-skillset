package com.volansys.trainingassignments.externallibraries;

import android.content.Context;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * VolleyInstance class provides Singleton pattern for accessing volley instances with Thread safety.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 08-02-2018
 */

public class VolleyInstance {

    private static VolleyInstance INSTANCE;
    private Context mContext;
    private RequestQueue mRequestQueue;

    private VolleyInstance(Context context){
        mContext = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized VolleyInstance getInstance(Context context){
        if(INSTANCE == null)
            INSTANCE = new VolleyInstance(context);
        return INSTANCE;
    }

    public RequestQueue getRequestQueue(){
        if(mRequestQueue == null)
            mRequestQueue = Volley.newRequestQueue(mContext);
        return mRequestQueue;
    }
}


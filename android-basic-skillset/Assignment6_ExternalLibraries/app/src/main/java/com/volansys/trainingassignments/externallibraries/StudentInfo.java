package com.volansys.trainingassignments.externallibraries;

/**
 * StudentInfo class used as POJO model in retrofit operations.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 08-02-2018
 */

public class StudentInfo {

    private String rollNo;
    private String name;
    private String email;

    public String getRollNo(){
        return rollNo;
    }
    public void setRollNo(String rollNo){
        this.rollNo = rollNo;
    }

    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name = name;
    }

    public String getEmail(){
        return email;
    }
    public void setEmail(String email){
        this.email = email;
    }
}

package com.volansys.trainingassignments.externallibraries;

import android.graphics.Bitmap;

import java.util.List;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * ApiInterface, for insertStudentInfo for requesting with various methods.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 08-02-2018
 */

public interface ApiInterface {

    @FormUrlEncoded
    @POST("/scripts/insertStudent.php")
    void insertStudentInfo(@Field("rollno") String rollNo,
                           @Field("name") String name,
                           @Field("email") String email,
                           Callback<StudentInfo> callback);

    @GET("/scripts/getStudentsByName.php")
    void retrieveStudentInfo(@Query("rollno") String rollNo, Callback<List<StudentInfo>> callback);

    @GET("/scripts/getImage.php")
    void showImage(Callback<ImageInfo> callback);
}

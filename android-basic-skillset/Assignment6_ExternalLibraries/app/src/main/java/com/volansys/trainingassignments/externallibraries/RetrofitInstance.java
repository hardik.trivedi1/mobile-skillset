package com.volansys.trainingassignments.externallibraries;

import retrofit.RestAdapter;

/**
 * RetrofitInstance class is used to create RestAdapter and client for network operations.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 08-02-2018
 */

public class RetrofitInstance {

    public static ApiInterface getClient() {

        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.3.104/")
                .build();

        ApiInterface api = adapter.create(ApiInterface.class);
        return api;
    }
}


package com.volansys.trainingassignments.externallibraries;

import android.app.Application;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;

/**
 * This is a subclass of {@link Application} used to provide shared objects for this app, such as
 * the {@link Tracker}.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 09-02-2018
 */

public class GoogleAnalyticsApplication extends Application {

    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();

        sAnalytics = GoogleAnalytics.getInstance(this);
    }

    /**
     * Gets the default {@link Tracker} for this {@link Application}.
     * @return tracker
     */
    synchronized public Tracker getDefaultTracker() {
        if (sTracker == null) {
            sTracker = sAnalytics.newTracker("UA-113882583-1");
        }
        sTracker.enableExceptionReporting(true);
        sTracker.enableAutoActivityTracking(true);
        return sTracker;
    }

}

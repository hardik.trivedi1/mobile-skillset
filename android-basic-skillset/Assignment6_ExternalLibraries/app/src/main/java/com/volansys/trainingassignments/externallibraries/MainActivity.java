package com.volansys.trainingassignments.externallibraries;

import android.app.AlertDialog;
import android.content.AsyncTaskLoader;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.apsalar.sdk.Apsalar;
import com.apsalar.sdk.ApsalarConfig;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * MainActivity, having buttons for extracting app's features.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 08-02-2018
 */

public class MainActivity extends AppCompatActivity {

    private static final String[] HTTP_ACTIONS = new String[]{"Insert", "Retrieve"};
    private static final String[] VOLLEY_ACTIONS = new String[]{"Insert", "Retrieve", "Image"};
    private static final String[] RETROFIT_ACTONS = new String[]{"Insert", "Retrieve", "Image"};

    private Button mShowHttpMethodsButton;
    private Button mShowVolleyFeaturesButon;
    private String mRetrievedData;
    private RequestQueue mRequestQueue;
    private Button mShowRetrofitv1Features;
    private Button mCrashForCrashlytics;
    private Button mSendEventForGoogleAnalyticsButton;
    private Button mSendEventForApslarButton;
    private Button mSendEventForFirebase;

    private Tracker mTracker;

    private FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();
        clickEvents();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.v("MainActivity", "tracking MainActivity");
        mTracker.setScreenName("MainActivity");
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    private void initialization() {
        mShowHttpMethodsButton = findViewById(R.id.button_http_post_get);
        mShowVolleyFeaturesButon = findViewById(R.id.button_volley);
        mRequestQueue = VolleyInstance.getInstance(this).getRequestQueue();
        mShowRetrofitv1Features = findViewById(R.id.button_retrofit_1);
        mCrashForCrashlytics = findViewById(R.id.button_crashlytics);

        Fabric.with(this, new Crashlytics());

        GoogleAnalyticsApplication application = (GoogleAnalyticsApplication) getApplication();
        mTracker = application.getDefaultTracker();

        mSendEventForGoogleAnalyticsButton = findViewById(R.id.button_google_analytics);
        mSendEventForApslarButton = findViewById(R.id.button_apslar);

        ApsalarConfig config = new ApsalarConfig("hardiktrivedi","Wo0cYyCc");
        Apsalar.init(this,config);

        mSendEventForFirebase = findViewById(R.id.button_firebase);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    private void clickEvents() {
        mShowHttpMethodsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setItems(HTTP_ACTIONS, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (HTTP_ACTIONS[i].equals("Insert")) {
                                    insertStudentData();
                                } else if (HTTP_ACTIONS[i].equals("Retrieve")) {
                                    retrieveStudentData();
                                }
                            }
                        })
                        .create().show();
            }
        });

        mShowVolleyFeaturesButon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setItems(VOLLEY_ACTIONS, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (VOLLEY_ACTIONS[i].equals("Insert")) {
                                    insertStudentDataByVolley();
                                } else if (VOLLEY_ACTIONS[i].equals("Retrieve")) {
                                    retrieveStudentDataByVolley();
                                } else if (VOLLEY_ACTIONS[i].equals("Image")) {
                                    showImageByVolley();
                                }
                            }
                        })
                        .create().show();
            }
        });

        mShowRetrofitv1Features.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setItems(RETROFIT_ACTONS, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (RETROFIT_ACTONS[i].equals("Insert")) {
                                    insertStudentDataByRetrofit1();
                                } else if (RETROFIT_ACTONS[i].equals("Retrieve")) {
                                    retrieveStudentDataByRetrofit1();
                                } else if (RETROFIT_ACTONS[i].equals("Image")) {
                                    showImageByRetrofit1();
                                }
                            }
                        })
                        .create().show();
            }
        });

        mCrashForCrashlytics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Crashlytics Actions")
                        .setPositiveButton("Event", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Answers.getInstance()
                                        .logCustom(new CustomEvent("Event from Crashlytics button")
                                        .putCustomAttribute("name","Hardik")
                                        .putCustomAttribute("number",786));
                            }
                        })
                        .setNegativeButton("Crash", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                throw new RuntimeException("Making crash for analysis");
                            }
                        })
                        .setNeutralButton("Leave", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
            }
        });

        mSendEventForGoogleAnalyticsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTracker.send(new HitBuilders.EventBuilder()
                        .setCategory("Button clicks")
                        .setAction("Send")
                        .setLabel("Send events for Google analytics button clicked.")
                        .build());
            }
        });

        mSendEventForApslarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Apsalar.event("from send event button");
            }
        });

        mSendEventForFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putString("eventName","send firebase an event");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            }
        });
    }

    private void insertStudentData() {
        final View customView = LayoutInflater.from(MainActivity.this)
                .inflate(R.layout.custom_dialog_view, null, false);
        new AlertDialog.Builder(MainActivity.this)
                .setView(customView)
                .setPositiveButton("Insert", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        insertStudent(customView);
                    }
                })
                .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    private void insertStudent(View rootView) {
        final EditText rollnoEditText = rootView.findViewById(R.id.editText_rollno);
        final EditText nameEditText = rootView.findViewById(R.id.editText_name);
        final EditText emailEditText = rootView.findViewById(R.id.editText_email);

        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case 200:
                        Toast.makeText(MainActivity.this,
                                "Student mRetrievedData inserted successfully.",
                                Toast.LENGTH_SHORT).show();
                        break;
                    case 500:
                        Toast.makeText(MainActivity.this,
                                "Server encountered an error.\nPlease insert again.",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        };

        class InsertData extends AsyncTaskLoader<Void> {

            InsertData(Context context) {
                super(context);
            }

            @Override
            public Void loadInBackground() {

                HashMap<String, String> params = new HashMap<>();
                params.put("rollno", rollnoEditText.getText().toString());
                params.put("name", nameEditText.getText().toString());
                params.put("email", emailEditText.getText().toString());
                if (sendPostRequest("http://192.168.3.104/scripts/insertStudent.php", params).equals("okay"))
                    handler.sendEmptyMessage(200);
                else
                    handler.sendEmptyMessage(500);


                return null;
            }
        }
        new InsertData(this).forceLoad();
    }

    private String sendPostRequest(String requestURL, HashMap<String, String> postDataParams) {
        URL url;
        StringBuilder sb = new StringBuilder();
        try {
            url = new URL(requestURL);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();

            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));

            writer.write(getPostDataString(postDataParams));
            writer.close();

            if (connection.getResponseCode() == HttpsURLConnection.HTTP_OK) {

                BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                sb = new StringBuilder();
                String response;
                while ((response = br.readLine()) != null) {
                    sb.append(response);
                }
            }

        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
        return sb.toString();
    }

    private String getPostDataString(HashMap<String, String> params)
            throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    private void retrieveStudentData() {
        final EditText queryEditText = new EditText(MainActivity.this);
        queryEditText.setHint("Enter number here...");
        queryEditText.setLayoutParams(new RelativeLayout.LayoutParams(200, 50));
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Find student by number")
                .setView(queryEditText)
                .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        retrieveData(queryEditText.getText().toString());
                    }
                })
                .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    private void retrieveData(final String queryNo) {
        mRetrievedData = "";
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Inquired data")
                        .setMessage(mRetrievedData)
                        .create().show();
            }
        };
        class RetrieveData extends AsyncTaskLoader<Void> {
            RetrieveData(Context context) {
                super(context);
            }

            @Override
            public Void loadInBackground() {
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL("http://192.168.3.104/scripts/getStudentsByName.php?rollno=" + queryNo)
                                    .openConnection();
                    connection.connect();
                    JSONArray dataArray = new JSONArray(getJSONString(connection.getInputStream()));
                    for (int i = 0; i < dataArray.length(); i++) {
                        mRetrievedData = mRetrievedData +
                                "\nRoll no.: " +
                                dataArray.getJSONObject(i).getString("rollno") +
                                "\nName : " +
                                dataArray.getJSONObject(i).getString("name") +
                                "\nEmail : " +
                                dataArray.getJSONObject(i).getString("email");
                    }
                    handler.sendEmptyMessage(0);

                } catch (MalformedURLException e) {
                } catch (IOException e) {
                } catch (JSONException e) {
                }
                return null;
            }

            private String getJSONString(InputStream is) throws IOException {
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String temp;
                while ((temp = reader.readLine()) != null) {
                    sb.append(temp + "\n");
                }
                return sb.toString();
            }

        }

        new RetrieveData(this).forceLoad();
    }

    private void insertStudentDataByVolley() {
        final View customView = LayoutInflater.from(MainActivity.this)
                .inflate(R.layout.custom_dialog_view, null, false);
        new AlertDialog.Builder(MainActivity.this)
                .setView(customView)
                .setPositiveButton("Insert", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        insertStudentByVolley(customView);
                    }
                })
                .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    private void insertStudentByVolley(View rootView) {
        final EditText rollnoEditText = rootView.findViewById(R.id.editText_rollno);
        final EditText nameEditText = rootView.findViewById(R.id.editText_name);
        final EditText emailEditText = rootView.findViewById(R.id.editText_email);

        StringRequest request = new StringRequest(Request.Method.POST,
                "http://192.168.3.104/scripts/insertStudent.php",
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if (response.equals("okay"))
                            Toast.makeText(MainActivity.this,
                                    "Student data added successfully",
                                    Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(MainActivity.this,
                                    "Server encountered an error.\nPlease try again.",
                                    Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                HashMap<String, String> params = new HashMap<>();
                params.put("rollno", rollnoEditText.getText().toString());
                params.put("name", nameEditText.getText().toString());
                params.put("email", emailEditText.getText().toString());

                return params;
            }
        };

        mRequestQueue.add(request);
    }

    private void retrieveStudentDataByVolley() {
        final EditText queryEditText = new EditText(MainActivity.this);
        queryEditText.setHint("Enter number here...");
        queryEditText.setLayoutParams(new RelativeLayout.LayoutParams(100, 50));
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Find student by number")
                .setView(queryEditText)
                .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        retrieveDataByVolley(queryEditText.getText().toString());
                    }
                })
                .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    private void retrieveDataByVolley(final String queryNo) {
        mRetrievedData = "";
        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET,
                "http://192.168.3.104/scripts/getStudentsByName.php?rollno=" + queryNo,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                mRetrievedData = mRetrievedData +
                                        "\nRoll no.: " +
                                        response.getJSONObject(i).getString("rollno") +
                                        "\nName : " +
                                        response.getJSONObject(i).getString("name") +
                                        "\nEmail : " +
                                        response.getJSONObject(i).getString("email");
                            }
                        } catch (JSONException e) {
                        }
                        new AlertDialog.Builder(MainActivity.this)
                                .setTitle("Inquired data")
                                .setMessage(mRetrievedData)
                                .create().show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,
                                "Error occured!!!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
        mRequestQueue.add(request);
    }

    private void showImageByVolley() {
        ImageRequest request = new ImageRequest("http://192.168.3.104/images/image4.jpg",
                new Response.Listener<Bitmap>() {
                    @Override
                    public void onResponse(Bitmap response) {
                        ImageView imageView = new ImageView(MainActivity.this);
                        imageView.setImageBitmap(response);
                        new AlertDialog.Builder(MainActivity.this)
                                .setView(imageView)
                                .create().show();
                    }
                },
                1000,
                1000,
                ImageView.ScaleType.FIT_CENTER,
                Bitmap.Config.ARGB_4444,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,
                                "Error occured!!!",
                                Toast.LENGTH_SHORT).show();
                    }
                });

        mRequestQueue.add(request);
    }

    private void insertStudentDataByRetrofit1() {
        final View customView = LayoutInflater.from(MainActivity.this)
                .inflate(R.layout.custom_dialog_view, null, false);
        new AlertDialog.Builder(MainActivity.this)
                .setView(customView)
                .setPositiveButton("Insert", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        insertStudentByRetrofit1(customView);
                    }
                })
                .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    private void insertStudentByRetrofit1(View rootView) {
        final EditText rollnoEditText = rootView.findViewById(R.id.editText_rollno);
        final EditText nameEditText = rootView.findViewById(R.id.editText_name);
        final EditText emailEditText = rootView.findViewById(R.id.editText_email);

        RetrofitInstance.getClient().insertStudentInfo(rollnoEditText.getText().toString(),
                nameEditText.getText().toString(),
                emailEditText.getText().toString(),
                new Callback<StudentInfo>() {
                    @Override
                    public void success(StudentInfo studentInfo, retrofit.client.Response response) {
                        if (response.getStatus() == 200)
                            Toast.makeText(MainActivity.this,
                                    "Student added successfully.",
                                    Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(MainActivity.this,
                                    "Server encountered an error.\nPlease try again.",
                                    Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        if (error.getMessage().contains("BEGIN_OBJECT"))
                            Toast.makeText(MainActivity.this,
                                    "Student added successfully.",
                                    Toast.LENGTH_SHORT).show();
                        else
                            Toast.makeText(MainActivity.this,
                                    "Error occured.",
                                    Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void retrieveStudentDataByRetrofit1() {
        final EditText queryEditText = new EditText(MainActivity.this);
        queryEditText.setHint("Enter number here...");
        queryEditText.setLayoutParams(new RelativeLayout.LayoutParams(100, 50));
        new AlertDialog.Builder(MainActivity.this)
                .setTitle("Find student by number")
                .setView(queryEditText)
                .setPositiveButton("Search", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        retrieveDataByRetrofit1(queryEditText.getText().toString());
                    }
                })
                .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                })
                .create().show();
    }

    private void retrieveDataByRetrofit1(final String queryNo) {
        mRetrievedData = "";
        RetrofitInstance.getClient().retrieveStudentInfo(queryNo, new Callback<List<StudentInfo>>() {
            @Override
            public void success(List<StudentInfo> studentInfos, retrofit.client.Response response) {
                if (response.getStatus() == 200) {
                    for (StudentInfo student : studentInfos)
                        mRetrievedData = mRetrievedData +
                                "\nRoll no.: " +
                                queryNo +
                                "\nName : " +
                                student.getName() +
                                "\nEmail : " +
                                student.getEmail();
                    new AlertDialog.Builder(MainActivity.this)
                            .setTitle("Inquired data")
                            .setMessage(mRetrievedData)
                            .create().show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this,
                        "Some error occured.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void showImageByRetrofit1() {
        RetrofitInstance.getClient().showImage(new Callback<ImageInfo>() {
            @Override
            public void success(ImageInfo info, retrofit.client.Response response) {
                ImageView imageView = new ImageView(MainActivity.this);
                imageView.setMaxHeight(1000);
                imageView.setMaxWidth(1000);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

                byte[] tempImage = Base64.decode(info.getImage(), 0);
                imageView.setImageBitmap(BitmapFactory.decodeByteArray(tempImage, 0, tempImage.length));


                new AlertDialog.Builder(MainActivity.this)
                        .setView(imageView)
                        .create().show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MainActivity.this,
                        error.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}

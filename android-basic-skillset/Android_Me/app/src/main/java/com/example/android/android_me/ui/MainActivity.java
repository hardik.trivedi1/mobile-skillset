package com.example.android.android_me.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

public class MainActivity extends AppCompatActivity implements MasterListFragment.OnImageClickListener{

    private int headIndex,bodyIndex,legsIndex;

    private boolean mTwoPane;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GridView gd=findViewById(R.id.images_grid_view);
        gd.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                onImageSelected(i);
            }
        });

        if(findViewById(R.id.android_me_linear_layout)!=null)
        {
            mTwoPane=true;

            Button next = findViewById(R.id.next_button);
            next.setVisibility(View.GONE);

            gd.setNumColumns(2);
            if(savedInstanceState == null) {
                FragmentManager fragmentManager = getSupportFragmentManager();

                BodyPartFragment headFragment = new BodyPartFragment();
                headFragment.setmImageIds(AndroidImageAssets.getHeads());
                headFragment.setmListIndex(getIntent().getIntExtra("head",0));
                fragmentManager.beginTransaction().add(R.id.head_container, headFragment).commit();

                BodyPartFragment bodyFragment = new BodyPartFragment();
                bodyFragment.setmImageIds(AndroidImageAssets.getBodies());
                bodyFragment.setmListIndex(getIntent().getIntExtra("body",0));
                fragmentManager.beginTransaction().add(R.id.body_container, bodyFragment).commit();

                BodyPartFragment legsFragment = new BodyPartFragment();
                legsFragment.setmImageIds(AndroidImageAssets.getLegs());
                legsFragment.setmListIndex(getIntent().getIntExtra("legs",0));
                fragmentManager.beginTransaction().add(R.id.legs_container, legsFragment).commit();
            }
        }
        else {
            mTwoPane = false;
        }
    }



    @Override
    public void onImageSelected(int position) {
        int bodyPartNumber = position/12;
        int listIndex = position - 12*bodyPartNumber;

        if(mTwoPane)
        {
            BodyPartFragment bodyPartFragment = new BodyPartFragment();
            switch (bodyPartNumber){
                case 0:
                    bodyPartFragment.setmImageIds(AndroidImageAssets.getHeads());
                    bodyPartFragment.setmListIndex(listIndex);
                    getSupportFragmentManager().beginTransaction().replace(R.id.head_container,bodyPartFragment).commit();
                    break;

                case 1:
                    bodyPartFragment.setmImageIds(AndroidImageAssets.getBodies());
                    bodyPartFragment.setmListIndex(listIndex);
                    getSupportFragmentManager().beginTransaction().replace(R.id.body_container,bodyPartFragment).commit();
                    break;

                case 2:
                    bodyPartFragment.setmImageIds(AndroidImageAssets.getLegs());
                    bodyPartFragment.setmListIndex(listIndex);
                    getSupportFragmentManager().beginTransaction().replace(R.id.legs_container,bodyPartFragment).commit();
                    break;

                default:break;
            }
        }
        else
        {
            switch (bodyPartNumber)
            {
                case 0: headIndex=listIndex;
                    break;
                case 1: bodyIndex=listIndex;
                    break;
                case 2: legsIndex=listIndex;
                    break;
                default:break;
            }
        }


        final Intent intent = new Intent(this,AndroidMeActivity.class);
        intent.putExtra("head",headIndex);
        intent.putExtra("body",bodyIndex);
        intent.putExtra("legs",legsIndex);

        Button next=findViewById(R.id.next_button);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(intent);
            }
        });
    }
}
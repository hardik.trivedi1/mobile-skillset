package com.example.android.android_me.ui;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.android.android_me.R;
import com.example.android.android_me.data.AndroidImageAssets;

import java.util.ArrayList;
import java.util.List;

public class BodyPartFragment extends Fragment
{
    private String TAG="BodyPartFragment";
    public BodyPartFragment(){}

    private List<Integer> mImageIds;
    private int mListIndex;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if(savedInstanceState !=null)
        {
            mImageIds = savedInstanceState.getIntegerArrayList("list");
            mListIndex = savedInstanceState.getInt("index");
        }
        View rootView = inflater.inflate(R.layout.fragment_body_part,container,false);

        final ImageView bodyPart = rootView.findViewById(R.id.body_part_image_view);
        if(mImageIds!=null)
            bodyPart.setImageResource(mImageIds.get(mListIndex));
        else
            Log.v(TAG,"Incorrect Resource");

        bodyPart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mListIndex<mImageIds.size()-1)
                    mListIndex++;
                else
                    mListIndex=0;

                bodyPart.setImageResource(mImageIds.get(mListIndex));
            }
        });

        return rootView;
    }

    public void setmImageIds(List<Integer> imageIds)
    {
        mImageIds=imageIds;
    }
    public void setmListIndex(Integer listIndex)
    {
        mListIndex=listIndex;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putIntegerArrayList("list", (ArrayList<Integer>)mImageIds);
        outState.putInt("index",mListIndex);
    }
}
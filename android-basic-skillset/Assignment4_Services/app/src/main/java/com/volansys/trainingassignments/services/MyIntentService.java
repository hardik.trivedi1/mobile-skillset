package com.volansys.trainingassignments.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * MyIntentService for handling service calls in queue handling mechanism.
 * @author Hardik Trivedi
 * @version 1.0
 * @since 12-02-2018
 */
public class MyIntentService extends IntentService {

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String incomingMessage = intent.getStringExtra("input");
        Log.w("MyIntentService","before sleep");
        SystemClock.sleep(3000);
        Log.w("MyIntentService","after sleep");
        String outputMessage = "Hello " + incomingMessage;
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(ResponseReceiver.ACTION_RESP);
        broadcastIntent.putExtra("output",outputMessage);
        sendBroadcast(broadcastIntent);
    }
}

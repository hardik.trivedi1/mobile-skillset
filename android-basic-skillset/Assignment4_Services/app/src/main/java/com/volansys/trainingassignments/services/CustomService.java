package com.volansys.trainingassignments.services;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * CustomService is used to run our tasks by services.
 * @author Hardik Trivedi
 * @since 05-02-2018
 * @version 1.0
 */

public class CustomService extends Service {

    private final IBinder mBinder = new LocalBinder();
    private static final String TAG = "CustomService";
    private Bitmap mImageBmp;

    public class LocalBinder extends Binder {
        CustomService getService() {
            return CustomService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG,"from onCreate()");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.v(TAG,"from onStartCommand()");
        if(intent.getAction().equals("foreground")){
            PendingIntent notifiactionIntent = PendingIntent.getActivity(getApplicationContext(),
                    0,
                    new Intent(this,MainActivity.class)
                            .putExtra("foreground","From foreground service"),
                    0);
            Notification notification = new Notification.Builder(this)
                    .setContentTitle("Foreground service is running.")
                    .setContentText("Foreground service is started from MainActivity.")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(notifiactionIntent)
                    .setTicker("It's running.")
                    .setOngoing(true)
                    .build();
            startForeground(1,notification);
        }
        else if(intent.getAction().equals("background")){
            Toast.makeText(this, "Background service", Toast.LENGTH_SHORT).show();
        }
        return Service.START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.v(TAG,"from onBind()");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG,"from onDestroy()");
    }

    public void downloadImage(final ImageView imageView){
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                imageView.setImageBitmap(mImageBmp);
                stopSelf();
                Log.v(TAG,"self stopped.");
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL(getResources().getString(R.string.url))
                                    .openConnection();
                    connection.connect();
                    mImageBmp = BitmapFactory.decodeStream(connection.getInputStream());
                    handler.sendEmptyMessage(0);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
            }
        }).start();
    }

}

package com.volansys.trainingassignments.services;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

/**
 * MainActivity is for starting and stopping various types of services.
 *
 * @author Hardik Trivedi
 * @since 05-02-2018
 * @version 1.0
 */

public class MainActivity extends AppCompatActivity {

    private Button mStartForegroundServiceButton;
    private Button mStopForegroundServiceButton;
    private Button mStartBackgroundServiceButton;
    private Button mStopBackgroundServiceButton;
    private Button mShowBoundServiceButton;
    private CustomService mService;
    private boolean mBound = false;
    private EditText mTakeNameEditText;
    private Button mGreetUsingIntentService;
    private Button mNfcReaderButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(getIntent().getStringExtra("foreground") != null)
            Toast.makeText(this,
                    getIntent().getStringExtra("foreground"),
                    Toast.LENGTH_SHORT).show();

        initialization();

        clickEvents();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(this, CustomService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mConnection);

        mBound = false;
    }

    private void initialization() {
        mStartForegroundServiceButton = findViewById(R.id.button_start_foreground);
        mStopForegroundServiceButton = findViewById(R.id.button_stop_foreground);
        mStartBackgroundServiceButton = findViewById(R.id.button_start_background);
        mStopBackgroundServiceButton = findViewById(R.id.button_stop_background);
        mShowBoundServiceButton = findViewById(R.id.button_show_bound);
        mTakeNameEditText = findViewById(R.id.editText_name);
        mGreetUsingIntentService = findViewById(R.id.button_start_intent_service);
        mNfcReaderButton = findViewById(R.id.button_nfc_tag_reader);
    }

    private void clickEvents(){
        mStartForegroundServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(MainActivity.this,CustomService.class)
                                                                        .setAction("foreground"));
            }
        });

        mStopForegroundServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(MainActivity.this,CustomService.class)
                                                                        .setAction("foreground"));
            }
        });

        mStartBackgroundServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startService(new Intent(MainActivity.this,CustomService.class)
                                                                        .setAction("background"));
            }
        });

        mStopBackgroundServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService(new Intent(MainActivity.this,CustomService.class)
                                                                        .setAction("background"));
            }
        });

        mShowBoundServiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imageView = new ImageView(MainActivity.this);
                mService.downloadImage(imageView);
                new AlertDialog.Builder(MainActivity.this)
                        .setView(imageView)
                        .create().show();
            }
        });

        mGreetUsingIntentService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,MyIntentService.class);
                intent.putExtra("input",mTakeNameEditText.getText().toString());
                startService(intent);
                Log.w("MainActivity","onClick()");
            }
        });

        mNfcReaderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,NfcReaderActivity.class));
            }
        });
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            CustomService.LocalBinder binder = (CustomService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


}

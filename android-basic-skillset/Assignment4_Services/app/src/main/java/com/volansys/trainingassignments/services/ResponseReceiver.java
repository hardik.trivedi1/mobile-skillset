package com.volansys.trainingassignments.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

/**
 * ResponseReceiver is used to inform UI thread that MyIntentService has completed a task.
 * @author Hardik Trivedi
 * @version 1.0
 * @since 12-02-2018
 */

public class ResponseReceiver extends BroadcastReceiver {
    public static final String ACTION_RESP =
            "com.volansys.services.MESSAGE_PROCESSED";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.w("ResponseReceiver","onReceice()");
        Toast.makeText(context, intent.getStringExtra("output"), Toast.LENGTH_SHORT).show();
    }
}

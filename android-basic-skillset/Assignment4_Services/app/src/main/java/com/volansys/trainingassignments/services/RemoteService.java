package com.volansys.trainingassignments.services;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.util.Log;


/**
 * RemoteService class which provides method access to other processes as well.
 * @author Hardik Trivedi
 * @version 1.0
 * @since 12-02-2018
 */

public class RemoteService extends Service {

   private IRemoteService.Stub mBinder = new IRemoteService.Stub() {
       @Override
       public String getMessage(String name) throws RemoteException {
           Log.w("RemoteService","getMessage() called");
           return "Hello " + name;
       }
   };

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

}

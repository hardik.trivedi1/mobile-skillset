// IRemoteService.aidl
package com.volansys.trainingassignments.services;

// Declare any non-default types here with import statements

interface IRemoteService {
    /**
     * IRemoteService is an interface for performing IPC for local process as well as other app's
     * processes.
     * It has method sortData which sorts data in ascending order
     * @auther Hardik Trivedi
     * @version 1.0
     * @since 12-02-2018
     *
     */

   String getMessage(String name);

}

package com.volansys.touchgestures;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class SensorManagerExampleActivity extends AppCompatActivity {

    private SensorManager mSensorManager;
    private TextView mSensorsTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_manager_example);

        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensorsTextView = findViewById(R.id.textView_sensors);

        List<Sensor> sensors = mSensorManager.getSensorList(Sensor.TYPE_ALL);
        for (Sensor sensor : sensors){
            mSensorsTextView.append("\n" + sensor.getName());
        }

        Sensor magnetoMeter = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED);
        mSensorManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent sensorEvent) {
                Toast.makeText(SensorManagerExampleActivity.this, "Magnetic Field : " + sensorEvent.values[0], Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onAccuracyChanged(Sensor sensor, int i) {
            }
        },magnetoMeter,SensorManager.SENSOR_DELAY_NORMAL);
    }
}

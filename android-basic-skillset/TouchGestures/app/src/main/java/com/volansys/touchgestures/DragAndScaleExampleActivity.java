package com.volansys.touchgestures;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageButton;

import static android.view.MotionEvent.INVALID_POINTER_ID;

public class DragAndScaleExampleActivity extends AppCompatActivity
        implements ScaleGestureDetector.OnScaleGestureListener,View.OnTouchListener{

    private ScaleGestureDetector mScaleDetector;
    private ImageButton mButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drag_and_scale_example);

        mScaleDetector = new ScaleGestureDetector(this,this);

        mButton = findViewById(R.id.imageButton);
        mButton.setOnTouchListener(this);
    }

    // The ‘active pointer’ is the one currently moving our object.
    private int mActivePointerId = INVALID_POINTER_ID;
    float mLastTouchX,mLastTouchY,mPosX,mPosY;
    @Override
    public boolean onTouch(View view,MotionEvent ev) {
        // Let the ScaleGestureDetector inspect all events.
        mScaleDetector.onTouchEvent(ev);

        final int action = MotionEventCompat.getActionMasked(ev);

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                final int pointerIndex = MotionEventCompat.getActionIndex(ev);
                final float x = MotionEventCompat.getX(ev, pointerIndex);
                final float y = MotionEventCompat.getY(ev, pointerIndex);

                // Remember where we started (for dragging)
                mLastTouchX = x;
                mLastTouchY = y;
                // Save the ID of this pointer (for dragging)
                mActivePointerId = MotionEventCompat.getPointerId(ev, 0);
                Log.d("Dragging","Dragging started from X:" + mLastTouchX + " Y:" + mLastTouchY);
                mButton.setLeft((int)mLastTouchX); mButton.setTop((int) mLastTouchY);
                mButton.setRight((int)mLastTouchX + 100); mButton.setBottom((int) mLastTouchY + 100);
                break;
            }

            case MotionEvent.ACTION_MOVE: {
                // Find the index of the active pointer and fetch its position
                final int pointerIndex =
                        MotionEventCompat.findPointerIndex(ev, mActivePointerId);

                final float x = MotionEventCompat.getX(ev, pointerIndex);
                final float y = MotionEventCompat.getY(ev, pointerIndex);

                // Calculate the distance moved
                final float dx = x - mLastTouchX;
                final float dy = y - mLastTouchY;

                mPosX += dx;
                mPosY += dy;

                //invalidate();

                // Remember this touch position for the next move event
                mLastTouchX = x;
                mLastTouchY = y;
                Log.d("Dragging","Dragging...\nDifference X:" + mPosX + " Y:" + mPosY);
                mButton.setLeft((int)mLastTouchX); mButton.setTop((int) mLastTouchY);
                mButton.setRight((int)mLastTouchX + 400); mButton.setBottom((int) mLastTouchY + 400);
                break;
            }

            case MotionEvent.ACTION_UP: {
                mActivePointerId = INVALID_POINTER_ID;
                Log.d("Dragging","Dragging done. We are at X:" + mLastTouchX + " Y:" + mLastTouchY);
                mButton.setLeft((int)mLastTouchX); mButton.setTop((int) mLastTouchY);
                mButton.setRight((int)mLastTouchX + 400); mButton.setBottom((int) mLastTouchY + 400);
                break;
            }

            case MotionEvent.ACTION_CANCEL: {
                mActivePointerId = INVALID_POINTER_ID;
                break;
            }

            case MotionEvent.ACTION_POINTER_UP: {

                final int pointerIndex = MotionEventCompat.getActionIndex(ev);
                final int pointerId = MotionEventCompat.getPointerId(ev, pointerIndex);

                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchX = MotionEventCompat.getX(ev, newPointerIndex);
                    mLastTouchY = MotionEventCompat.getY(ev, newPointerIndex);
                    mActivePointerId = MotionEventCompat.getPointerId(ev, newPointerIndex);
                }
                Log.d("Dragging","Dragging done. We are at X:" + mLastTouchX + " Y:" + mLastTouchY);
                break;
            }
        }
        return true;
    }


    @Override
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {

    }

}

package com.volansys.touchgestures;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.security.Permission;
import java.util.ArrayList;

public class BluetoothExampleActivity extends AppCompatActivity {

    private Button mOn,mOff,mMakeVisible,mShowDevices;
    private ListView mListOfDevices;
    private BluetoothAdapter mBLEAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bluetooth_example);

        mBLEAdapter = BluetoothAdapter.getDefaultAdapter();

        mListOfDevices = findViewById(R.id.listView_devices);

        mOn = findViewById(R.id.button_on);
        mOn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!mBLEAdapter.isEnabled()) {
                    Intent turnOn = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    startActivityForResult(turnOn, 0);
                    Toast.makeText(getApplicationContext(), "Bluetooth turned on.",Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), "Blutooth already on.", Toast.LENGTH_LONG).show();
                }
            }
        });

        mOff = findViewById(R.id.button_off);
        mOff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mBLEAdapter.disable();
                Toast.makeText(BluetoothExampleActivity.this, "Bluetooth turned OFF.", Toast.LENGTH_SHORT).show();
            }
        });

        mMakeVisible = findViewById(R.id.button_make_visible);
        mMakeVisible.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent getVisible = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
                startActivityForResult(getVisible, 0);
            }
        });

        mShowDevices = findViewById(R.id.button_show_devices);
        mShowDevices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ArrayList<String> devices = new ArrayList<>();
                for (BluetoothDevice device : mBLEAdapter.getBondedDevices()){
                    devices.add("Name : " + device.getName() + "\nAddress : " + device.getAddress());
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(BluetoothExampleActivity.this,
                        R.layout.support_simple_spinner_dropdown_item,
                        devices);

                mListOfDevices.setAdapter(adapter);
            }
        });

        mListOfDevices.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
               final Handler handler = new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        Toast.makeText(BluetoothExampleActivity.this, "Data sent.", Toast.LENGTH_SHORT).show();
                    }
                };
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        BluetoothDevice device = (BluetoothDevice) mBLEAdapter.getBondedDevices().toArray()[i];
                        try {
                            BluetoothSocket socket = device.createRfcommSocketToServiceRecord(device.getUuids()[0].getUuid());
                            socket.connect();
                            OutputStream out = socket.getOutputStream();
                            BufferedReader reader = new BufferedReader(new InputStreamReader(getResources().openRawResource(R.raw.sample)));
                            out.write(reader.readLine().getBytes());
                            socket.close();
                            handler.sendEmptyMessage(0);
                        }
                        catch (IOException e){}
                    }
                }).start();

            }
        });
    }

}

package com.volansys.touchgestures;

import android.content.Context;
import android.media.AudioManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class AudioManagerExampleActivity extends AppCompatActivity {

    private Button mVibrate,mRing,mSilent,mWhichMode;
    private AudioManager mAudioManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_manager_example);

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mVibrate = findViewById(R.id.button_vibrate);
        mVibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
                Toast.makeText(AudioManagerExampleActivity.this,
                        "Audio Profile:Vibrate",
                        Toast.LENGTH_SHORT).show();
            }
        });

        mRing = findViewById(R.id.button_ring);
        mRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                Toast.makeText(AudioManagerExampleActivity.this,
                        "Audio Profile:Normal",
                        Toast.LENGTH_SHORT).show();
            }
        });

        mSilent = findViewById(R.id.button_silent);
        mSilent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mAudioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                Toast.makeText(AudioManagerExampleActivity.this,
                        "Audio Profile:Silent",
                        Toast.LENGTH_SHORT).show();
            }
        });

        mWhichMode  =findViewById(R.id.button_which_mode);
        mWhichMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (mAudioManager.getRingerMode()){
                    case AudioManager.RINGER_MODE_NORMAL:
                        Toast.makeText(AudioManagerExampleActivity.this,
                                "Audio Profile:Normal",
                                Toast.LENGTH_SHORT).show();
                        break;

                    case AudioManager.RINGER_MODE_SILENT:
                        Toast.makeText(AudioManagerExampleActivity.this,
                                "Audio Profile:Silent",
                                Toast.LENGTH_SHORT).show();
                        break;

                    case AudioManager.RINGER_MODE_VIBRATE:
                        Toast.makeText(AudioManagerExampleActivity.this,
                                "Audio Profile:Vibrate",
                                Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });
    }
}

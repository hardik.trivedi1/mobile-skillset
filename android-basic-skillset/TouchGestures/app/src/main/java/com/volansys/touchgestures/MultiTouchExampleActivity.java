package com.volansys.touchgestures;

import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.Toast;

public class MultiTouchExampleActivity extends AppCompatActivity {

    private static final String DEBUG_TAG = "MultiTouchActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_touch_example);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);
// Get the index of the pointer associated with the action.
        int index = MotionEventCompat.getActionIndex(event);
        int xPos = -1;
        int yPos = -1;

        Log.d(DEBUG_TAG,"The action is " + actionToString(action));

        if (event.getPointerCount() > 1) {
            Log.d(DEBUG_TAG,"Multitouch event");
            // The coordinates of the current screen contact, relative to
            // the responding View or Activity.
            xPos = (int)MotionEventCompat.getX(event, index);
            yPos = (int)MotionEventCompat.getY(event, index);
            Log.d(DEBUG_TAG,"X:"+xPos+"\tY:"+yPos);

        } else {
            // Single touch event
            Log.d(DEBUG_TAG,"Single touch event");
            xPos = (int)MotionEventCompat.getX(event, index);
            yPos = (int) MotionEventCompat.getY(event, index);
            Log.d(DEBUG_TAG,"X:"+xPos+"\tY:"+yPos);
        }

        Toast.makeText(this, "Number of touch " + event.getPointerCount(), Toast.LENGTH_SHORT).show();
        return super.onTouchEvent(event);
    }
    public static String actionToString(int action) {
        switch (action) {

            case MotionEvent.ACTION_DOWN: return "Down";
            case MotionEvent.ACTION_MOVE: return "Move";
            case MotionEvent.ACTION_POINTER_DOWN: return "Pointer Down";
            case MotionEvent.ACTION_UP: return "Up";
            case MotionEvent.ACTION_POINTER_UP: return "Pointer Up";
            case MotionEvent.ACTION_OUTSIDE: return "Outside";
            case MotionEvent.ACTION_CANCEL: return "Cancel";
        }
        return "";
    }
}

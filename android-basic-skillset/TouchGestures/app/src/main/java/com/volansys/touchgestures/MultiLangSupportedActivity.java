package com.volansys.touchgestures;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MultiLangSupportedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_lang_supported);
    }
}

package com.volansys.touchgestures;

import android.animation.ObjectAnimator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ObjectAnimatorExampleActivity extends AppCompatActivity {

    private ImageView imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_animator_example);

        imageView = findViewById(R.id.imageView);
        ObjectAnimator.ofFloat(imageView,"rotationX",0f,90f,180f,270f,360f)
                .setDuration(5000)
                .start();
        ObjectAnimator.ofFloat(imageView,"rotationY",0f,90f,180f,270f,360f)
                .setDuration(5000)
                .start();
        ObjectAnimator.ofFloat(imageView,"scaleX",1f,2f,3f)
                .setDuration(3000)
                .start();
        ObjectAnimator.ofFloat(imageView,"scaleY",1f,2f,3f)
                .setDuration(3000)
                .start();
    }
}

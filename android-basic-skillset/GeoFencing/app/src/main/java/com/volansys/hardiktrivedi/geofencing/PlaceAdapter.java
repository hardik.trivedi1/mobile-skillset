package com.volansys.hardiktrivedi.geofencing;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.volansys.hardiktrivedi.geofencing.data.PlaceContract;

/**
 * Created by hardik on 9/1/18.
 */

public class PlaceAdapter extends CursorAdapter {

    public PlaceAdapter(Context context,Cursor cursor){
        super(context,cursor,0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView placeName = view.findViewById(R.id.textViewPlaceName),
                latitude = view.findViewById(R.id.textViewLatitude),
                longitude = view.findViewById(R.id.textViewLongitude);

        placeName.setText(cursor.getString(cursor.getColumnIndexOrThrow(PlaceContract.COLOUMN_PLACE_NAME)));
        latitude.setText(cursor.getString(cursor.getColumnIndexOrThrow(PlaceContract.COLOUMN_LATITUDE)));
        longitude.setText(cursor.getString(cursor.getColumnIndexOrThrow(PlaceContract.COLOUMN_LONGITUDE)));
    }
}

package com.volansys.hardiktrivedi.geofencing.data;

import android.net.Uri;

public class PlaceContract
{
    public PlaceContract(){}


        public static final String TABLE_NAME="places";

        public static final String _ID= "_id";
        public static final String COLOUMN_PLACE_NAME="geoFenceName";
        public static final String COLOUMN_LATITUDE = "latitude";
        public static final String COLOUMN_LONGITUDE = "longitude";


        public static final Uri BASE_CONTENT_URI = Uri.parse("content://com.volansys.hardiktrivedi.geofencing/places");

}
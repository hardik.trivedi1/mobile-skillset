package com.volansys.hardiktrivedi.geofencing;

import android.Manifest;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.preference.Preference;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.volansys.hardiktrivedi.geofencing.data.PlaceContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;

public class MainActivity extends AppCompatActivity
        implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        OnMapReadyCallback,
        GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener,
        ResultCallback<Status>,
        SampleActivity.IRemoveGeofence{

    private static final String TAG = MainActivity.class.getSimpleName();

    private GoogleMap map;
    private GoogleApiClient googleApiClient;
    private Location lastLocation;


    private SupportMapFragment mapFragment;

    private static final String NOTIFICATION_MSG = "NOTIFICATION MSG";
    // Create a Intent send by the notification
    public static Intent makeNotificationIntent(Context context, String msg) {
        Intent intent = new Intent( context, MainActivity.class );
        intent.putExtra( NOTIFICATION_MSG, msg );
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        geofenceId = getPreferences(Context.MODE_PRIVATE).getInt("uniqueFenceId",0);
        // initialize GoogleMaps
        initGMaps();

        // create GoogleApiClient
        createGoogleApi();
        Log.w(TAG,"from onCreate()");
        SampleActivity.setListener(this);
    }

    // Create GoogleApiClient instance
    private void createGoogleApi() {
        Log.d(TAG, "createGoogleApi()");
        if ( googleApiClient == null ) {
            googleApiClient = new GoogleApiClient.Builder( this )
                    .addConnectionCallbacks( this )
                    .addOnConnectionFailedListener( this )
                    .addApi( LocationServices.API )
                    .build();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Call GoogleApiClient connection when starting the Activity
        googleApiClient.disconnect();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // Disconnect GoogleApiClient when stopping Activity
        googleApiClient.disconnect();
        googleApiClient.connect();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        getPreferences(Context.MODE_PRIVATE).edit()
                .putInt("uniqueFenceId",geofenceId)
                .apply();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate( R.menu.main_menu, menu );
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch ( item.getItemId() ) {
            case R.id.geofence: {
                startGeofence();
                return true;
            }
            case R.id.clear: {
                clearGeofence();
                return true;
            }
            case R.id.next:{
                startActivity(new Intent(this,SampleActivity.class));
            }
            case R.id.other:{
                startActivity(new Intent(this,OtherMapExerciseActivity.class));
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private final int REQ_PERMISSION = 999;

    // Check for permission to access Location
    private boolean checkPermission() {
        Log.d(TAG, "checkPermission()");
        // Ask for permission if it wasn't granted yet
        return (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED );
    }

    // Asks for permission
    private void askPermission() {
        Log.d(TAG, "askPermission()");
        ActivityCompat.requestPermissions(
                this,
                new String[] { Manifest.permission.ACCESS_FINE_LOCATION },
                REQ_PERMISSION
        );
    }

    // Verify user's response of the permission requested
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult()");
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch ( requestCode ) {
            case REQ_PERMISSION: {
                if ( grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED ){
                    // Permission granted
                    getLastKnownLocation();

                } else {
                    // Permission denied
                    permissionsDenied();
                }
                break;
            }
        }
    }

    // App cannot work without the permissions
    private void permissionsDenied() {
        Log.w(TAG, "permissionsDenied()");
        // TODO close app and warn user
    }

    // Initialize GoogleMaps
    private void initGMaps(){
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    // Callback called when Map is ready
    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady()");
        map = googleMap;
        map.setOnMapClickListener(this);
        map.setOnMarkerClickListener(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.d(TAG, "onMapClick("+latLng +")");
        markerForGeofence(latLng,GEOFENCE_REQ_ID+geofenceId);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        Log.d(TAG, "onMarkerClickListener: " + marker.getPosition() );
        return false;
    }

    private LocationRequest locationRequest;
    // Defined in mili seconds.
    // This number in extremely low, and should be used only for debug
    private final int UPDATE_INTERVAL =  1000;
    private final int FASTEST_INTERVAL = 900;

    // Start location Updates
    private void startLocationUpdates(){
        Log.i(TAG, "startLocationUpdates()");
        locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);

        if ( checkPermission() )
            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged ["+location+"]");
        lastLocation = location;
        writeActualLocation(location);
    }

    // GoogleApiClient.ConnectionCallbacks connected
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Log.i(TAG, "onConnected()");
        getLastKnownLocation();
        recoverGeofenceMarker();
    }

    // GoogleApiClient.ConnectionCallbacks suspended
    @Override
    public void onConnectionSuspended(int i) {
        Log.w(TAG, "onConnectionSuspended()");
    }

    // GoogleApiClient.OnConnectionFailedListener fail
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.w(TAG, "onConnectionFailed()");
    }

    // Get last known location
    private void getLastKnownLocation() {
        Log.d(TAG, "getLastKnownLocation()");
        if ( checkPermission() ) {
            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            if ( lastLocation != null ) {
                Log.i(TAG, "LasKnown location. " +
                        "Long: " + lastLocation.getLongitude() +
                        " | Lat: " + lastLocation.getLatitude());
                writeLastLocation();
                startLocationUpdates();
            } else {
                Log.w(TAG, "No location retrieved yet");
                startLocationUpdates();
            }
        }
        else askPermission();
    }

    private void writeActualLocation(Location location) {

        markerLocation(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    private void writeLastLocation() {
        writeActualLocation(lastLocation);
    }

    private Marker locationMarker;
    private void markerLocation(LatLng latLng) {
        Log.i(TAG, "markerLocation("+latLng+")");
        String title = latLng.latitude + ", " + latLng.longitude;
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .title(title);
        if ( map!=null ) {
            if ( locationMarker != null )
                locationMarker.remove();
            locationMarker = map.addMarker(markerOptions);
            float zoom = 14f;
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
            map.animateCamera(cameraUpdate);
        }
    }


    private Marker geoFenceMarker;
    private HashMap<String,Marker> mGeofenceMarkers=new HashMap<>();//private ArrayList<Marker> markers=new ArrayList<>();
    private void markerForGeofence(LatLng latLng,String fenceId) {
        Log.i(TAG, "markerForGeofence("+latLng+")");
        String title = latLng.latitude + ", " + latLng.longitude;
        // Define marker options
        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE))
                .title(title);
        if ( map!=null ) {
            // Remove last geoFenceMarker
            //**if (geoFenceMarker != null)
            //**geoFenceMarker.remove();

            ////////////////////////////////////////////////////////////////geoFenceMarker = map.addMarker(markerOptions);
            if(newlyCreated)
                mGeofenceMarkers.put(fenceId,map.addMarker(markerOptions));
            else if(mGeofenceCircles.get(fenceId)==null)
                mGeofenceMarkers.put(fenceId,map.addMarker(markerOptions));

            // markers.add(geofenceId,map.addMarker(markerOptions));
        }
    }

    // Start Geofence creation process
    private void startGeofence() {
        Log.i(TAG, "startGeofence()");
        if( mGeofenceMarkers.get(GEOFENCE_REQ_ID+geofenceId)!=null){//markers.get(geofenceId) != null ) {
            newlyCreated = true;
            Geofence geofence = createGeofence(mGeofenceMarkers.get(GEOFENCE_REQ_ID+geofenceId).getPosition() /*geoFenceMarker.getPosition()*//*markers.get(geofenceId).getPosition()*/, GEOFENCE_RADIUS ,null);
            GeofencingRequest geofenceRequest = createGeofenceRequest( geofence );
            addGeofence( geofenceRequest );

        } else {
            Log.e(TAG, "Geofence marker is null");
        }
    }

    private void startGeofence(LatLng latLng,String fenceId){

        Geofence geofence = createGeofence(latLng,GEOFENCE_RADIUS,fenceId);
        GeofencingRequest geofenceRequest = createGeofenceRequest( geofence );
        addGeofence( geofenceRequest );

    }
    private static final long GEO_DURATION = 60 * 60 * 1000;
    private static final String GEOFENCE_REQ_ID = "My Geofence"; // private ArrayList<String> geofenceReqIds=new ArrayList<>();
    private static final float GEOFENCE_RADIUS = 500.0f; // in meters
    private int geofenceId;

    // Create a Geofence
    private Geofence createGeofence( LatLng latLng, float radius ,String fenceId) {
        Log.d(TAG, "createGeofence");
        //geofenceReqIds.add(geofenceId,GEOFENCE_REQ_ID+geofenceId);
        String tempFenceId ;
        if(fenceId==null)
            tempFenceId = GEOFENCE_REQ_ID+geofenceId;
        else
            tempFenceId = fenceId;

        return new Geofence.Builder()
                .setRequestId(tempFenceId/*geofenceReqIds.get(geofenceId)*/)
                .setCircularRegion( latLng.latitude, latLng.longitude, radius)
                .setExpirationDuration( GEO_DURATION )
                .setTransitionTypes( Geofence.GEOFENCE_TRANSITION_ENTER
                        | Geofence.GEOFENCE_TRANSITION_EXIT
                        | Geofence.GEOFENCE_TRANSITION_DWELL)
                .setLoiteringDelay(1000)
                .build();
    }

    // Create a Geofence Request
    private GeofencingRequest createGeofenceRequest( Geofence geofence ) {
        Log.d(TAG, "createGeofenceRequest");
        return new GeofencingRequest.Builder()
                .setInitialTrigger( GeofencingRequest.INITIAL_TRIGGER_ENTER )
                .addGeofence( geofence )
                .build();
    }

    private PendingIntent geoFencePendingIntent;
    private final int GEOFENCE_REQ_CODE = 0;
    private PendingIntent createGeofencePendingIntent() {
        Log.d(TAG, "createGeofencePendingIntent");
        if ( geoFencePendingIntent != null )
            return geoFencePendingIntent;

        Intent intent = new Intent( this, GeofenceTrasitionService.class);
        return PendingIntent.getService(
                this, GEOFENCE_REQ_CODE, intent, PendingIntent.FLAG_UPDATE_CURRENT );
    }

    // Add the created GeofenceRequest to the device's monitoring list
    private void addGeofence(GeofencingRequest request) {
        Log.d(TAG, "addGeofence");
        if (checkPermission())
            LocationServices.GeofencingApi.addGeofences(
                    googleApiClient,
                    request,
                    createGeofencePendingIntent()
            ).setResultCallback(this);
    }

    boolean newlyCreated;

    @Override
    public void onResult(@NonNull Status status) {
        Log.i(TAG, "onResult: " + status);
        if ( status.isSuccess() ) {
            if(newlyCreated){
                Log.w(TAG,"from onResult() where geofenceId is "+geofenceId);

                saveGeofence();geofenceId++;}
                int temp = geofenceId;
            drawGeofence(GEOFENCE_REQ_ID+(temp-1));
            Log.w(TAG,"from onResult() where geofenceId is "+geofenceId);
            newlyCreated=false;
            //geofenceId++;

        } else {
            Log.d(TAG,"onResult() is empty");
        }
    }

    // Draw Geofence circle on GoogleMap
    private Circle geoFenceLimits;
    private HashMap<String,Circle> mGeofenceCircles=new HashMap<>();//private ArrayList<Circle> limits=new ArrayList<>();
    private void drawGeofence(String fenceId) {
        Log.d(TAG, "drawGeofence()");

        //**if ( geoFenceLimits != null )
        //**geoFenceLimits.remove();
        if(mGeofenceMarkers.get(fenceId)!=null)
        {
            CircleOptions circleOptions;
            if(newlyCreated) {
                circleOptions = new CircleOptions()
                        .center(mGeofenceMarkers.get(fenceId).getPosition()/*geoFenceMarker.getPosition()*/ /*markers.get(geofenceId).getPosition()*/)
                        .strokeColor(Color.argb(50, 70, 70, 70))
                        .fillColor(Color.argb(100, 150, 150, 150))
                        .radius(GEOFENCE_RADIUS);

                /////////////////////////////////////////////////////////////////geoFenceLimits = map.addCircle( circleOptions );
                mGeofenceCircles.put(fenceId, map.addCircle(circleOptions));
                //limits.add(geofenceId,map.addCircle(circleOptions));
            }
            else if( mGeofenceCircles.get(fenceId)==null){
                circleOptions = new CircleOptions()
                        .center(mGeofenceMarkers.get(fenceId).getPosition()/*geoFenceMarker.getPosition()*/ /*markers.get(geofenceId).getPosition()*/)
                        .strokeColor(Color.argb(50, 70, 70, 70))
                        .fillColor(Color.argb(100, 150, 150, 150))
                        .radius(GEOFENCE_RADIUS);

                /////////////////////////////////////////////////////////////////geoFenceLimits = map.addCircle( circleOptions );
                mGeofenceCircles.put(fenceId, map.addCircle(circleOptions));
                //limits.add(geofenceId,map.addCircle(circleOptions));
            }
        }
    }

    private final String KEY_GEOFENCE_LAT = "GEOFENCE LATITUDE";
    private final String KEY_GEOFENCE_LON = "GEOFENCE LONGITUDE";

    // Saving GeoFence marker with prefs mng
    private void saveGeofence() {
        Log.d(TAG, "saveGeofence()");
        ContentValues data = new ContentValues();
        data.put(PlaceContract.COLOUMN_PLACE_NAME,GEOFENCE_REQ_ID+geofenceId);
        data.put(PlaceContract.COLOUMN_LATITUDE,mGeofenceMarkers.get(GEOFENCE_REQ_ID+geofenceId).getPosition().latitude/*markers.get(geofenceId).getPosition().latitude*//*geoFenceMarker.getPosition().latitude*/);
        data.put(PlaceContract.COLOUMN_LONGITUDE,mGeofenceMarkers.get(GEOFENCE_REQ_ID+geofenceId).getPosition().longitude/*markers.get(geofenceId).getPosition().longitude*//*geoFenceMarker.getPosition().longitude*/);
        getContentResolver().insert(PlaceContract.BASE_CONTENT_URI,data);

        SharedPreferences sharedPref = getPreferences( Context.MODE_PRIVATE );
        SharedPreferences.Editor editor = sharedPref.edit();

        editor.putInt("uniqueFenceId",geofenceId);
        editor.apply();

    }

    // Recovering last Geofence marker
    private void recoverGeofenceMarker() {
        Log.d(TAG, "recoverGeofenceMarker");

        String coloumns[]={PlaceContract._ID,PlaceContract.COLOUMN_PLACE_NAME,
                PlaceContract.COLOUMN_LATITUDE,PlaceContract.COLOUMN_LONGITUDE};
        Cursor cursor = getContentResolver().query(
                PlaceContract.BASE_CONTENT_URI,coloumns,null,null,null);
        if(cursor.getCount()!=0) {
            cursor.moveToFirst();
            do {
                double lang = cursor.getDouble(cursor.getColumnIndexOrThrow(PlaceContract.COLOUMN_LONGITUDE)),
                        lat = cursor.getDouble(cursor.getColumnIndexOrThrow(PlaceContract.COLOUMN_LATITUDE));
                String fenceId = cursor.getString(cursor.getColumnIndexOrThrow(PlaceContract.COLOUMN_PLACE_NAME));
                LatLng mark = new LatLng(lat, lang);
                markerForGeofence(mark,fenceId);
                drawGeofence(fenceId);
                startGeofence(new LatLng(lat,lang),fenceId);
            } while (cursor.moveToNext());
            cursor.close();
        }
    }

    // Clear Geofence
    private void clearGeofence() {
        Log.d(TAG, "clearGeofence()");
        /******  LocationServices.GeofencingApi.removeGeofences(
         googleApiClient,
         createGeofencePendingIntent()
         ).setResultCallback(new ResultCallback<Status>() {
        @Override
        public void onResult(@NonNull Status status) {
        if ( status.isSuccess() ) {
        // remove drawing
        removeGeofenceDraw();
        }
        }
        });*****/
        geofenceId--;
        ArrayList<String> temp=new ArrayList<>();
        //temp.add(geofenceReqIds.get(geofenceId));
        LocationServices.GeofencingApi.removeGeofences(googleApiClient,temp)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        //removeGeofenceDraw();
                        //geofenceId--;

                    }
                });
    }


    private void removeGeofenceDraw(String fenceId) {
       /******************** if ( geoFenceMarker != null)
         geoFenceMarker.remove();
         if ( geoFenceLimits != null )
         geoFenceLimits.remove();****************************/
        //limits.get(geofenceId).remove();
       // markers.get(geofenceId).remove();
      //  geofenceReqIds.remove(geofenceId);
        mGeofenceMarkers.get(fenceId).remove();
        mGeofenceCircles.get(fenceId).remove();
        mGeofenceMarkers.remove(fenceId);
        mGeofenceCircles.remove(fenceId);
        Log.w(TAG,"from removeGeofenceDraw() with fenceId is "+fenceId);
    }


    @Override
    public void removeSelectedFence(View listItemView) {
        final ArrayList<String> temp=new ArrayList<>();
        //temp.add(geofenceReqIds.get(geofenceId));
        TextView tempTextView=listItemView.findViewById(R.id.textViewPlaceName);
        temp.add(tempTextView.getText().toString());
        LocationServices.GeofencingApi.removeGeofences(googleApiClient,temp)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        removeGeofenceDraw(temp.get(0));
                        Log.w(TAG,"from removeselectedFence() where geoFenceId is "+temp.get(0));
                    }
                });
    }
}


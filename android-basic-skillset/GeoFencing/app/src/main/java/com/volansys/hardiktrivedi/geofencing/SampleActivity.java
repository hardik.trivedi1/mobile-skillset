package com.volansys.hardiktrivedi.geofencing;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.volansys.hardiktrivedi.geofencing.data.PlaceContract;
import com.volansys.hardiktrivedi.geofencing.data.PlaceDbHelper;

import java.io.IOException;

public class SampleActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    private ListView mPlacesListView;
    private PlaceAdapter mPlaceAdapter;
    private PlaceDbHelper mPlaceDbHelper;
    private Geocoder mGeoCoder;
    private static SQLiteDatabase mDatabase;
    private static IRemoveGeofence iRemoveGeofence;
    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        mPlaceDbHelper = new PlaceDbHelper(this);
        mDatabase = mPlaceDbHelper.getWritableDatabase();

        mPlacesListView = findViewById(R.id.listView);

        View emptyViewForListView = findViewById(R.id.empty_view);
        mPlacesListView.setEmptyView(emptyViewForListView);

        mPlaceAdapter = new PlaceAdapter(this,null);
        mPlacesListView.setAdapter(mPlaceAdapter);

        getSupportLoaderManager().initLoader(0,null,this);

        mPlacesListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
                MyDialogFragment.setListViewItem(view);
                new MyDialogFragment().show(getSupportFragmentManager(),"Delete");

                return false;
            }
        });

        mGeoCoder = new Geocoder(this);

        mPlacesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                TextView tvLat = (TextView) view.findViewById(R.id.textViewLatitude);
                TextView tvLong = (TextView) view.findViewById(R.id.textViewLongitude);
                try {
                    Address currentAddress = mGeoCoder.getFromLocation(Double.parseDouble(tvLat.getText().toString()),
                            Double.parseDouble(tvLong.getText().toString()), 3).get(0);
                    Toast.makeText(SampleActivity.this,
                            currentAddress.getAddressLine(0), Toast.LENGTH_SHORT).show();
                }
                catch (IOException e){}
            }
        });
    }

    private static void deleteAndRemoveFence(View view)
    {
        TextView temp=view.findViewById(R.id.textViewPlaceName);
        iRemoveGeofence.removeSelectedFence(view);
        mDatabase.delete(PlaceContract.TABLE_NAME,
                PlaceContract.COLOUMN_PLACE_NAME+"=\""+temp.getText()+"\"",null);
    }

    public static class MyDialogFragment extends DialogFragment {
        static View tempListViewItem;
        public static void setListViewItem(View v)
        {
            tempListViewItem = v;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the Builder class for convenient dialog construction
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            return builder
                    .setTitle("Do you want to remove Geo fence from this place?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    deleteAndRemoveFence(tempListViewItem);
                                    }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    }
                    })
                    .create();
            }
        }



    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String coloumns[]={PlaceContract._ID,PlaceContract.COLOUMN_PLACE_NAME,
                            PlaceContract.COLOUMN_LATITUDE,PlaceContract.COLOUMN_LONGITUDE};
        return new CursorLoader(this,
                PlaceContract.BASE_CONTENT_URI,coloumns,null,null,null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mPlaceAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mPlaceAdapter.swapCursor(null);
    }



    public static void setListener(Context context)
    {
        try{
            iRemoveGeofence = (IRemoveGeofence)context;
        }catch (Exception e){}
    }
    public interface IRemoveGeofence
    {
        void removeSelectedFence(View listItemView);
    }
}
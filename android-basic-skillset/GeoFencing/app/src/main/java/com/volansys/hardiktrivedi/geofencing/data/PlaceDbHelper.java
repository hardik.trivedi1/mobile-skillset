package com.volansys.hardiktrivedi.geofencing.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PlaceDbHelper extends SQLiteOpenHelper
{
    public static final String DATABASE_NAME="places.db";

    String CREATE_TABLE_COMMAND="CREATE TABLE places (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
            "geoFenceName TEXT NOT NULL, latitude DOUBLE NOT NULL, longitude DOUBLE NOT NULL);";


    public static final int DATABASE_VERSION=1;

    public PlaceDbHelper(Context context)
    {
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_COMMAND);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}

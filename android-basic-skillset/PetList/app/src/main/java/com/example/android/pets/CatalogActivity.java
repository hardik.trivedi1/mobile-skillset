/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.android.pets;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.pets.data.PetDbHelper;
import com.example.android.pets.data.PetContract;

/**
 * Displays list of pets that were entered and stored in the app.
 */
public class CatalogActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>{

    PetDbHelper petDbHelper;
    SQLiteDatabase db;
    ListView petList;
    PetCursorAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        // Setup FAB to open EditorActivity
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CatalogActivity.this, EditorActivity.class);
                startActivity(intent);
            }
        });

        petDbHelper=new PetDbHelper(this);
        db=petDbHelper.getWritableDatabase();
        petList=findViewById(R.id.listView);

        View emptyView = findViewById(R.id.empty_view);
        petList.setEmptyView(emptyView);


        adapter=new PetCursorAdapter(this,null);
        petList.setAdapter(adapter);

        getSupportLoaderManager().initLoader(PET_LOADER,null,this);

        petList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent=new Intent(CatalogActivity.this,EditorActivity.class);
                //Uri uri= ContentUris.withAppendedId(PetContract.BASE_CONTENT_URI,id);
                intent.setData(Uri.parse("content://com.example.android.pets/pets/"+id));

                startActivity(intent);
            }
        });

        //showData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu options from the res/menu/menu_catalog.xml file.
        // This adds menu items to the app bar.
        getMenuInflater().inflate(R.menu.menu_catalog, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // User clicked on a menu option in the app bar overflow menu
        switch (item.getItemId()) {
            // Respond to a click on the "Insert dummy data" menu option
            case R.id.action_insert_dummy_data:
                // Do nothing for now
                insertPet();
                showData();
                return true;
            // Respond to a click on the "Delete all entries" menu option
            case R.id.action_delete_all_entries:
                // Do nothing for now
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void showData() {
        String coloumns[]={PetContract._ID,PetContract.COLOUMN_PET_NAME,PetContract.COLOUMN_BREED,PetContract.COLOUMN_GENDER,PetContract.COLOUMN_WEIGHT};
        //Cursor c=db.query(PetContract.TABLE_NAME,coloumns,null,null,null,null,null);
        Cursor c=getContentResolver().query(PetContract.BASE_CONTENT_URI,coloumns,null,null,null,null);
        //Toast.makeText(CatalogActivity.this, "Total rows : "+c.getCount(), Toast.LENGTH_SHORT).show();

    }

    private void insertPet() {
        ContentValues data=new ContentValues();
        data.put(PetContract.COLOUMN_PET_NAME,"Jay");
        data.put(PetContract.COLOUMN_BREED,"Husky");
        data.put(PetContract.COLOUMN_GENDER,1);
        data.put(PetContract.COLOUMN_WEIGHT,85);

        //long rowid=db.insert(PetContract.TABLE_NAME,null,data);
        Uri rowid=getContentResolver().insert(PetContract.BASE_CONTENT_URI,data);
        Log.v("Dummy insertion","rowId:"+rowid);
    }

    private final int PET_LOADER = 0;

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String coloumns[]={PetContract._ID,PetContract.COLOUMN_PET_NAME,PetContract.COLOUMN_BREED,PetContract.COLOUMN_GENDER,PetContract.COLOUMN_WEIGHT};

        return new CursorLoader(this,PetContract.BASE_CONTENT_URI,coloumns,null,null,null);

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }
}

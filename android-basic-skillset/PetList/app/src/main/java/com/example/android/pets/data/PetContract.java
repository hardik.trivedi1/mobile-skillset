package com.example.android.pets.data;

import android.net.Uri;
import android.provider.BaseColumns;

public class PetContract
{
    public PetContract(){}


        public static final String TABLE_NAME="pets";

        public static final String _ID=BaseColumns._ID;
        public static final String COLOUMN_PET_NAME="name";
        public static final String COLOUMN_BREED="breed";
        public static final String COLOUMN_GENDER="gender";
        public static final String COLOUMN_WEIGHT="weight";

        public static final int GENDER_UNKNOWN=0,GENDER_MALE=1,GENDER_FEMALE=2;

        public static final Uri BASE_CONTENT_URI = Uri.parse("content://com.example.android.pets/pets");

}
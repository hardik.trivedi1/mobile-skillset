package com.volansys.hardiktrivedi.udacityproject2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class QuizActivity extends AppCompatActivity {


    RadioGroup que1,que2,que3,que4,que5;
    RadioButton answerRadio1,answerRadio2,answerRadio3,answerRadio5;
    CheckBox answer4Checkbox1,answer4CheckBox2,answer4Checkbox3,answer4Checkbox4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        que1 = (RadioGroup)findViewById(R.id.que1Options);
        que2 = (RadioGroup)findViewById(R.id.que2Options);
        que3 = (RadioGroup)findViewById(R.id.que3Options);
        que4 = (RadioGroup)findViewById(R.id.que4Options);
        que5 = (RadioGroup)findViewById(R.id.que5Options);

        answer4Checkbox1 = (CheckBox)findViewById(R.id.radioButton13);
        answer4CheckBox2 = (CheckBox)findViewById(R.id.radioButton14);
        answer4Checkbox3 = (CheckBox)findViewById(R.id.radioButton15);
        answer4Checkbox4 = (CheckBox)findViewById(R.id.radioButton16);


    }

    public void checkAnswers(View v)
    {
        try {
            answerRadio1 = (RadioButton) findViewById(que1.getCheckedRadioButtonId());
            answerRadio2 = (RadioButton) findViewById(que2.getCheckedRadioButtonId());
            answerRadio3 = (RadioButton) findViewById(que3.getCheckedRadioButtonId());
            answerRadio5 = (RadioButton) findViewById(que5.getCheckedRadioButtonId());


            String answer1 = answerRadio1.getText().toString(),
                    answer2 = answerRadio2.getText().toString(),
                    answer3 = answerRadio3.getText().toString(),
                    answer5 = answerRadio5.getText().toString();

            String answer4 = "";
            if (answer4Checkbox1.isChecked())
                answer4 += answer4Checkbox1.getText().toString();
            if (answer4CheckBox2.isChecked())
                answer4 += answer4CheckBox2.getText().toString();
            if (answer4Checkbox3.isChecked())
                answer4 += answer4Checkbox3.getText().toString();
            if (answer4Checkbox4.isChecked())
                answer4 += answer4Checkbox4.getText().toString();

            int rightAnswersCount = 0;

            if (answer1.equals("336 BCE"))
                rightAnswersCount++;
            else {
                RadioButton rightAnswer=(RadioButton)que1.findViewById(R.id.radioButton2);
                answerRadio1.setTextColor(Color.RED);
                rightAnswer.setTextColor(Color.GREEN);
            }
            if (answer2.equals("Rigveda"))
                rightAnswersCount++;
            else {
                RadioButton rightAnswer=(RadioButton)que2.findViewById(R.id.radioButton7);
                answerRadio2.setTextColor(Color.RED);
                rightAnswer.setTextColor(Color.GREEN);
            }
            if (answer3.equals("1774 AD"))
                rightAnswersCount++;
            else {
                RadioButton rightAnswer=(RadioButton)que3.findViewById(R.id.radioButton9);
                answerRadio3.setTextColor(Color.RED);
                rightAnswer.setTextColor(Color.GREEN);
            }
            if (answer4.trim().equals("ChandraguptaAshoka"))
                rightAnswersCount++;
            else {
                answer4CheckBox2.setTextColor(Color.GREEN); answer4Checkbox3.setTextColor(Color.GREEN);
                answer4Checkbox1.setTextColor(Color.RED);   answer4Checkbox4.setTextColor(Color.RED);
            }
            if (answer5.equals("King George VI"))
                rightAnswersCount++;
            else {
                RadioButton rightAnswer=(RadioButton)que5.findViewById(R.id.radioButton20);
                answerRadio5.setTextColor(Color.RED);
                rightAnswer.setTextColor(Color.GREEN);
            }


            Toast.makeText(QuizActivity.this, getIntent().getStringExtra("name") + ", you have " + rightAnswersCount + " answers right.", Toast.LENGTH_LONG).show();
        }catch (Exception e){
            Toast.makeText(QuizActivity.this, "Some questions are remained to answer.", Toast.LENGTH_SHORT).show();
        }

    }

}

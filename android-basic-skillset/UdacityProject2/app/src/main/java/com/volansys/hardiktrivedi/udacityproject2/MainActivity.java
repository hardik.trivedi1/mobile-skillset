package com.volansys.hardiktrivedi.udacityproject2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.DragEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText editTextName = (EditText)findViewById(R.id.editText);
        Button startQuiz = (Button)findViewById(R.id.button);
        startQuiz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent openQuiz = new Intent(getBaseContext(),QuizActivity.class);
                openQuiz.putExtra("name",editTextName.getText().toString());
                startActivity(openQuiz);
            }
        });

    }
}

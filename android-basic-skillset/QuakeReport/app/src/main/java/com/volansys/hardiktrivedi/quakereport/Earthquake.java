package com.volansys.hardiktrivedi.quakereport;

public class Earthquake
{
    private String place;
    private long time;
    private double magnitude;
    public double longitude,latitude;
    public String url;
    Earthquake(double magnitude,String place,long time)
    {
        this.magnitude=magnitude;
        this.place=place;
        this.time=time;
    }
    public double getMagnitude(){return magnitude;}
    public String getPlace(){return place;}
    public long getTime(){return time;}

    public void setLongitudeAndLatitude(double longitude,double latitude)
    {
        this.longitude=longitude;
        this.latitude=latitude;
    }


    //http://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2016-01-01&endtime=2016-01-31&minmag=6&limit=10

    /*long timeInMilliseconds = 1454124312220L;
    Date dateObject = new Date(timeInMilliseconds);

     SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM DD, yyyy");
 String dateToDisplay = dateFormatter.format(dateObject);
    */
}
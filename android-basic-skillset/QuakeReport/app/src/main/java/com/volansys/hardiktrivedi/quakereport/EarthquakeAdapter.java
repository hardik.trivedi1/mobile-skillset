package com.volansys.hardiktrivedi.quakereport;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class EarthquakeAdapter extends ArrayAdapter<Earthquake>
{
    public EarthquakeAdapter(Activity context, ArrayList<Earthquake> data)
    {
        super(context,R.layout.list_item,data);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        View listItemView = convertView;

        if(listItemView == null)
        {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }
        Earthquake data = getItem(position);
        TextView magnitude = listItemView.findViewById(R.id.textView);
        magnitude.setText(""+Math.floor(data.getMagnitude()));
        GradientDrawable magnitudeCircle = (GradientDrawable) magnitude.getBackground();
        int magnitudeColor = getMagnitudeColor(data.getMagnitude());
        magnitudeCircle.setColor(magnitudeColor);

        TextView place = listItemView.findViewById(R.id.textView2);
        place.setText(data.getPlace().toString());

        TextView time = listItemView.findViewById(R.id.textView3);
        long timeInMilliseconds = data.getTime();
        Date dateObject = new Date(timeInMilliseconds);
        SimpleDateFormat dateFormatter = new SimpleDateFormat("MMM DD, yyyy\nHH:MM");
        String dateToDisplay = dateFormatter.format(dateObject);
        time.setText(dateToDisplay);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Whoa!!!!!", Toast.LENGTH_SHORT).show();
            }
        });
        TextView latlong = listItemView.findViewById(R.id.textView4);
        latlong.setVisibility(View.INVISIBLE);
        latlong.setText(String.valueOf(data.latitude+","+data.longitude));

        listItemView.setTag(data.url);

        return listItemView;
    }

    public int getMagnitudeColor(double mag)
    {
        int color=0;
        if (mag<1&&mag>=0)
            color=R.color.magnitude1;
        else if(mag<2&&mag>=1)
            color=R.color.magnitude2;
        else if(mag<3&&mag>=2)
            color=R.color.magnitude3;
        else if(mag<4&&mag>=3)
            color=R.color.magnitude4;
        else if(mag<5&&mag>=4)
            color=R.color.magnitude5;
        else if(mag<6&&mag>=5)
            color=R.color.magnitude6;
        else if(mag<7&&mag>=6)
            color=R.color.magnitude7 ;
        else if(mag<8&&mag>=7)
            color=R.color.magnitude8;
        else if(mag<9&&mag>=8)
            color= R.color.magnitude9;
        else if(mag<10&&mag>=9)
            color= R.color.magnitude10plus;

        return ContextCompat.getColor(getContext(), color);
    }
}
package com.volansys.hardiktrivedi.quakereport;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<ArrayList<Earthquake>>{

    private static String USGS_REQUEST_URL =
            "https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&orderby=time&limit=100&starttime=2017-12-01";

    ListView lv;
    TextView initmsg;
    EarthquakeAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        String minMagnitude = sharedPrefs.getString(
                getString(R.string.settings_min_magnitude_key),
                getString(R.string.settings_min_magnitude_default));
        USGS_REQUEST_URL+="&minmag="+minMagnitude;

        mAdapter = new EarthquakeAdapter(this,new ArrayList<Earthquake>());
        lv=findViewById(R.id.listView);
        initmsg = findViewById(R.id.initialMsg);
        lv.setEmptyView(initmsg);

       // setEarthquakesDataFromInternet();


        getSupportLoaderManager().initLoader(0,null, (LoaderManager.LoaderCallbacks<ArrayList<Earthquake>>)this);

        Log.v("Quake","From onCreate()");

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                tempView=view;
                new chooseWhereToGo().show(getSupportFragmentManager(),"Dialog");
            }
        });


    }

    static View tempView=null;
    public static class chooseWhereToGo extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final String[] items = {"See location on Map","Visit page for more info"};

            AlertDialog.Builder builder =
                    new AlertDialog.Builder(getActivity());

            builder.setTitle("Choose any one")
                    .setItems(items, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int item) {
                            if(item==0)
                            {
                                    Intent showQuakeLocation = new Intent(getContext(),MapsActivity.class);
                                    TextView latlong = tempView.findViewById(R.id.textView4);
                                    String temp=latlong.getText().toString();  Log.w("LatLong",temp);
                                    String[] sCoOrdinates = temp.split(",");
                                    double latitude = Double.parseDouble(sCoOrdinates[0]),logitude = Double.parseDouble(sCoOrdinates[1]);
                                    showQuakeLocation.putExtra("lat",latitude);
                                    showQuakeLocation.putExtra("long",logitude);
                                    startActivity(showQuakeLocation);
                            }
                            else {
                                    Intent showQuakeInfo = new Intent(getContext(),WebViewer.class);
                                    showQuakeInfo.putExtra("url",tempView.getTag().toString());
                                    startActivity(showQuakeInfo);
                            }
                        }
                    });

            return builder.create();
        }
    }
    @Override
    public Loader<ArrayList<Earthquake>> onCreateLoader(int i, Bundle bundle) {
        // Create a new loader for the given URL
        return new EarthquakeLoader(this);
    }


    @Override
    public void onLoadFinished(Loader<ArrayList<Earthquake>> loader, ArrayList<Earthquake> earthquakes) {
        mAdapter.clear();

        View loadingIndicator = findViewById(R.id.loading_indicator);
        loadingIndicator.setVisibility(View.GONE);

        if(!isNetworkAvailable())
            initmsg.setText("No Internet Connection.");
        if (earthquakes != null && !earthquakes.isEmpty()) {
            mAdapter.addAll(earthquakes);
        }
        lv.setAdapter(mAdapter);

        Log.v("Quake","From onLoadFinished()");
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Earthquake>> loader) {
        mAdapter.clear();
        Log.v("Quake","From onLoaderReset()");
    }


    public static class EarthquakeLoader extends AsyncTaskLoader<ArrayList<Earthquake>>
    {
        EarthquakeLoader(Context context)
        {
            super(context);
        }


        @Override
        protected void onStartLoading() {
            forceLoad();
        }

        @Override
        public ArrayList<Earthquake> loadInBackground() {
            String jsonResponse = "";

            try{
                URL url=new URL(USGS_REQUEST_URL);


                HttpURLConnection urlConnection = null;
                InputStream inputStream = null;

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setReadTimeout(10000);
                urlConnection.setConnectTimeout(15000);
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();
                if (urlConnection.getResponseCode() == 200) {
                    inputStream = urlConnection.getInputStream();
                    jsonResponse = readFromStream(inputStream);
                } else {
                    Log.e("Error", "Error response code: " + urlConnection.getResponseCode());
                }
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (inputStream != null) {
                    inputStream.close();}
                Log.v("Quake","From loadInBackground()");
            }catch(Exception e){}
            return extractFeatureFromJson(jsonResponse);
        }
        @Override
        public void deliverResult(ArrayList<Earthquake> data) {
            super.deliverResult(data);
        }
    }


    private void setEarthquakesDataFromInternet()
    {
        class DownloadData extends AsyncTask<String,Void,ArrayList<Earthquake>>
        {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(ArrayList<Earthquake> earthquakes) {
                super.onPostExecute(earthquakes);
                ListView earthquakeListView = (ListView) findViewById(R.id.listView);

                EarthquakeAdapter adapter = new EarthquakeAdapter(MainActivity.this,earthquakes);
                earthquakeListView.setAdapter(adapter);
            }

            @Override
            protected ArrayList<Earthquake> doInBackground(String... strings) {
                String jsonResponse = "";

                try{
                        URL url=new URL(USGS_REQUEST_URL);


                        HttpURLConnection urlConnection = null;
                        InputStream inputStream = null;

                            urlConnection = (HttpURLConnection) url.openConnection();
                            urlConnection.setReadTimeout(10000);
                            urlConnection.setConnectTimeout(15000);
                            urlConnection.setRequestMethod("GET");
                            urlConnection.connect();
                            if (urlConnection.getResponseCode() == 200) {
                                inputStream = urlConnection.getInputStream();
                                jsonResponse = readFromStream(inputStream);
                            } else {
                                Log.e("Error", "Error response code: " + urlConnection.getResponseCode());
                            }
                            if (urlConnection != null) {
                                urlConnection.disconnect();
                            }
                            if (inputStream != null) {
                                inputStream.close();}
                    }catch(Exception e){}
                    return extractFeatureFromJson(jsonResponse);

            }
        }
        new DownloadData().execute();


    }

    private static String readFromStream(InputStream inputStream) throws IOException {
        StringBuilder output = new StringBuilder();
        if (inputStream != null) {
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while (line != null) {
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }


    private static ArrayList<Earthquake> extractFeatureFromJson(String earthquakeJSON) {
        if (TextUtils.isEmpty(earthquakeJSON)) {
            return null;
        }

        // Create an empty ArrayList that we can start adding earthquakes to
        ArrayList<Earthquake> earthquakes = new ArrayList<>();

        // Try to parse the JSON response string. If there's a problem with the way the JSON
        // is formatted, a JSONException exception object will be thrown.
        // Catch the exception so the app doesn't crash, and print the error message to the logs.
        try {

            // Create a JSONObject from the JSON response string
            JSONObject baseJsonResponse = new JSONObject(earthquakeJSON);

            // Extract the JSONArray associated with the key called "features",
            // which represents a list of features (or earthquakes).
            JSONArray earthquakeArray = baseJsonResponse.getJSONArray("features");

            // For each earthquake in the earthquakeArray, create an {@link Earthquake} object
            for (int i = 0; i < earthquakeArray.length(); i++) {

                // Get a single earthquake at position i within the list of earthquakes
                JSONObject currentEarthquake = earthquakeArray.getJSONObject(i);

                // For a given earthquake, extract the JSONObject associated with the
                // key called "properties", which represents a list of all properties
                // for that earthquake.
                JSONObject properties = currentEarthquake.getJSONObject("properties");

                // Extract the value for the key called "mag"
                double magnitude = properties.getDouble("mag");

                // Extract the value for the key called "place"
                String location = properties.getString("place");

                // Extract the value for the key called "time"
                long time = properties.getLong("time");

                // Create a new {@link Earthquake} object with the magnitude, location, time,
                // and url from the JSON response.
                Earthquake earthquake = new Earthquake(magnitude, location, time);

                JSONArray coordinates=currentEarthquake.getJSONObject("geometry").getJSONArray("coordinates");

                earthquake.setLongitudeAndLatitude(coordinates.getDouble(0),coordinates.getDouble(1));

                earthquake.url = properties.getString("url");
                // Add the new {@link Earthquake} to the list of earthquakes.
                earthquakes.add(earthquake);


            }

        } catch (JSONException e) {
            // If an error is thrown when executing any of the above statements in the "try" block,
            // catch the exception here, so the app doesn't crash. Print a log message
            // with the message from the exception.
            Log.e("QueryUtils", "Problem parsing the earthquake JSON results", e);
        }

        // Return the list of earthquakes
        return earthquakes;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, Settings.class);
            startActivity(settingsIntent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

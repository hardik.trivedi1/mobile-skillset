package com.volansys.trainingassignments.concurrency;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by hardik on 14/3/18.
 */

public class AlaramReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "It's an alarm!!!", Toast.LENGTH_SHORT).show();
    }
}

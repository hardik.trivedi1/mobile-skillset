package com.volansys.trainingassignments.concurrency;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class OneImageActivity extends AppCompatActivity {

    private Button mDownloadImageButton;
    private ImageView mOneImageView;
    private Button mDownloadImageAsyncButton;
    private Bitmap mImageBitmap;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_image);

        initialization();

        mDownloadImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadOneImage();
            }
        });

        mDownloadImageAsyncButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadOneImageAsync();
            }
        });
    }

    private void initialization(){
        mDownloadImageButton = findViewById(R.id.button_download_one_image_handler);
        mDownloadImageAsyncButton = findViewById(R.id.button_download_one_image_async);
        mOneImageView = findViewById(R.id.imageView_one);
        mProgressBar = findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.INVISIBLE);

    }
    private void downloadOneImage(){
        mProgressBar.setVisibility(View.VISIBLE);
       final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                mProgressBar.setVisibility(View.INVISIBLE);
                mOneImageView.setImageBitmap(mImageBitmap);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL("http://192.168.3.104/images/image4.jpg")
                                    .openConnection();
                    connection.connect();
                    mImageBitmap = BitmapFactory.decodeStream(connection.getInputStream());
                    handler.sendEmptyMessage(0);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
            }
        }).start();
    }

    private void downloadOneImageAsync(){

        class DownloadAsync extends AsyncTask<Void,Integer,Bitmap>{

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                super.onPostExecute(bitmap);
                mProgressBar.setVisibility(View.INVISIBLE);
                mOneImageView.setImageBitmap(bitmap);
            }

            @Override
            protected Bitmap doInBackground(Void... voids) {
                Bitmap tempBmp = null;
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL(getResources().getString(R.string.url))
                                    .openConnection();
                    connection.connect();
                    tempBmp = BitmapFactory.decodeStream(connection.getInputStream());
                    publishProgress(50);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
                return tempBmp;
            }

            @Override
            protected void onProgressUpdate(Integer... values) {
                super.onProgressUpdate(values);
            }

        }
        new DownloadAsync().execute();
    }
}

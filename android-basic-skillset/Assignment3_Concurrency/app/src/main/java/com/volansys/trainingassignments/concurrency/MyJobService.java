package com.volansys.trainingassignments.concurrency;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MyJobService extends JobService {
    public MyJobService() {
        super();
    }

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        Log.e("onStartJob()","Called");
        final String message = "";
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                Toast.makeText(MyJobService.this, message, Toast.LENGTH_SHORT).show();
                jobFinished(jobParameters,true);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL("http://192.168.3.104/scripts/getStudentsByName.php?rollno=1")
                                    .openConnection();
                    connection.connect();
                    message.concat(new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine());
                    handler.sendEmptyMessage(0);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
            }
        }).start();
        return false;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }


}

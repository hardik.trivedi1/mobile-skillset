package com.volansys.trainingassignments.concurrency;

import android.content.AsyncTaskLoader;
import android.content.ContentProvider;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by hardik on 5/2/18.
 */

public class ImageListAdapter extends BaseAdapter {

    private Context mContext;
    private Bitmap[] mImages;

    public ImageListAdapter(Context context,int size){
        mContext = context;
        mImages = new Bitmap[size];
    }

    @Override
    public int getCount() {
        return mImages.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rootView = LayoutInflater.from(mContext)
                .inflate(R.layout.list_item_one_image,viewGroup,false);
        showImages(rootView,position);
        return rootView;
    }

    private void showImages(View rootView,final int i){
        final ImageView imageView = rootView.findViewById(R.id.image_list_item);
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                imageView.setImageBitmap(mImages[i]);
            }
        };
        class ShowImages extends AsyncTaskLoader<Bitmap> {

            public ShowImages(Context context){
                super(context);
            }
            @Override
            public Bitmap loadInBackground() {
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL(mContext.getResources().getString(R.string.url))
                                    .openConnection();
                    connection.connect();
                    mImages[i] = BitmapFactory.decodeStream(connection.getInputStream());
                    handler.sendEmptyMessage(0);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
                return mImages[i];
            }
        }
        new ShowImages(mContext).forceLoad();
    }
}

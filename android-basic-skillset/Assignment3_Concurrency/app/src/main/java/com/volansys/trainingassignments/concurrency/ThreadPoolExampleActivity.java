package com.volansys.trainingassignments.concurrency;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.volansys.trainingassignments.concurrency.threadPoolHelpers.DefaultExecutorSupplier;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ThreadPoolExampleActivity extends AppCompatActivity {

    private Button mBackgroundTask,mMainThreadTask;
    private ImageView mImageView;
    private Bitmap mBitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread_pool_example);

        mImageView = findViewById(R.id.imageView_pool);
        mBackgroundTask = findViewById(R.id.button_backgrnd_task);
        mBackgroundTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Handler handler = new Handler(){
                    @Override
                    public void handleMessage(Message msg) {
                        mImageView.setImageBitmap(mBitmap);
                    }
                };
                DefaultExecutorSupplier.getInstance().forBackgroundTasks().execute(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            HttpURLConnection connection =
                                    (HttpURLConnection) new URL("http://192.168.3.104/images/image4.jpg")
                                            .openConnection();
                            connection.connect();
                            mBitmap = BitmapFactory.decodeStream(connection.getInputStream());
                            handler.sendEmptyMessage(0);
                        }
                        catch (MalformedURLException e){}
                        catch (IOException e){}
                    }
                });
            }
        });

        mMainThreadTask = findViewById(R.id.button_main_thread);
        mMainThreadTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DefaultExecutorSupplier.getInstance().forMainThreadTasks().execute(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(ThreadPoolExampleActivity.this,
                                "Main Thread execution here !!!",
                                Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}

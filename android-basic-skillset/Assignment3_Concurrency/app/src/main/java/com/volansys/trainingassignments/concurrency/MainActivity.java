package com.volansys.trainingassignments.concurrency;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button mDownloadOneImage;
    private Button mDownloadMoreImage;
    private Button mShowThreadPoolExample;
    private Button mSetAlarmExample;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();

        clickEvents();
    }

    private void clickEvents() {
        mDownloadOneImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,OneImageActivity.class));
            }
        });

        mDownloadMoreImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,MoreImagesActivity.class));
            }
        });

        mShowThreadPoolExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,ThreadPoolExampleActivity.class));
            }
        });
        final AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        final PendingIntent pendingIntent = PendingIntent.getBroadcast(this,
                0,
                new Intent(MainActivity.this,AlaramReceiver.class),
                0);
        mSetAlarmExample.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alarmManager.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,5000,pendingIntent);
            }
        });
    }

    private void initialization() {
        mDownloadOneImage = findViewById(R.id.button_download_one_image);
        mDownloadMoreImage = findViewById(R.id.button_download_more_images);
        mShowThreadPoolExample = findViewById(R.id.button_thread_pool_example);
        mSetAlarmExample = findViewById(R.id.button_set_alarm);

        JobScheduler jobScheduler =
                (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(new JobInfo.Builder(1,
                new ComponentName(this, MyJobService.class))
                .setPeriodic(15 * 60 * 1000)
                .build());
    }
}

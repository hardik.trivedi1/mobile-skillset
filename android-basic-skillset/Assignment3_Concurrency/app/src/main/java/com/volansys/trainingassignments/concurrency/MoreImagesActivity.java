package com.volansys.trainingassignments.concurrency;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

public class MoreImagesActivity extends AppCompatActivity {

    private ListView mImagesListView;
    private ImageListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_images);

        mImagesListView = findViewById(R.id.listview_images);
        mAdapter = new ImageListAdapter(this,4);

        mImagesListView.setAdapter(mAdapter);
    }

}

package com.volansys.junittesting;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void checkForce() throws Exception {
        assertEquals((7.2 * Math.pow(10,10)),FindForce.findForce(0.002,0.004,0.001),0.005);
    }
}
package com.volansys.junittesting;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class GreetingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_greeting);

        TextView textView = findViewById(R.id.textview_greet);
        textView.setText(getIntent().getStringExtra("greet_msg"));
    }
}

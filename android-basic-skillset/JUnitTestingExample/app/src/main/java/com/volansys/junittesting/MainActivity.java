package com.volansys.junittesting;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.vending.licensing.AESObfuscator;
import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.Policy;
import com.google.android.vending.licensing.ServerManagedPolicy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {

    public static String BASE_URL = "http://192.168.3.104/scripts/getStudentsByName.php?rollno=1";
    // Generate 20 random bytes, and put them here.
    private static final byte[] SALT = new byte[]{
            -46, 65, 30, -128, -103, -57, 74, -64, 51, 88, -95,
            -45, 77, -117, -36, -113, -11, 32, -64, 89
    };
    public static String BASE64_PUBLIC_KEY;

    private TextView mResultTextView;
    private EditText mQ1EditText, mQ2EditText, mREditText;
    private Button mFindForceButton, mGreetTestButton, mCheckWebResponseButton;

    private double mQ1, mQ2, mR, mResult;
    private String mResponseString;
    private Handler mHandler;
    private LicenseCheckerCallback mLicenseCheckerCallback;
    private LicenseChecker mChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();

        clickEvents();
    }

    private void clickEvents() {
        mFindForceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mQ1 = Double.parseDouble(mQ1EditText.getText().toString());
                mQ2 = Double.parseDouble(mQ2EditText.getText().toString());
                mR = Double.parseDouble(mREditText.getText().toString());

                mResult = FindForce.findForce(mQ1, mQ2, mR);

                mResultTextView.setText("Force : " + String.valueOf(mResult) + " N");
            }
        });

        mGreetTestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, GreetingActivity.class);
                intent.putExtra("greet_msg", "Hello there!!");
                startActivity(intent);
            }
        });

        mCheckWebResponseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Handler handler = new Handler() {
                    @Override
                    public void handleMessage(Message msg) {
                        if (msg.what == 200)
                            mResultTextView.setText(mResponseString);
                    }
                };
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            HttpURLConnection connection = (HttpURLConnection) new URL(BASE_URL).openConnection();
                            connection.connect();
                            mResponseString = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();
                            handler.sendEmptyMessage(200);
                        } catch (MalformedURLException e) {
                        } catch (IOException e) {
                        }
                    }
                }).start();
            }
        });
    }

    @SuppressLint("MissingPermission")
    private void initialization() {
        mResultTextView = findViewById(R.id.text_result);
        mQ1EditText = findViewById(R.id.editText_q1);
        mQ2EditText = findViewById(R.id.editText_q2);
        mREditText = findViewById(R.id.editText_r);
        mFindForceButton = findViewById(R.id.button_find_force);
        mGreetTestButton = findViewById(R.id.button_greet_test);
        mCheckWebResponseButton = findViewById(R.id.button_web_response);

        TelephonyManager manager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        mHandler = new Handler();

        mLicenseCheckerCallback = new MyLicenseCheckerCallback();
        BASE64_PUBLIC_KEY = getString(R.string.base_key_sample);

        mChecker = new LicenseChecker(
                this, new ServerManagedPolicy(this,
                new AESObfuscator(SALT, getPackageName(), manager.getDeviceId())),
                BASE64_PUBLIC_KEY);
        doCheck();
    }

    private void doCheck() {
        setProgressBarIndeterminateVisibility(true);
        mResultTextView.setText("Checking...");
        mChecker.checkAccess(mLicenseCheckerCallback);
    }


    private class MyLicenseCheckerCallback implements LicenseCheckerCallback {
        public void allow(int reason) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            // Should allow user access.
            displayResult("Allowed.");
        }

        public void dontAllow(int reason) {
            if (isFinishing()) {
                // Don't update UI if Activity is finishing.
                return;
            }
            displayResult("Don't allow.");

            if (reason == Policy.RETRY) {
                // If the reason received from the policy is RETRY, it was probably
                // due to a loss of connection with the service, so we should give the
                // user a chance to retry. So show a dialog to retry.
                showDialog(1);
            } else {
                // Otherwise, the user is not licensed to use this app.
                // Your response should always inform the user that the application
                // is not licensed, but your behavior at that point can vary. You might
                // provide the user a limited access version of your app or you can
                // take them to Google Play to purchase the app.
                showDialog(2);
            }
        }

        @Override
        public void applicationError(int errorCode) {

        }
    }

    private void displayResult(final String result) {
        mHandler.post(new Runnable() {
            public void run() {
                mResultTextView.setText(result);
                setProgressBarIndeterminateVisibility(false);
               // mCheckLicenseButton.setEnabled(true);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mChecker.onDestroy();
    }
}

package com.volansys.junittesting;

import java.util.Random;

/**
 * Created by hardik on 3/4/18.
 */

public class FindForce {

    public static double findForce(double q1,double q2,double r){
        return ((9 * Math.pow(10,9)) * ((q1 * q2) / Math.pow(r,2)));
    }

    public int getDummyIdForMockito(){ return (new Random().nextInt(100) + 10);}
}

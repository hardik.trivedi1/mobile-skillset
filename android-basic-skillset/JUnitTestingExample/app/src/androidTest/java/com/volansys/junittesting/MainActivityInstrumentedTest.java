package com.volansys.junittesting;

import android.app.Activity;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.app.AppCompatActivity;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;

/**
 * Created by hardik on 3/4/18.
 */

public class MainActivityInstrumentedTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private AppCompatActivity mActivity;
    private TextView mResultTextView;
    private EditText mQ1EditText,mQ2EditText,mREditText;
    private Button mFindForceButton;

    public MainActivityInstrumentedTest() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
        mResultTextView = mActivity.findViewById(R.id.text_result);
        mQ1EditText = mActivity.findViewById(R.id.editText_q1);
        mQ2EditText = mActivity.findViewById(R.id.editText_q2);
        mREditText = mActivity.findViewById(R.id.editText_r);
        mFindForceButton = mActivity.findViewById(R.id.button_find_force);
    }

    public void testPreconditions() {
        // Try to add a message to add context to your assertions.
        // These messages will be shown if
        // a tests fails and make it easy to
        // understand why a test failed
        assertNotNull("mMainActivity is null", mActivity);
        assertNotNull("mQ1EditText is null",mQ1EditText);
        assertNotNull("mQ2EditText is null",mQ2EditText);
        assertNotNull("mREditText is null",mREditText);
        assertNotNull("mResultTextView is null",mResultTextView);
    }


    public void testCheckForce() throws Exception{
        Espresso.onView(ViewMatchers.withId(R.id.editText_q1)).perform(ViewActions.typeText("0.002"));
        Espresso.onView(ViewMatchers.withId(R.id.editText_q2)).perform(ViewActions.typeText("0.004"));
        Espresso.onView(ViewMatchers.withId(R.id.editText_r)).perform(ViewActions.typeText("0.001"));
        Espresso.onView(ViewMatchers.withId(R.id.button_find_force)).perform(ViewActions.click());
        assertEquals((7.2 * Math.pow(10,10)),Double.parseDouble(mResultTextView.getText().toString().split(" ")[2]),0.005);
    }

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void testShouldShowHelloWorldForEmptyIntent() {
        onView(withId(R.id.textView)).check(matches(withText("Find Force between Two charges")));
    }
}

package com.volansys.junittesting;

import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.action.ViewActions;
import android.test.ActivityInstrumentationTestCase2;

import com.google.android.vending.licensing.LicenseChecker;
import com.google.android.vending.licensing.LicenseCheckerCallback;
import com.google.android.vending.licensing.ServerManagedPolicy;
import com.google.android.vending.licensing.AESObfuscator;

import com.squareup.okhttp.mockwebserver.MockResponse;
import com.squareup.okhttp.mockwebserver.MockWebServer;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by hardik on 4/4/18.
 */

public class MainActivityMockitoTest extends ActivityInstrumentationTestCase2<MainActivity> {

    private MockWebServer mServer;

    public MainActivityMockitoTest(){
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getActivity();
        mServer = new MockWebServer();
        mServer.start();
        injectInstrumentation(InstrumentationRegistry.getInstrumentation());
        MainActivity.BASE_URL = mServer.url("/").toString();
    }

    @Test
    public void testFindForceMethod() throws Exception{
        FindForce test = Mockito.mock(FindForce.class);
        Mockito.when(test.getDummyIdForMockito()).thenReturn(18);
        test.getDummyIdForMockito();
        test.getDummyIdForMockito();
        Mockito.verify(test,Mockito.times(2)).getDummyIdForMockito();
        assertEquals(18,test.getDummyIdForMockito());

        test.findForce(0.002,0.004,0.001);
        Mockito.verify(test).findForce(ArgumentMatchers.eq(0.002),ArgumentMatchers.eq(0.001),ArgumentMatchers.eq(0.001));
    }

    @Test
    public void testResponseWithMockito() throws Exception{
        onView(withId(R.id.button_web_response)).perform(ViewActions.click());
        mServer.enqueue(new MockResponse()
                .setResponseCode(200)
                .setBody(getActivity().getResources().getString(R.string.response)));
        onView(withId(R.id.text_result)).check(matches(withText(getActivity().getResources().getString(R.string.response))));
    }
}

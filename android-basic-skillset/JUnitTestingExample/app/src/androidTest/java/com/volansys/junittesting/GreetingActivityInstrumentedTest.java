package com.volansys.junittesting;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.action.ViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by hardik on 3/4/18.
 */

public class GreetingActivityInstrumentedTest extends ActivityInstrumentationTestCase2<GreetingActivity> {

    private TextView textView;

    @Rule
    public ActivityTestRule<GreetingActivity> testRule =
            new ActivityTestRule<GreetingActivity>(GreetingActivity.class,true,false);


    public GreetingActivityInstrumentedTest() {
        super(GreetingActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        textView = getActivity().findViewById(R.id.textview_greet);
        Context targetContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        Intent result = new Intent(targetContext, GreetingActivity.class);
        result.putExtra("greet_msg", "Hello World!!");
        testRule.launchActivity(result);
    }

    public void testTextOfGreetTextView() throws Exception{
        onView(withId(R.id.textview_greet)).check(matches(withText("Hello World!!")));
    }

}

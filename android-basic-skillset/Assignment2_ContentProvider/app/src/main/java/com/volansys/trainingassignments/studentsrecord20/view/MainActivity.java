package com.volansys.trainingassignments.studentsrecord20.view;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.volansys.trainingassignments.studentsrecord20.R;
import com.volansys.trainingassignments.studentsrecord20.model.Student;
import com.volansys.trainingassignments.studentsrecord20.model.StudentContract;
import com.volansys.trainingassignments.studentsrecord20.reactive.DataObservable;
import com.volansys.trainingassignments.studentsrecord20.reactive.Observer;

import java.util.ArrayList;

/**
 * MainActivity is used to show model of Students stored in Database using RecyclerView.Along with it,
 * there are two menu options for adding or changing user's name and having filtered results in text
 * file.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public class MainActivity extends AppCompatActivity implements Observer,
                                                            LoaderManager.LoaderCallbacks<Cursor> {

    private ArrayList<Student> mStudents;
    private RecyclerView mStudentsListRecyclerView;
    private StudentDataAdapter mStudentDataAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        checkPermissions();

    }

    private void showData(){
        if(getPreferences(Context.MODE_PRIVATE).contains("username"))
            Toast.makeText(this,
                    "Welcome " + getPreferences(Context.MODE_PRIVATE)
                                                    .getString("username","user"),
                    Toast.LENGTH_SHORT).show();
        getSupportLoaderManager().initLoader(0,null,this);
        DataObservable.getInstance().subscribe(this);
    }
    private void checkPermissions() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

             ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    0);
        }
        else{
            showData();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showData();
                } else {
                    finish();
                }
                return;
            }
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        DataObservable.getInstance().unsubscribe(this);
    }

    private void loadStudentsList(Cursor cursor) {
        if(cursor.getCount() != 0) {
            mStudents = new ArrayList<>();
            cursor.moveToFirst();
            do {
                mStudents.add(new Student(
                        cursor.getInt(cursor.getColumnIndexOrThrow(StudentContract.COLOUMN_ENROLLMENT_NO)),
                        cursor.getString(cursor.getColumnIndexOrThrow(StudentContract.COLOUMN_NAME)),
                        cursor.getString(cursor.getColumnIndexOrThrow(StudentContract.COLOUMN_STREAM))));
            } while (cursor.moveToNext());

            mStudentsListRecyclerView = findViewById(R.id.recyclerview_student_data);
            mStudentsListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
            mStudentDataAdapter = new StudentDataAdapter(this, mStudents);

            mStudentsListRecyclerView.setAdapter(mStudentDataAdapter);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main,menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item_change_name:
                final EditText takeUsername = new EditText(MainActivity.this);
                takeUsername.setScaleX(0.85f);

                if(getPreferences(Context.MODE_PRIVATE).contains("username"))
                    takeUsername.setHint("Your current username : " +
                        getPreferences(Context.MODE_PRIVATE).getString("username",null));
                else takeUsername.setHint("You don't have set the username.");

                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Change your name ")
                        .setView(takeUsername)
                        .setPositiveButton("Change", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getPreferences(Context.MODE_PRIVATE).edit()
                                        .putString("username",takeUsername.getText().toString())
                                        .commit();
                            }
                        })
                        .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
                return true;

            case R.id.item_add_student:
                startActivity(new Intent(this,AddStudentActivity.class));
                return true;

            case R.id.item_filter_data:
                startActivity(new Intent(MainActivity.this,FilterStudentsActivity.class));
                return true;

            case R.id.item_show_contacts:
                startActivity(new Intent(this,ContactsShowingActivity.class));

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onDataChanged() {
        getSupportLoaderManager().restartLoader(0,null,this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        return new CursorLoader(this,
                StudentContract.URI_WHOLE_TABLE,
                StudentContract.COLOUMNS,
                null,
                null,
                null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        loadStudentsList(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {

    }
}

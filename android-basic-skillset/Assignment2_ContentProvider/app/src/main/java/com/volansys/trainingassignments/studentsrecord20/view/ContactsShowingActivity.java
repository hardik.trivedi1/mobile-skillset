package com.volansys.trainingassignments.studentsrecord20.view;

import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.content.CursorLoader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.volansys.trainingassignments.studentsrecord20.R;

public class ContactsShowingActivity extends AppCompatActivity
                                                implements LoaderManager.LoaderCallbacks<Cursor>{

    private static final String[] COLOUMNS =
                                new String[]{ContactsContract.Contacts.DISPLAY_NAME_PRIMARY};
    private static final int[] IDS = new int[]{R.id.listitem_textview};
    private static final String[] PROJECTION =
            {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.LOOKUP_KEY,
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY

            };
    private static final int CONTACT_ID_INDEX = 0;
    private static final int CONTACT_KEY_INDEX = 0;

    private ListView mContactsListView;
    private long mContactId;
    private String mContactKey;
    private Uri mContactUri;
    private SimpleCursorAdapter mCursorAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_showing);

        initialization();

        mContactsListView.setAdapter(mCursorAdapter);

        clickEvents();
    }

    private void initialization() {
        mContactsListView = findViewById(R.id.listview_contacts);
        mCursorAdapter = new SimpleCursorAdapter(this,
                R.layout.custom_list_item_2,
                null,
                COLOUMNS,
                IDS,
                0);
        getLoaderManager().initLoader(0, null, this);
    }


    private void clickEvents() {
        mContactsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {


            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return  new CursorLoader(
                this,
                ContactsContract.Contacts.CONTENT_URI,
                PROJECTION,
                null,
                null,
                null
        );

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mCursorAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }
}

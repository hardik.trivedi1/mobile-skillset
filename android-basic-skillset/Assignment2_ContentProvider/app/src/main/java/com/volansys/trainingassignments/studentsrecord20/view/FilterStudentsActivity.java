package com.volansys.trainingassignments.studentsrecord20.view;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.volansys.trainingassignments.studentsrecord20.R;
import com.volansys.trainingassignments.studentsrecord20.model.StudentContract;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class FilterStudentsActivity extends AppCompatActivity {

    private Button mFilterDataButton;
    private TextView mFileDataTextView;
    private Button mSaveDataInFileButton;
    private File mAppDirectory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_students);

        initializations();

        clickEvents();
    }

    private void initializations() {
        mFilterDataButton = findViewById(R.id.button_filter_data);
        mFileDataTextView = findViewById(R.id.textView_file_data);
        mSaveDataInFileButton = findViewById(R.id.button_save_data);
        mAppDirectory = new File(Environment.getExternalStorageDirectory(),"Students Record");
    }

    private void clickEvents() {
        final String[] streams = new String[]{"Science","Commerce","Arts"};
        final StringBuilder filteredData = new StringBuilder();
        mFilterDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final ArrayList<String> selectedStreams = new ArrayList<>();
                filteredData.delete(0,filteredData.capacity()-1);
                new AlertDialog.Builder(FilterStudentsActivity.this)
                        .setTitle("Choose stream(s)")
                        .setMultiChoiceItems(streams, null, new DialogInterface.OnMultiChoiceClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int position, boolean checked) {
                                if(checked)
                                    selectedStreams.add(streams[position]);
                                else
                                    selectedStreams.remove(streams[position]);
                            }
                        })
                        .setPositiveButton("Show", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Cursor cursor = getContentResolver().query(StudentContract.URI_FILTERED_STREAM,
                                        StudentContract.COLOUMNS,
                                        StudentContract.COLOUMN_STREAM,
                                        selectedStreams.toArray(new String[selectedStreams.size()]),
                                        StudentContract.COLOUMN_ENROLLMENT_NO);
                                if(cursor.getCount() != 0){
                                    cursor.moveToFirst();
                                    do{
                                        filteredData.append("Name : " +
                                                cursor.getString(cursor.getColumnIndexOrThrow(StudentContract.COLOUMN_NAME))
                                                                + "\t\t\tStream : " +
                                                cursor.getString(cursor.getColumnIndexOrThrow(StudentContract.COLOUMN_STREAM))
                                                                + "\n" );
                                    }while (cursor.moveToNext());
                                    mFileDataTextView.setText(filteredData);
                                }
                            }
                        })
                        .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        })
                        .create().show();
            }
        });

        mSaveDataInFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!mAppDirectory.exists())
                    mAppDirectory.mkdir();
                File dataTxtFile =
                        new File(mAppDirectory,"Student Data " + new Date().getTime() + ".txt");
                try {
                    FileWriter writer = new FileWriter(dataTxtFile);
                    writer.append(filteredData);
                    writer.flush();
                    writer.close();
                    Toast.makeText(FilterStudentsActivity.this,
                            "File saved successfully at " + dataTxtFile.getAbsolutePath(),
                            Toast.LENGTH_SHORT).show();
                }catch (IOException e){
                    Toast.makeText(FilterStudentsActivity.this,
                            "Problem in saving file. Try again!!!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

}

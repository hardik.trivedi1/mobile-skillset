package com.volansys.trainingassignments.studentsrecord20.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * StudentDatabaseHelper class which extends SQLiteOpenHelper class is used for handling Database
 * operations.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018.
 */

public class StudentDatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "student.db";
    public static int DATABASE_VERSION = 1;

    public StudentDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(StudentContract.CREATE_TABLE_STUDENTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL(StudentContract.DROP_TABLE_STUDENTS);
        onCreate(sqLiteDatabase);
    }

    public SQLiteDatabase getDatabase(){
        return getWritableDatabase();
    }
}

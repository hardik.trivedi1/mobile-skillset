package com.volansys.trainingassignments.studentsrecord20.reactive;

/**
 * This is Observer interface, which holds methods necessary for Any Observer Object.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public interface Observer {
    void onDataChanged();
}

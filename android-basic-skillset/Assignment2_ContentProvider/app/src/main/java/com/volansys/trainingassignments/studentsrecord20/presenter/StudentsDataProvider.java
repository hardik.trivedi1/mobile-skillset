package com.volansys.trainingassignments.studentsrecord20.presenter;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.volansys.trainingassignments.studentsrecord20.model.StudentContract;
import com.volansys.trainingassignments.studentsrecord20.model.StudentDatabaseHelper;

/**
 * StudentsDataProvider class, which extends ContentProvider class, facilitates our app and other app
 *  who may have content uri, to query database.
 *  This class acts as "presenter" in MVP pattern.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 01-02-2018
 */

public class StudentsDataProvider extends ContentProvider {

    private StudentDatabaseHelper mDatabaseHelper;

    private static UriMatcher sMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sMatcher.addURI("com.volansys.trainingassignments.studentsrecord20",
                        "students",0);
        sMatcher.addURI("com.volansys.trainingassignments.studentsrecord20",
                        "students/*",1);
    }
    @Override
    public boolean onCreate() {
        mDatabaseHelper = new StudentDatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri,
                        @Nullable String[] coloumns,
                        @Nullable String selectionClause,
                        @Nullable String[] selectionArgs,
                        @Nullable String orderBy) {
        switch (sMatcher.match(uri)){
            case 0:
                return mDatabaseHelper.getReadableDatabase()
                                      .query(StudentContract.TABLE_NAME,
                                              coloumns,
                                              null,
                                              null,
                                              null,
                                              null,
                                              orderBy);
            case 1:
                return mDatabaseHelper.getReadableDatabase()
                                      .query(StudentContract.TABLE_NAME,
                                              coloumns,
                                              StudentContract.COLOUMN_STREAM + "=? OR " +
                                                      StudentContract.COLOUMN_STREAM + "=? OR " +
                                                      StudentContract.COLOUMN_STREAM + "=?",
                                              selectionArgs,
                                              null,
                                              null,
                                              orderBy);
            default:
                throw new IllegalArgumentException("Invalid URI");
        }

    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri,
                      @Nullable ContentValues contentValues) throws SQLiteConstraintException{
        mDatabaseHelper.getDatabase()
                       .insertOrThrow(StudentContract.TABLE_NAME,null,contentValues);
        return uri;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String whereClause, @Nullable String[] whereArgs) {
        return mDatabaseHelper.getWritableDatabase()
                              .delete(StudentContract.TABLE_NAME,whereClause,whereArgs);
    }

    @Override
    public int update(@NonNull Uri uri,
                      @Nullable ContentValues contentValues,
                      @Nullable String whereClause,
                      @Nullable String[] strings) {
        return mDatabaseHelper.getWritableDatabase().update(StudentContract.TABLE_NAME,
                                                        contentValues,
                                                        whereClause,
                                                        strings);
    }
}

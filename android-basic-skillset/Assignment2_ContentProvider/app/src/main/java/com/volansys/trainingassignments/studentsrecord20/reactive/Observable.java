package com.volansys.trainingassignments.studentsrecord20.reactive;

/**
 * This is Observable interface, which holds methods necessary for Any Observable Object.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public interface Observable {
    void subscribe(Observer observer);
    void notifyObservers();
    void unsubscribe(Observer observer);
}

package com.volansys.hardiktrivedi.javafeatures;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private TextView mInstructionTextView;
    private Switch mToggleBackgroundSwitch;
    private RelativeLayout mMainLayout;
    private EditText mTakeNameEditText;
    private Button mShowAgeButton;
    private EditText mPlayNameEditText,mPlayerNameEditText,mPlayerTshirtNumberEditText;
    private Button mShowPlayerDetailsButton;
    private TextView mShowPlayersTextView;
    private EditText mTakeTextEditText;
    private Button mShowAdditionButton;
    private TextView mAnswerTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mInstructionTextView = findViewById(R.id.textView);
        mToggleBackgroundSwitch = findViewById(R.id.switch1);
        mMainLayout = findViewById(R.id.main_layout);
        mTakeNameEditText = findViewById(R.id.editText);
        mShowAgeButton = findViewById(R.id.button);
        mPlayNameEditText = findViewById(R.id.editText3);
        mPlayerNameEditText  = findViewById(R.id.editText4);
        mPlayerTshirtNumberEditText = findViewById(R.id.editText5);
        mShowPlayerDetailsButton = findViewById(R.id.button3);
        mShowPlayersTextView = findViewById(R.id.textView7);
        mTakeTextEditText = findViewById(R.id.editText6);
        mShowAdditionButton = findViewById(R.id.button5);
        mAnswerTextView = findViewById(R.id.textView8);

        if(mToggleBackgroundSwitch.isChecked()){
            mInstructionTextView.setText("Switch to light background.");
            mMainLayout.setBackgroundColor(Color.GRAY);
        }
        else{
            mInstructionTextView.setText("Switch to dark background");
            mMainLayout.setBackgroundColor(Color.WHITE);
        }

        mToggleBackgroundSwitch.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    mInstructionTextView.setText("Switch to light background.");
                    mMainLayout.setBackgroundColor(Color.GRAY);
                }
                else{
                    mInstructionTextView.setText("Switch to dark background");
                    mMainLayout.setBackgroundColor(Color.WHITE);
                }
            }
        });

        final HashMap<String,String> data = new HashMap<>();
        data.put("Hardik","20");
        data.put("Dipu","22");
        data.put("Dolly","22");
        data.put("Pooja","20");

        mShowAgeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,
                        "The age is "  + data.get(mTakeNameEditText.getText().toString()),
                             Toast.LENGTH_LONG).show();
            }
        });

        mShowPlayerDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Player player = new Player(mPlayNameEditText.getText().toString(),
                        mPlayerNameEditText.getText().toString(),
                        Integer.parseInt(mPlayerTshirtNumberEditText.getText().toString()));

                mShowPlayersTextView.setText("Play : " + player.getPlayname() + "\n" +
                                             "Player Name : " + player.getPlayerName() + "\n" +
                                             "T-Shirt Number : " + player.getPlayerTshirtNumber());
            }
        });

        mShowAdditionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String temp = mTakeTextEditText.getText().toString();
                if(temp.split(",").length == 2)
                    showAddition(temp.split(",")[0],temp.split(",")[1]);
                else if(temp.split(",").length == 3)
                    showAddition(temp.split(",")[0],
                            temp.split(",")[1],
                            temp.split(",")[2]);
                else
                    showAddition(temp);
            }
        });
    }


    private void showAddition(String arg1,String arg2){
        int intArg1 = Integer.parseInt(arg1),
                intArg2 = Integer.parseInt(arg2);
        int answer = intArg1+intArg2;
        mAnswerTextView.setText(String.valueOf(answer));
    }
    private void showAddition(String arg1,String arg2,String arg3){
        int intArg1 = Integer.parseInt(arg1),
                intArg2 = Integer.parseInt(arg2),
                intArg3 = Integer.parseInt(arg3);
        int answer = intArg1+intArg2+intArg3;
        mAnswerTextView.setText(String.valueOf(answer));
    }
    private void showAddition(String arg){
        int answer = 0;
        for(int i=0;i<arg.split(",").length;i++){
            answer  = answer + Integer.parseInt(arg.split(",")[i]);
        }
        mAnswerTextView.setText(String.valueOf(answer));
    }
}


interface IPlay{
    void setPlayName(String playName);
    String getPlayname();
    void setPlayerName(String playerName);
    String getPlayerName();
}

class Play implements IPlay{
    private String mPlayName;
    private String mPlayerName;

    Play(String playName,String playerName){
        mPlayName = playName;
        mPlayerName = playerName;
    }
    @Override
    public void setPlayName(String playName) {
        mPlayName = playName;
    }

    @Override
    public String getPlayname() {
        return mPlayName;
    }

    @Override
    public void setPlayerName(String playerName) {
        mPlayerName = playerName;
    }

    @Override
    public String getPlayerName() {
        return mPlayerName;
    }
}

class Player extends Play{
    private int mTshirtNumber;

    Player(String playName,String playerName,int tShirtNumber){
        super(playName,playerName);
        mTshirtNumber = tShirtNumber;
    }

    public void setPlayerTshirtNumber(int tShirtNumber){
        mTshirtNumber = tShirtNumber;
    }

    public int getPlayerTshirtNumber(){
        return mTshirtNumber;
    }


}

package com.volansys.trainingassignments.studentsrecord.data;

/**
 * StudentContract class is used for storing constants that may need in various parts of app.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public class StudentContract {

    public static String TABLE_NAME = "students";
    public static String COLOUMN_ENROLLMENT_NO = "number";
    public static String COLOUMN_NAME = "name";
    public static String COLOUMN_STREAM = "stream";
    public static String[] COLOUMNS = {COLOUMN_ENROLLMENT_NO,COLOUMN_NAME,COLOUMN_STREAM};

    public static String CREATE_TABLE_STUDENTS = "CREATE TABLE students " +
            "(number INTEGER PRIMARY KEY," +
            " name TEXT NOT NULL," +
            " stream TEXT NOT NULL);";
    public static String DROP_TABLE_STUDENTS = "DROP TABLE IF EXISTS students";
}

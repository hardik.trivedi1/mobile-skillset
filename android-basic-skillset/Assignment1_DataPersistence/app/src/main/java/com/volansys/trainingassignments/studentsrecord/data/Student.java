package com.volansys.trainingassignments.studentsrecord.data;

/**
 * Student class, which holds data of students like Student's no,Student's name and Stream.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public class Student {

    private int mEnrollmentNumber;
    private String mName;
    private String mStream;

    public Student(int number,String name,String stream){
        mEnrollmentNumber = number;
        mName = name;
        mStream = stream;
    }

    public void setEnrollmentNumber(int number){
        mEnrollmentNumber = number;
    }
    public int getEnrollmentNumber(){
        return mEnrollmentNumber;
    }

    public void setName(String name){
        mName = name;
    }
    public String getName(){
        return mName;
    }

    public void setStream(String stream){
        mStream = stream;
    }
    public String getStream(){
        return mStream;
    }
}

package com.volansys.trainingassignments.studentsrecord.reactive;

import java.util.ArrayList;

/**
 * DataObservable class, which implements Observable interface
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public class DataObservable implements Observable {


    private ArrayList<Observer> mObservers;
    private static DataObservable INSTANCE;

    private DataObservable(){
        mObservers = new ArrayList<>();
    }

    public static DataObservable getInstance(){
        if(INSTANCE == null)
            INSTANCE = new DataObservable();
        return INSTANCE;
    }

    @Override
    public void subscribe(Observer observer) {
        if(!mObservers.contains(observer))
            mObservers.add(observer);
    }

    @Override
    public void notifyObservers() {
        for (Observer data : mObservers) {
            data.onDataChanged();
        }
    }

    @Override
    public void unsubscribe(Observer observer) {
        if(mObservers.contains(observer))
            mObservers.remove(observer);
    }
}

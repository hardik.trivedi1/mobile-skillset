package com.volansys.trainingassignments.studentsrecord.ui;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.volansys.trainingassignments.studentsrecord.R;
import com.volansys.trainingassignments.studentsrecord.data.StudentContract;
import com.volansys.trainingassignments.studentsrecord.data.StudentDatabaseHelper;
import com.volansys.trainingassignments.studentsrecord.reactive.DataObservable;

/**
 * AddStudentActivity activity is used to insert Student's data in Database.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public class AddStudentActivity extends AppCompatActivity {

    private static String TAG = "AddStudentActivity";

    private EditText mTakeNumberEditText;
    private EditText mTakeNameEditText;
    private EditText mTakeStreamEditText;
    private Button mAddStudentButton;
    private StudentDatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);

        initializations();

        onClickEvents();

    }

    private void initializations() {
        mTakeNumberEditText = findViewById(R.id.editText_number);
        mTakeNameEditText = findViewById(R.id.editText_name);
        mTakeStreamEditText = findViewById(R.id.editText_stream);
        mAddStudentButton = findViewById(R.id.button_add_student);
        mDatabaseHelper = new StudentDatabaseHelper(this);

        if(getIntent().getStringExtra("update") != null)
            mAddStudentButton.setText("Update");
    }

    private void onClickEvents() {
       mAddStudentButton.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {
               ContentValues data = new ContentValues();
               data.put(StudentContract.COLOUMN_ENROLLMENT_NO,
                       Integer.valueOf(mTakeNumberEditText.getText().toString()));
               data.put(StudentContract.COLOUMN_NAME, mTakeNameEditText.getText().toString());
               data.put(StudentContract.COLOUMN_STREAM, mTakeStreamEditText.getText().toString());

               if(mAddStudentButton.getText().toString().equals("Add Student")) {
                   try {
                       mDatabaseHelper.getDatabase().insertOrThrow(StudentContract.TABLE_NAME,
                               null,
                               data);
                       DataObservable.getInstance().notifyObservers();
                       new AlertDialog.Builder(AddStudentActivity.this)
                               .setTitle("Student added successfully.")
                               .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialogInterface, int i) {
                                       finish();
                                   }
                               })
                               .create().show();
                   } catch (SQLiteConstraintException e) {
                       new AlertDialog.Builder(AddStudentActivity.this)
                               .setTitle("This entry has already been done.")
                               .setMessage("Do you wish to update this entry?")
                               .setPositiveButton("Update", new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialogInterface, int i) {
                                       mAddStudentButton.setText("Update");
                                   }
                               })
                               .setNegativeButton("Leave", new DialogInterface.OnClickListener() {
                                   @Override
                                   public void onClick(DialogInterface dialogInterface, int i) {
                                       finish();
                                   }
                               })
                               .create()
                               .show();
                   }
               }
               else{
                  mDatabaseHelper.getDatabase()
                          .update(StudentContract.TABLE_NAME,
                                  data,
                                  StudentContract.COLOUMN_ENROLLMENT_NO +
                                          "=" +
                                          Integer.valueOf(mTakeNumberEditText.getText().toString()),
                                  null);
                   DataObservable.getInstance().notifyObservers();
                   new AlertDialog.Builder(AddStudentActivity.this)
                           .setTitle("Student info updated successfully.")
                           .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialogInterface, int i) {
                                   finish();
                               }
                           })
                           .create().show();
               }
           }
       });
    }

}

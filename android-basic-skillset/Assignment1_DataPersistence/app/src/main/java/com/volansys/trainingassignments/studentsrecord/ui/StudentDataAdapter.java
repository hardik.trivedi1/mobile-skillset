package com.volansys.trainingassignments.studentsrecord.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.volansys.trainingassignments.studentsrecord.R;
import com.volansys.trainingassignments.studentsrecord.data.Student;
import com.volansys.trainingassignments.studentsrecord.data.StudentContract;
import com.volansys.trainingassignments.studentsrecord.data.StudentDatabaseHelper;
import com.volansys.trainingassignments.studentsrecord.reactive.DataObservable;

import java.util.ArrayList;

/**
 * StudentDataAdapter is a class,which extends RecyclerView.Adapter class, for binding data with
 * RecyclerView.
 * It contains one inner static class StudentDataHolder which extends RecyclerView.ViewHoler class,
 * used for holding view data.
 *
 * @author Hardik Trivedi
 * @version 1.0
 * @since 31-01-2018
 */

public class StudentDataAdapter extends RecyclerView.Adapter<StudentDataAdapter.StudentDataHolder> {

    public static class StudentDataHolder extends RecyclerView.ViewHolder{

        public TextView studentNumberTextView;
        public TextView studentsNameTextView;
        public TextView studentsStreamTextView;
        public StudentDataHolder(View itemView) {
            super(itemView);
            studentNumberTextView = itemView.findViewById(R.id.text_student_no);
            studentsNameTextView = itemView.findViewById(R.id.text_student_name);
            studentsStreamTextView = itemView.findViewById(R.id.text_student_stream);
        }
    }

    private Activity mContext;
    private ArrayList<Student> mStudents;

    public StudentDataAdapter(Activity context,ArrayList<Student> students){
        mContext = context;
        mStudents = students;
    }

    @Override
    public StudentDataHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(mContext)
                                      .inflate(R.layout.custom_list_item,parent,false);
        final String[] items = new String[]{"Update","Delete"};
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(mContext)
                        .setItems(items,
                            new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (items[i].equals("Update")){
                                    mContext.startActivity(new Intent(mContext,AddStudentActivity.class)
                                                        .putExtra("update","Update"));
                                }
                                else if (items[i].equals("Delete")){
                                    final TextView tempView = itemView.findViewById(R.id.text_student_no);
                                    new AlertDialog.Builder(mContext)
                                            .setTitle("Do you want to delete this Student's info?")
                                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    new StudentDatabaseHelper(mContext)
                                                            .getDatabase()
                                                            .delete(StudentContract.TABLE_NAME,
                                                                    StudentContract.COLOUMN_ENROLLMENT_NO
                                                                            + "=" +
                                                                            tempView.getText().toString(),
                                                                    null);
                                                    DataObservable.getInstance().notifyObservers();
                                                }
                                            })
                                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialogInterface, int i) {
                                                    dialogInterface.dismiss();
                                                }
                                            })
                                            .create().show();

                                }
                            }
                        })
                        .create().show();
            }
        });
        return new StudentDataHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentDataHolder holder, int position) {
        holder.studentNumberTextView
              .setText(String.valueOf(mStudents.get(position).getEnrollmentNumber()));
        holder.studentsNameTextView.setText(mStudents.get(position).getName());
        holder.studentsStreamTextView.setText(mStudents.get(position).getStream());
    }

    @Override
    public int getItemCount() {
        return mStudents.size();
    }
}

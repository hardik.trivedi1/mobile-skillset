package com.volansys.trainingassignments.networking;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AsyncTaskLoader;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


import com.volansys.trainingassignments.services.IRemoteService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlpull.v1.XmlPullParserException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MalformedURLException;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.URL;
import java.net.UnknownHostException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;




/**
 * MainActivity, used for exhibiting various network related task.
 * @author Hardik Trivedi
 * @since 06-02-2018
 * @version 1.0
 */

public class MainActivity extends AppCompatActivity {

    private static final String[] ITEMS = new String[]{"SAX","DOM","PullParser"};
    private Button mShowNetworkConnectivitiesButton;
    private Button mParseXmlButton;
    private String mDomData,mJsonData;
    private Button mParseJsonButton;
    private Button mShowNotificationButton;

    private Button mGreetUsingService;
    private IRemoteService mService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialization();
        clickEvents();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if(mService == null){
            Intent intent = new Intent("com.volansys.trainingassignments.services.RemoteService");
            intent.setPackage("com.volansys.trainingassignments.services");
            bindService(intent,mRemoteConnection,Context.BIND_AUTO_CREATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        unbindService(mRemoteConnection);
    }

    private void initialization() {
        mShowNetworkConnectivitiesButton = findViewById(R.id.button_show_connections);
        mParseXmlButton = findViewById(R.id.button_parse_xml);
        mParseJsonButton = findViewById(R.id.button_parse_json);
        mShowNotificationButton = findViewById(R.id.button_show_notification);

        mGreetUsingService = findViewById(R.id.button_aidl);
    }

    private void clickEvents(){
        mShowNetworkConnectivitiesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String connections = getConnectivities();
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Nework Connectivities of Phone")
                        .setMessage(connections.split(",")[0] + "\n" +
                                    connections.split(",")[1] + "\n" +
                                    connections.split(",")[2] + "\n" +
                                    connections.split(",")[3])
                        .create().show();
            }
        });

        mParseXmlButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MainActivity.this)
                        .setItems(ITEMS, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if(ITEMS[i].equals("SAX")){
                                    showSAXParsedData();
                                }
                                else if (ITEMS[i].equals("DOM")){
                                    showDOMParsedData();
                                }
                                else if (ITEMS[i].equals("PullParser")){
                                    showPullParsedData();
                                }
                            }
                        })
                        .create().show();
            }
        });

        mParseJsonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showJSONDataParsed();
            }
        });

        mShowNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int notificationId = 0;
                Intent intent = new Intent(MainActivity.this,MainActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(MainActivity.this,
                        0,
                        intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                NotificationManager manager =
                        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                NotificationCompat.Builder notification =
                        new NotificationCompat.Builder(MainActivity.this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Sample notificaton")
                        .setContentText("Notification from Networking")
                        .setContentIntent(pendingIntent)
                        .setAutoCancel(true);
                manager.notify(notificationId,notification.build());
                for(int i = 0; i < 10; i++) {
                    try{Thread.sleep(1000);}catch (InterruptedException e){}
                    notification.setContentText("Count " + i);
                    manager.notify(notificationId, notification.build());
                }
                for (int i = 1; i <= 100; i++){
                    try{Thread.sleep(500);}catch (InterruptedException e){}
                    notification.setProgress(100,i,false);
                    manager.notify(notificationId,notification.build());
                }
                manager.cancel(notificationId);
                Notification notification2 =
                        new NotificationCompat.Builder(MainActivity.this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("Sample notificaton")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText("This is text in big view."))
                        .addAction(0,"Show",pendingIntent)
                        .setAutoCancel(true)
                        .build();
                manager.notify(1,notification2);
            }
        });

        mGreetUsingService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Toast.makeText(MainActivity.this,
                            mService.getMessage("Hardik"), Toast.LENGTH_SHORT).show();
                }
                catch (RemoteException e){}
            }
        });

    }

    private ServiceConnection mRemoteConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = IRemoteService.Stub.asInterface((IBinder) iBinder);
            Toast.makeText(MainActivity.this, "Service connected.", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
            Toast.makeText(MainActivity.this, "Service disconnected.", Toast.LENGTH_SHORT).show();
        }
    };

    @TargetApi(Build.VERSION_CODES.N)
    private String getConnectivities(){

        String connections = "";
        ConnectivityManager manager =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if(manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected())
            connections = connections + "WiFi is connected.";
        else
            connections = connections + "WiFi is disconnected.";

        if(manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnected())
            connections = connections + ",Mobile data is on.";
        else
            connections = connections + ",Mobile data is off.";

       /* if(manager.getActiveNetworkInfo() != null && manager.getActiveNetworkInfo().isConnected()){
            try {
                if (InetAddress.getByName("www.google.com").isReachable(2000))
                    connections = connections + ",Internet is available.";
                else
                    connections = connections + ",Internet is unavailable.";
            }
            catch (UnknownHostException e){}
            catch (IOException e){}
        }
        else
            connections = connections + ",Internet is unavailable.";*/
       if(isOnline())
           connections = connections + ",Internet is available.";
       else
           connections = connections + ",Internet is unavailable.";


        if(manager.isActiveNetworkMetered())
            connections = connections + ",Network is metered.";
        else
            connections = connections + ",Network is not metered.";

        return connections;
    }

    private void showSAXParsedData(){
        final SAXHandler saxHandler = new SAXHandler();
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("SAX Parsed Data")
                        .setMessage(saxHandler.mData)
                        .create().show();
            }
        };
        class PasreData extends AsyncTaskLoader<Void>{

            public PasreData(Context context){
                super(context);
            }

            @Override
            public Void loadInBackground() {
                try {
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL("http://192.168.3.104/xml/sample.xml")
                                                                                .openConnection();
                    connection.connect();
                    SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
                    parser.parse(connection.getInputStream(),saxHandler);
                    handler.sendEmptyMessage(0);
                    connection.disconnect();
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
                catch (SAXException e){}
                catch (ParserConfigurationException e){}
                return null;
            }

        }
        new PasreData(MainActivity.this).forceLoad();
    }

    private void showDOMParsedData(){
        mDomData = "";
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("DOM Parsed Data")
                        .setMessage(mDomData)
                        .create().show();
            }
        };
        class ParseData extends AsyncTaskLoader<Void>{

            ParseData(Context context){
                super(context);
            }
            @Override
            public Void loadInBackground() {
                try{
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL("http://192.168.3.104/xml/sample.xml")
                                                                                .openConnection();
                    connection.connect();
                    Document mainDoc = DocumentBuilderFactory.newInstance()
                            .newDocumentBuilder()
                            .parse(connection.getInputStream());
                    mainDoc.normalize();
                    NodeList studentData = mainDoc.getElementsByTagName("student");
                    for (int i = 0; i < studentData.getLength(); i++){
                        if(studentData.item(i).getNodeType() == Node.ELEMENT_NODE){
                            Element nodeElement = (Element) studentData.item(i);
                            mDomData = mDomData +
                                    "\nRoll no : " +
                                    nodeElement.getAttribute("rollno") +
                                    "\nName : " +
                                    nodeElement.getElementsByTagName("name").item(0).getTextContent() +
                                    "\nEmail : " +
                                    nodeElement.getElementsByTagName("email").item(0).getTextContent();
                        }
                    }
                    handler.sendEmptyMessage(0);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
                catch (ParserConfigurationException e){}
                catch (SAXException e){}
                return null;
            }
        }
        new ParseData(MainActivity.this).forceLoad();
    }

    private void showPullParsedData(){
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Pull Parsed Data")
                        .setMessage(msg.getData().getString("data"))
                        .create().show();
            }
        };
        class ParseData extends AsyncTaskLoader<Void>{

            public ParseData(Context context){
                super(context);
            }

            @Override
            public Void loadInBackground() {
                try{
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL("http://192.168.3.104/xml/sample.xml")
                                                                                .openConnection();
                    connection.connect();
                    String tempData = new XmlPullParserHandler().parse(connection.getInputStream());
                    Message message = new Message();
                    Bundle bundle = new Bundle();
                    bundle.putString("data",tempData);
                    message.setData(bundle);
                    handler.sendMessage(message);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
                catch (XmlPullParserException e){}
                return null;
            }
        }
        new ParseData(this).forceLoad();
    }

    private void showJSONDataParsed(){
        mJsonData = "";
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Some Android books on Google")
                        .setMessage(mJsonData)
                        .create().show();
            }
        };
        class ParseJson extends AsyncTaskLoader<Void>{

            public ParseJson(Context context){
                super(context);
            }

            @Override
            public Void loadInBackground() {
                try{
                    String urlString =
                            "https://www.googleapis.com/books/v1/volumes?q=android&maxResults=8";
                    HttpURLConnection connection =
                            (HttpURLConnection) new URL(urlString)
                                    .openConnection();
                    connection.connect();
                    JSONObject rootObject = new JSONObject(getJSONString(connection.getInputStream()));
                    JSONArray itemsArray = rootObject.getJSONArray("items");
                    for(int i = 0; i < itemsArray.length(); i++){
                        JSONObject temp = itemsArray.getJSONObject(i);
                        mJsonData = mJsonData +
                                "\n- Book name : " +
                                temp.getJSONObject("volumeInfo").getString("title") +
                                "\n\t\tPublishers : " +
                                temp.getJSONObject("volumeInfo").getString("publisher") +
                                "\n\t\tPages : " +
                                temp.getJSONObject("volumeInfo").getInt("pageCount");
                    }
                    handler.sendEmptyMessage(0);
                }
                catch (MalformedURLException e){}
                catch (IOException e){}
                catch (JSONException e){}
                return null;
            }

            private String getJSONString(InputStream is) throws IOException{
                BufferedReader reader =
                        new BufferedReader(new InputStreamReader(is));
                StringBuilder sb = new StringBuilder();
                String temp;
                while ((temp = reader.readLine()) != null){
                    sb.append(temp + "\n");
                }
                return sb.toString();
            }

        }
        new ParseJson(this).forceLoad();
    }
    public boolean isOnline() {
        Runtime runtime = Runtime.getRuntime();
        try {
            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);
        }
        catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }
    public boolean isOnline2() {
        try {
            int timeoutMs = 1500;
            Socket sock = new Socket();
            SocketAddress sockaddr = new InetSocketAddress("8.8.8.8", 53);

            sock.connect(sockaddr, timeoutMs);
            sock.close();

            return true;
        } catch (IOException e) { return false; }
    }
}

package com.volansys.trainingassignments.networking;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * SAXHandler is used to parse XML data using SAX approach.
 * @author Hardik Trivedi
 * @since 06-02-2018
 * @version 1.0
 */

public class SAXHandler extends DefaultHandler {

    private boolean mName = false;
    private boolean mEmail = false;
    public String mData = "";
    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (qName.equalsIgnoreCase("student")){
            mData = mData + "\nRoll No.: " + attributes.getValue("rollno");
        }
        else if(qName.equalsIgnoreCase("name"))
            mName = true;
        else if (qName.equalsIgnoreCase("email"))
            mEmail = true;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if(mName){
            mData = mData + "\nName : " + new String(ch,start,length);
            mName = false;
        }
        else if (mEmail){
            mData = mData + "\nEmail : " + new String(ch,start,length);
            mEmail = false;
        }
    }
}

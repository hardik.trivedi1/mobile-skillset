package com.volansys.trainingassignments.networking;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;

/**
 * XmlPullParser class is used to parse XML data using Pull Parsing method.
 *
 * @author Hardik Trivedi
 * @since 07-02-2018
 * @version 1.0
 */

public class XmlPullParserHandler {

    private String mData = "";

    public String getData(){
        return mData;
    }

    public String parse(InputStream is) throws XmlPullParserException,IOException{
        XmlPullParser parser = XmlPullParserFactory.newInstance().newPullParser();
        parser.setInput(is,null);

        int eventType = parser.getEventType();
        String tempText = new String();
        while(eventType != XmlPullParser.END_DOCUMENT){
            String tageName = parser.getName();
            switch (eventType){
                case XmlPullParser.START_TAG:
                    if(tageName.equalsIgnoreCase("student"))
                        mData = mData + "\nRoll no.: " + parser.getAttributeValue(null,"rollno");
                    break;

                case XmlPullParser.TEXT:
                    tempText = parser.getText();
                    break;

                case XmlPullParser.END_TAG:
                    if(tageName.equalsIgnoreCase("name"))
                        mData = mData + "\nName : " + tempText;
                    else if (tageName.equalsIgnoreCase("email"))
                        mData = mData + "\nEmail : " + tempText;
                    break;

            }
            eventType = parser.next();
        }
        return mData;
    }
}

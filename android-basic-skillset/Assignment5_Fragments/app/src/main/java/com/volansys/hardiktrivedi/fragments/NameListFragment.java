package com.volansys.hardiktrivedi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Created by hardik on 23/1/18.
 */

public class NameListFragment extends Fragment {

    private ListView mNameListView;
    private ArrayAdapter<String> mAdapter;

    private String data[] = new String[]{"One","Two","Three","Four","Five","Six","Seven","Eight"};
    private OnListItemClickListener mListener;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        try{
            mListener = (OnListItemClickListener)context;
        }catch (ClassCastException exp){}
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_list,container,false);
        mNameListView = fragmentView.findViewById(R.id.listView);

        mAdapter = new ArrayAdapter<String>(getContext(),
                                            R.layout.support_simple_spinner_dropdown_item,
                                            data);
        return  fragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedStates){
        super.onActivityCreated(savedStates);

        mNameListView.setAdapter(mAdapter);

        mNameListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                mListener.listItemClick(data[i]);
            }
        });
    }

    public interface OnListItemClickListener{
        void listItemClick(String tempData);
    }

    public String[] getData(){
        return data;
    }

    public ListView getmNameListView(){
        return mNameListView;
    }
}

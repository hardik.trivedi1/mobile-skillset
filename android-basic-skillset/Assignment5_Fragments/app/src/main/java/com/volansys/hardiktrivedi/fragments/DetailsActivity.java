package com.volansys.hardiktrivedi.fragments;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class DetailsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        DetailsFragment newDetails = new DetailsFragment();
        newDetails.setData(getIntent().getStringExtra("data"));
        getSupportFragmentManager().beginTransaction()
                            .add(R.id.details_fragment_container,newDetails)
                            .commit();

        if (getResources().getConfiguration().orientation
                == Configuration.ORIENTATION_LANDSCAPE) {
            finish();
        }
    }
}

package com.volansys.hardiktrivedi.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NameListFragment.OnListItemClickListener{

    private boolean mDualPane;

    private TextView mShowYourDataInBoldTextView;
    private Button mHighlightTextInListButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View v = findViewById(R.id.details_fragment_container);
        mDualPane = v != null && v.getVisibility() == View.VISIBLE;


        if(mDualPane) {
            DetailsFragment detailsFragment = new DetailsFragment();
            detailsFragment.setData("One");
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.details_fragment_container, detailsFragment)
                    .addToBackStack(null)
                    .commit();
        }

        mShowYourDataInBoldTextView = findViewById(R.id.textView2);
        if(mDualPane) {
            mHighlightTextInListButton = findViewById(R.id.button2);
            mHighlightTextInListButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    NameListFragment temp = (NameListFragment) getSupportFragmentManager()
                            .findFragmentById(R.id.list_fragment);
                    int position = 0;
                    for (int i = 0; i < temp.getData().length; i++) {
                        if (temp.getData()[i]
                                .equals(mShowYourDataInBoldTextView.getText().toString())) {
                            position = i;
                            break;
                        }
                    }
                    TextView tempTextView = (TextView)temp.getmNameListView().getChildAt(position);

                    tempTextView.setTextColor(Color.GREEN);
                }
            });
        }
    }

    @Override
    public void listItemClick(String tempData) {
        if(mDualPane) {
            DetailsFragment fragment = new DetailsFragment();
            fragment.setData(tempData);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.details_fragment_container, fragment)
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                    .addToBackStack(null)
                    .commit();

        }
        else{
            startActivity(new Intent(MainActivity.this,DetailsActivity.class)
                                .putExtra("data",tempData));
        }
    }

    @Override
    public void onBackPressed(){
        if(mDualPane && getSupportFragmentManager().getBackStackEntryCount() != 0)
            getSupportFragmentManager().popBackStack();
        else
            super.onBackPressed();
    }
}

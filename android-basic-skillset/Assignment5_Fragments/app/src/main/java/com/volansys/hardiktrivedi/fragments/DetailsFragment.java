package com.volansys.hardiktrivedi.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by hardik on 23/1/18.
 */

public class DetailsFragment extends Fragment {

    private static final String TAG = "DetailsFragment";

    private TextView mShowDataTextView;
    private String data;

    private Button mShowDataHereButton;

    @Override
    public void onAttach(Context context){
        super.onAttach(context);
        Log.v(TAG,"from onAttach()");
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.fragment_details,container,false);
        mShowDataTextView = fragmentView.findViewById(R.id.textView);
        mShowDataTextView.setText(data);
        mShowDataHereButton = fragmentView.findViewById(R.id.button);
        Log.v(TAG,"from onCreateView()");
        return  fragmentView;
    }

    public void setData(String tempData){
        data = tempData;
    }

    @Override
    public void onActivityCreated(Bundle savedStates){
        super.onActivityCreated(savedStates);
        mShowDataHereButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tempView = getActivity().findViewById(R.id.textView2);
                tempView.setText(data);
            }
        });
        Log.v(TAG,"from onActivityCreated()");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.v(TAG,"from onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.v(TAG,"from onResume()");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.v(TAG,"from onStop()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.v(TAG,"from onDestroyView()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.v(TAG,"from onDestroy()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.v(TAG,"from onDetach()");
    }
}

package com.volansys.hardiktrivedi.uicomponents;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class GridViewExampleActivity extends AppCompatActivity {

    private Button mShowSimpleGridViewButton;
    private Button mShowCustomGridViewButton;
    private GridView mGridView;
    private ArrayAdapter<String> mSimpleGridViewAdapter;
    private CustomViewAdapter mCustomGridViewAdapter;
    private ArrayList<String> mGridData;
    private CustomItem[] customItems=new CustomItem[27];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grid_view_example);

        mShowSimpleGridViewButton = findViewById(R.id.simple_gridview_button);
        mShowCustomGridViewButton = findViewById(R.id.custom_gridview_button);
        mGridView = findViewById(R.id.gridView);

        mGridView.setNumColumns(3);

        mShowSimpleGridViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mGridData = new ArrayList<>();
                for (int i=0; i < 27; i++)
                    mGridData.add("Item no. " + (i+1));
                mSimpleGridViewAdapter =
                        new ArrayAdapter<String>(GridViewExampleActivity.this,
                                                    R.layout.support_simple_spinner_dropdown_item,
                                                    mGridData);
                mGridView.setAdapter(mSimpleGridViewAdapter);
            }
        });

        mShowCustomGridViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0; i < 27; i++)
                    customItems[i] = new CustomItem("Item no. " + (i+1),
                            BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
                mCustomGridViewAdapter = new CustomViewAdapter(GridViewExampleActivity.this,
                                                                customItems);
                mGridView.setAdapter(mCustomGridViewAdapter);
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(mGridView.getAdapter() instanceof CustomViewAdapter)
                    Toast.makeText(GridViewExampleActivity.this,
                            "You have clicked custom view " + customItems[i].getLabelText(),
                                 Toast.LENGTH_SHORT).show();
                else if(mGridView.getAdapter() instanceof ArrayAdapter)
                    Toast.makeText(GridViewExampleActivity.this,
                            "You have clicked simple view " + mGridData.get(i),
                                 Toast.LENGTH_SHORT).show();
            }
        });
    }
}

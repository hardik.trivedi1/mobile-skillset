package com.volansys.hardiktrivedi.uicomponents;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class RecyclerViewExampleActivity extends AppCompatActivity {

    private Button mShowSimpleRecyclerViewButton;
    private Button mShowCustomRecyclerViewButton;
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mSimpleAdapter,mCustomAdapter;
    private CustomItem[] customItems = new CustomItem[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_example);

        mShowSimpleRecyclerViewButton = findViewById(R.id.simple_recyclerview_button);
        mShowCustomRecyclerViewButton = findViewById(R.id.custom_recyclerview_button);
        mRecyclerView = findViewById(R.id.recycler_view);

        mShowSimpleRecyclerViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < 10; i++)
                    customItems[i] = new CustomItem("Item no." + (i+1),
                            BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
                mSimpleAdapter =
                        new RecyclerViewAdapter(RecyclerViewExampleActivity.this,
                                                                customItems);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                if(mRecyclerView.getAdapter() == null)
                    mRecyclerView.setAdapter(mSimpleAdapter);
                else
                    mRecyclerView.swapAdapter(mSimpleAdapter,false);
            }
        });

        mShowCustomRecyclerViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < 10; i++)
                    customItems[i] = new CustomItem("Thing no." + (i+1),
                            BitmapFactory.decodeResource(getResources(),R.raw.head12));
                mCustomAdapter =
                        new RecyclerViewAdapter(RecyclerViewExampleActivity.this,
                                customItems);
                mRecyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
                mRecyclerView.setAdapter(mCustomAdapter);
                if(mRecyclerView.getAdapter() == null)
                    mRecyclerView.setAdapter(mCustomAdapter);
                else
                    mRecyclerView.swapAdapter(mCustomAdapter,false);
            }
        });

        mRecyclerView.setOnFlingListener(new RecyclerView.OnFlingListener() {
            @Override
            public boolean onFling(int velocityX, int velocityY) {
                Toast.makeText(RecyclerViewExampleActivity.this,
                        "You moved at " + velocityY + " speed.", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
    }
}

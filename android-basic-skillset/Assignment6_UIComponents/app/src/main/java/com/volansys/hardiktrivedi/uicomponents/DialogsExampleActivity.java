package com.volansys.hardiktrivedi.uicomponents;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

public class DialogsExampleActivity extends AppCompatActivity
        implements OnDialogButtonClickListener {

    private Button mShowSimpleDialogUsingDialogFragmentButton;
    private Button mShowSimpleDialogUsingAlertDialogButton;
    private Button mShowSingleChoiceDialogButton;
    private Button mShowMultiChoiceDialogButton;
    private Button mShowCustomDialogButton;
    private Button mShowDatePickerDialogButton;
    private Button mShowTimePickerDialogButton;

    private Chronometer mTimer;
    private Button mStartTimerButton,mStopTimerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialogs_example);

        mShowSimpleDialogUsingDialogFragmentButton =
                                                findViewById(R.id.simple_dialog_fragment_button);
        mShowSimpleDialogUsingAlertDialogButton = findViewById(R.id.simple_dialog_alert_button);
        mShowSingleChoiceDialogButton = findViewById(R.id.single_choice_dialog_button);
        mShowMultiChoiceDialogButton = findViewById(R.id.multi_choice_dialog_button);
        mShowCustomDialogButton = findViewById(R.id.custom_dialog_button);
        mShowDatePickerDialogButton = findViewById(R.id.date_picker_dialog_button);
        mShowTimePickerDialogButton = findViewById(R.id.time_picker_dialog_button);

        mTimer = findViewById(R.id.chronometer);
        mStartTimerButton = findViewById(R.id.button_start_chrono);
        mStopTimerButton = findViewById(R.id.button_stop_chrono);

        mShowSimpleDialogUsingDialogFragmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new WhatDoYouWishDialog().show(getSupportFragmentManager(),
                        "WhatDoYouWishDialogFragment");
            }
        });

        mShowSimpleDialogUsingAlertDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder dialogBuilder =
                        new AlertDialog.Builder(DialogsExampleActivity.this);
                dialogBuilder.setTitle("What do you wish ?")
                        .setIcon(R.mipmap.ic_launcher)
                        .setPositiveButton("Move on", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(DialogsExampleActivity.this,
                                        "You have wished to move on.",
                                             Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("Stay here", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(DialogsExampleActivity.this,
                                        "You have wished to be here",
                                             Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setCancelable(false)
                        .create()
                        .show();
            }
        });

        mShowSingleChoiceDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String dialogItems[] = new String[]{"Walk","Dance","Eat","Sleep"};
                AlertDialog.Builder dialogBuilder =
                        new AlertDialog.Builder(DialogsExampleActivity.this);
                dialogBuilder.setIcon(R.mipmap.ic_launcher)
                        .setTitle("Choose any action ")
                        .setItems(dialogItems,
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        Toast.makeText(DialogsExampleActivity.this,
                                                "You wish to " + dialogItems[i],
                                                Toast.LENGTH_SHORT).show();
                                    }
                        })
                        .create()
                        .show();
            }
        });

        mShowMultiChoiceDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String dialogItems[] = new String[]{"Walk","Dance","Eat","Sleep"};
                final ArrayList<String> selectedData = new ArrayList<>();
                AlertDialog.Builder dialogBuilder =
                        new AlertDialog.Builder(DialogsExampleActivity.this);
                dialogBuilder.setIcon(R.mipmap.ic_launcher)
                        .setTitle("Choose any action ")
                        .setMultiChoiceItems(dialogItems,
                                null,
                                new DialogInterface.OnMultiChoiceClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface,
                                                                                int i, boolean b) {
                                    if(b)
                                        selectedData.add(dialogItems[i]);
                                    else if(selectedData.contains(dialogItems[i]))
                                        selectedData.remove(i);

                                }
                        })
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String tempData = "";
                                for (String temp : selectedData) {
                                    if(tempData != "")
                                        tempData = tempData + "," + temp;
                                    else
                                        tempData = tempData + temp;
                                }
                                Toast.makeText(DialogsExampleActivity.this,
                                        "You wish to " + tempData, Toast.LENGTH_SHORT).show();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .create()
                        .show();
            }
        });

        mShowCustomDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final View customView = getLayoutInflater().inflate(R.layout.custom_view_dialog,
                        null,false);
                AlertDialog.Builder dialogBuilder =
                        new AlertDialog.Builder(DialogsExampleActivity.this);
                dialogBuilder.setTitle("Give your name")
                        .setIcon(R.mipmap.ic_launcher)
                        .setView(customView)
                        .setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                EditText tempView = customView.findViewById(R.id.editText);
                                Toast.makeText(DialogsExampleActivity.this,
                                        "Hello " + tempView.getText(),
                                        Toast.LENGTH_SHORT).show();
                            }
                        })
                        .create()
                        .show();
            }
        });

        mShowDatePickerDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar tempDate = Calendar.getInstance();
                new DatePickerDialog(DialogsExampleActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                                Toast.makeText(DialogsExampleActivity.this,
                                        "Selected date is " +
                                             datePicker.getDayOfMonth() + "/" +
                                             (datePicker.getMonth()+1) + "/" +
                                             datePicker.getYear(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        ,tempDate.get(Calendar.YEAR)
                        ,tempDate.get(Calendar.MONTH)
                        ,tempDate.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

        mShowTimePickerDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar tempDate = Calendar.getInstance();
                new TimePickerDialog(DialogsExampleActivity.this,
                        new TimePickerDialog.OnTimeSetListener() {
                            @RequiresApi(api = Build.VERSION_CODES.M)
                            @Override
                            public void onTimeSet(TimePicker timePicker, int i, int i1) {
                                Toast.makeText(DialogsExampleActivity.this,
                                        "Selected time is " +
                                                timePicker.getHour() + ":" +
                                                timePicker.getMinute(),Toast.LENGTH_SHORT).show();
                            }
                        }
                        ,tempDate.get(Calendar.HOUR)
                        ,tempDate.get(Calendar.MINUTE)
                        ,true)
                        .show();
            }
        });

        mStartTimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimer.start();
            }
        });
        mStopTimerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mTimer.stop();
            }
        });
        mTimer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                if(Integer.parseInt(chronometer.getText().toString().split(":")[1])%5 == 0)
                    chronometer.setTextColor(Color.RED);
                else
                    chronometer.setTextColor(Color.BLACK);
            }
        });
    }

    public static class WhatDoYouWishDialog extends DialogFragment {

        private OnDialogButtonClickListener mPositiveButtonClickListener;
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            mPositiveButtonClickListener = (OnDialogButtonClickListener) context;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("What do you wish ?")
                    .setIcon(R.mipmap.ic_launcher)
                    .setPositiveButton("Move on", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Toast.makeText(getContext(),
                                    "You have wished to move on.", Toast.LENGTH_SHORT).show();
                            mPositiveButtonClickListener.onPositiveButtonClick();
                        }
                    })
                    .setNegativeButton("Stay here", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            Toast.makeText(getContext(),
                                    "You have wished to stay here.", Toast.LENGTH_SHORT).show();
                            mPositiveButtonClickListener.onNegativeButtonClick();
                        }
                    });
            return builder.create();
        }
    }

    @Override
    public void onPositiveButtonClick() {
        mShowSimpleDialogUsingDialogFragmentButton.setTextColor(Color.GREEN);
    }

    @Override
    public void onNegativeButtonClick(){
        mShowSimpleDialogUsingDialogFragmentButton.setTextColor(Color.BLACK);
    }
}
interface OnDialogButtonClickListener {
    void onPositiveButtonClick();
    void onNegativeButtonClick();
}



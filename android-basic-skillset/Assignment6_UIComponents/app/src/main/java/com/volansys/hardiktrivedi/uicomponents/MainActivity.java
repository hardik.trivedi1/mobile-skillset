package com.volansys.hardiktrivedi.uicomponents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private Button mStudentFormButton;
    private Button mShowListViewExampleButton;
    private Button mShowGridViewExampleButton;
    private Button mShowRecyclerViewExampleButton;
    private Button mShowDialogsExampleButton;
    private Button mShowExpandableListExampleButton;
    private Button mMultiViewExampleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mStudentFormButton = findViewById(R.id.student_form_button);
        mShowListViewExampleButton = findViewById(R.id.listview_example_button);
        mShowGridViewExampleButton = findViewById(R.id.gridview_example_button);
        mShowRecyclerViewExampleButton = findViewById(R.id.recyclerview_example_button);
        mShowDialogsExampleButton = findViewById(R.id.dialog_example_button);
        mShowExpandableListExampleButton = findViewById(R.id.expandable_list_example_button);
        mMultiViewExampleButton = findViewById(R.id.button_multi_view);

        mStudentFormButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                                                                StudentsFormActivity.class));
            }
        });

        mShowListViewExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                                                                ListViewExampleActivity.class));
            }
        });

        mShowGridViewExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                                                                GridViewExampleActivity.class));
            }
        });

        mShowRecyclerViewExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                                                                RecyclerViewExampleActivity.class));
            }
        });

        mShowDialogsExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                                                                DialogsExampleActivity.class));
            }
        });

        mShowExpandableListExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,
                                                                ExpandableListActivity.class));
            }
        });

        mMultiViewExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this,MultiViewExampleActivity.class));
            }
        });
    }
}

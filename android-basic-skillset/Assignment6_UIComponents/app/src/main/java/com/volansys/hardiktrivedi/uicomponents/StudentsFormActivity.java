package com.volansys.hardiktrivedi.uicomponents;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class StudentsFormActivity extends AppCompatActivity {

    private EditText mNameEditText,mPhoneNumberEditText,mAddressEditText;
    private RadioGroup mGenderRadioGroup;
    private Spinner mYearSpinner;
    private CheckBox mMathsCheckBox,mScienceCheckBox,mEnglishCheckBox;
    private SeekBar mSurpriseSubjectsSeekBar;
    private RatingBar mLastYearRatingBar;
    private Button mSubmitButton;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_form);

        mNameEditText = findViewById(R.id.name_editText);
        mPhoneNumberEditText = findViewById(R.id.number_editText);
        mAddressEditText = findViewById(R.id.address_editText);
        mGenderRadioGroup = findViewById(R.id.radio_group);
        mYearSpinner = findViewById(R.id.year_spinner);
        mMathsCheckBox = findViewById(R.id.maths_checkBox);
        mScienceCheckBox = findViewById(R.id.science_checkBox);
        mEnglishCheckBox = findViewById(R.id.english_checkbox);
        mSurpriseSubjectsSeekBar = findViewById(R.id.seekBar);
        mLastYearRatingBar = findViewById(R.id.ratingBar);
        mSubmitButton = findViewById(R.id.submit_button);




        ArrayList<String> spinnerData = new ArrayList<>();
        spinnerData.add("First");
        spinnerData.add("Second");
        spinnerData.add("Third");
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<String>(this,
                R.layout.support_simple_spinner_dropdown_item,spinnerData);
        mYearSpinner.setAdapter(spinnerAdapter);

        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent showDataIntent = new Intent(StudentsFormActivity.this,
                                                                        StudentsDataActivity.class);
                Bundle studentDataBundle = new Bundle();
                studentDataBundle.putString("name",mNameEditText.getText().toString());
                studentDataBundle.putString("number",mPhoneNumberEditText.getText().toString());
                studentDataBundle.putString("address",mAddressEditText.getText().toString());
                TextView gender = findViewById(mGenderRadioGroup.getCheckedRadioButtonId());
                if(gender != null)
                    studentDataBundle.putString("gender",gender.getText().toString());
                studentDataBundle.putString("maths",
                            mMathsCheckBox.isChecked()?mMathsCheckBox.getText().toString():"");
                studentDataBundle.putString("science",
                            mScienceCheckBox.isChecked()?mScienceCheckBox.getText().toString():"");
                studentDataBundle.putString("english",
                            mEnglishCheckBox.isChecked()?mEnglishCheckBox.getText().toString():"");
                studentDataBundle.putString("surprise",
                            String.valueOf(mSurpriseSubjectsSeekBar.getProgress()));
                studentDataBundle.putString("rating",
                            String.valueOf(mLastYearRatingBar.getRating()));
                studentDataBundle.putString("year",(String)mYearSpinner.getSelectedItem());

                showDataIntent.putExtra("studentDataBundle",studentDataBundle);
                startActivity(showDataIntent);
            }
        });
    }
}

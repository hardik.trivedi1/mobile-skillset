package com.volansys.hardiktrivedi.uicomponents;

import android.graphics.Bitmap;

/**
 * Created by hardik on 24/1/18.
 */

public class CustomItem {
    private String mLabelText;
    private Bitmap mImageBitmap;

    CustomItem(String label,Bitmap image){
        mLabelText = label;
        mImageBitmap = image;
    }

    public void setLabelText(String text){
        mLabelText = text;
    }
    public String getLabelText(){
        return mLabelText;
    }

    public Bitmap getImageBitmap(){
        return mImageBitmap;
    }
    public void setImageBitmap(Bitmap image){
        mImageBitmap = image;
    }
}

package com.volansys.hardiktrivedi.uicomponents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class StudentsDataActivity extends AppCompatActivity {

    private TextView mShowDataTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_students_data);

        mShowDataTextView = findViewById(R.id.students_data_text_view);

        if(getIntent().getBundleExtra("studentDataBundle") != null){
            Bundle studentDataBundle = getIntent().getBundleExtra("studentDataBundle");
            String data = "Name : " + studentDataBundle.getString("name") +
                    "\nPhone : " + studentDataBundle.getString("number") +
                    "\nAddress : " + studentDataBundle.getString("address") +
                    "\nGender : " + studentDataBundle.getString("gender") +
                    "\nSubjects : " + studentDataBundle.getString("maths")  +
                                      studentDataBundle.getString("science") +
                                      studentDataBundle.getString("english")  +
                    "\nNumber of surprise subjects : " + studentDataBundle.getString("surprise")+
                    "\nYear : " + studentDataBundle.getString("year") +
                    "\nLast year Rating : " + studentDataBundle.getString("rating") + " stars";

            mShowDataTextView.setText(data);
        }
    }
}

package com.volansys.hardiktrivedi.uicomponents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;

public class CardViewExampleActivity extends AppCompatActivity {

    private ArrayList<String> personNames = new ArrayList<>(Arrays.asList("Hardik","Dipu","Dolly","Pooh","JD"));
    private ArrayList<Integer> personImages = new ArrayList<>(Arrays.asList(R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher,
            R.mipmap.ic_launcher));
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_view_example);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView_card);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        CardViewCustomAdapter customAdapter =
                new CardViewCustomAdapter(CardViewExampleActivity.this, personNames,personImages);
        recyclerView.setAdapter(customAdapter);
    }
}

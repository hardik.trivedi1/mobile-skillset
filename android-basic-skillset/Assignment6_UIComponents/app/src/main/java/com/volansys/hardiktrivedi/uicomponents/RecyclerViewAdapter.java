package com.volansys.hardiktrivedi.uicomponents;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by hardik on 25/1/18.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder> {

    private Activity context;
    private CustomItem[] customItems;

    public RecyclerViewAdapter(Activity context, CustomItem[] customItems){
        this.context = context;
        this.customItems = customItems;
    }
    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(context)
                                      .inflate(R.layout.custom_item,parent,false);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TextView tempView = itemView.findViewById(R.id.custom_item_textview);
                Toast.makeText(context, "You have clicked " + tempView.getText() ,
                                            Toast.LENGTH_SHORT).show();
            }
        });
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder holder, final int position) {
        holder.mLabelTextView.setText(customItems[position].getLabelText());
        holder.mImageView.setImageBitmap(customItems[position].getImageBitmap());

        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,
                        "You have clicked on Image of " + customItems[position].getLabelText(),
                             Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return customItems.length;
    }
}

class RecyclerViewHolder extends RecyclerView.ViewHolder{

    public TextView mLabelTextView;
    public ImageView mImageView;

    public RecyclerViewHolder(View itemView) {
        super(itemView);
        mLabelTextView = itemView.findViewById(R.id.custom_item_textview);
        mImageView = itemView.findViewById(R.id.custom_item_imageView);
    }


}
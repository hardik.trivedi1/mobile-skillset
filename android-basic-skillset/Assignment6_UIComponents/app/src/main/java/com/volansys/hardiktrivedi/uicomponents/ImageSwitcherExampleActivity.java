package com.volansys.hardiktrivedi.uicomponents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ViewSwitcher;

public class ImageSwitcherExampleActivity extends AppCompatActivity {

    private ImageSwitcher mImageSwitcher;
    private Button mPreviousButton,mNextButton;
    private int mImageIds[] = new int[]{R.raw.image1,
                                        R.raw.image2,
                                        R.raw.image3,
                                        R.raw.image4,
                                        R.raw.image5,
                                        R.raw.image6,
                                        R.raw.image7,
                                        R.raw.image8};
    private int mIndex = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_switcher_example);

        mImageSwitcher = findViewById(R.id.imageSwitcher);
        mImageSwitcher.setFactory(new ViewSwitcher.ViewFactory() {

            public View makeView() {
                ImageView imageView = new ImageView(ImageSwitcherExampleActivity.this);
                imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                imageView.setLayoutParams(new ImageSwitcher.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
                return imageView;
            }
        });
        mImageSwitcher.setInAnimation(AnimationUtils.loadAnimation(this,android.R.anim.fade_in));
        mImageSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this,android.R.anim.fade_out));

        mNextButton = findViewById(R.id.button_next);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIndex++;
                if (mIndex >= mImageIds.length)
                    mIndex = 0;
                mImageSwitcher.setImageResource(mImageIds[mIndex]);
            }
        });

        mPreviousButton = findViewById(R.id.button_previous);
        mPreviousButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mIndex--;
                if (mIndex <= -1)
                    mIndex = mImageIds.length - 1;
                mImageSwitcher.setImageResource(mImageIds[mIndex]);
            }
        });
    }
}

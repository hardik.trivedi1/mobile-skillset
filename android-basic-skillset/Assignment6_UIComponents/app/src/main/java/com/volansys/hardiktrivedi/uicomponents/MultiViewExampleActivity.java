package com.volansys.hardiktrivedi.uicomponents;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MultiViewExampleActivity extends AppCompatActivity {

    private Button mShowViewPagerExampleButton;
    private Button mShowTabViewExampleButton;
    private Button mShowBottomNavigationExampleButton;
    private Button mShowCardViewExampleButton;
    private Button mShowImageSwitcherExampleButton;
    private Button mShowViewFlipperExampleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_multi_view_example);

        mShowViewPagerExampleButton = findViewById(R.id.button_view_pager);

        mShowViewPagerExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiViewExampleActivity.this,
                                                                    ViewPagerActivity.class));
            }
        });

        mShowTabViewExampleButton = findViewById(R.id.button_tab_view);

        mShowTabViewExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiViewExampleActivity.this,
                        TabViewExampleActivity.class));
            }
        });

        mShowBottomNavigationExampleButton = findViewById(R.id.button_bottom_navigation);

        mShowBottomNavigationExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiViewExampleActivity.this,
                        BottomNavigationActivity.class));
            }
        });


        mShowCardViewExampleButton = findViewById(R.id.button_card_view);

        mShowCardViewExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiViewExampleActivity.this,CardViewExampleActivity.class));
            }
        });

        mShowImageSwitcherExampleButton = findViewById(R.id.button_image_switcher);
        mShowImageSwitcherExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiViewExampleActivity.this,ImageSwitcherExampleActivity.class));
            }
        });

        mShowViewFlipperExampleButton = findViewById(R.id.button_view_flipper);
        mShowViewFlipperExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MultiViewExampleActivity.this,ViewFlipperExampleActivity.class));
            }
        });
    }
}

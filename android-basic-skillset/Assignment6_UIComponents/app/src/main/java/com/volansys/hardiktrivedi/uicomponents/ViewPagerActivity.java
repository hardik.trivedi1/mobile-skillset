package com.volansys.hardiktrivedi.uicomponents;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import java.util.ArrayList;
import java.util.HashMap;

public class ViewPagerActivity extends AppCompatActivity {

    public static ArrayList<Integer> VIEWDATA = new ArrayList<>();

    private ViewPager mPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);

        VIEWDATA.add(R.layout.red);
        VIEWDATA.add(R.layout.green);
        VIEWDATA.add(R.layout.blue);

        mPager = (ViewPager) findViewById(R.id.view_pager);
        mPager.setAdapter(new CustomPageAdapter(this));

    }

    @Override
    public void onBackPressed() {
        if (mPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            mPager.setCurrentItem(mPager.getCurrentItem() - 1);
        }
    }

}

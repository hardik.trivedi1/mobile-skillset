package com.volansys.hardiktrivedi.uicomponents;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by hardik on 24/1/18.
 */

public class CustomViewAdapter extends ArrayAdapter<CustomItem>{

    private Activity context;
    private CustomItem[] customItems;
    CustomViewAdapter(Activity context, CustomItem[] customItems){
        super(context,R.layout.custom_item,customItems);
        this.context = context;
        this.customItems = customItems;
    }
    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View rootView =
                context.getLayoutInflater().inflate(R.layout.custom_item,parent,false);

        TextView labelTextView = rootView.findViewById(R.id.custom_item_textview);
        ImageView imageView = rootView.findViewById(R.id.custom_item_imageView);

        labelTextView.setText(customItems[position].getLabelText());
        imageView.setImageBitmap(customItems[position].getImageBitmap());

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context,
                "You have clicked on Image which is in " + customItems[position].getLabelText()
                        , Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }
}

package com.volansys.hardiktrivedi.uicomponents;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

public class BottomNavigationActivity extends AppCompatActivity {

    private RedFragment mRedFragment = new RedFragment();
    private GreenFragment mGreenFragment = new GreenFragment();
    private BlueFragment mBlueFragment = new BlueFragment();

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_red:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fg_container,mRedFragment)
                            .commit();
                    return true;
                case R.id.navigation_green:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fg_container,mGreenFragment)
                            .commit();
                    return true;
                case R.id.navigation_blue:
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.fg_container,mBlueFragment)
                            .commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fg_container,mRedFragment)
                .commit();
    }

}

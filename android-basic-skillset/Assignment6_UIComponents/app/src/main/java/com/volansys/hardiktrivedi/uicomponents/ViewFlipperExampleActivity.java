package com.volansys.hardiktrivedi.uicomponents;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

public class ViewFlipperExampleActivity extends AppCompatActivity {

    private ViewFlipper mViewFlipper,mImageFlipper;
    private Button mNext,mPrevious;
    private int mImageIds[] = new int[]{R.raw.image1,
            R.raw.image2,
            R.raw.image3,
            R.raw.image4,
            R.raw.image5,
            R.raw.image6,
            R.raw.image7,
            R.raw.image8};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_flipper_example);

        mViewFlipper = findViewById(R.id.viewFlipper);
        mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left));
        mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right));

        mNext = findViewById(R.id.button_nxt);
        mNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewFlipper.showNext();
            }
        });

        mPrevious = findViewById(R.id.button_pre);
        mPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mViewFlipper.showPrevious();
            }
        });

        mImageFlipper = findViewById(R.id.viewFlipper2);
        mImageFlipper.setInAnimation(AnimationUtils.loadAnimation(this,android.R.anim.slide_in_left));
        mImageFlipper.setOutAnimation(AnimationUtils.loadAnimation(this,android.R.anim.slide_out_right));
        for (int i=0;i < mImageIds.length;i++){
            ImageView imageView = new ImageView(this);
            imageView.setImageResource(mImageIds[i]);
            mImageFlipper.addView(imageView);
        }
        mImageFlipper.setFlipInterval(1000);
        mImageFlipper.setAutoStart(true);
    }
}

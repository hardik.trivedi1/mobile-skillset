package com.volansys.hardiktrivedi.uicomponents;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListViewExampleActivity extends AppCompatActivity {

    private Button mShowCustomListViewButton;
    private Button mShowSimpleListViewButton;

    private ArrayList<String> mListData;
    private ArrayAdapter<String> mSimpleListAdapter;

    private ListView mListView;
    private CustomViewAdapter mCustomListAdapter;
    private CustomItem[] mCustomItems=new CustomItem[10];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view_example);

        mListView = findViewById(R.id.listView);
        mShowSimpleListViewButton = findViewById(R.id.simple_listview_button);
        mShowCustomListViewButton = findViewById(R.id.custom_listview_button);
        mListData = new ArrayList<>();


        mShowSimpleListViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0; i < 10; i++)
                    mListData.add("Item no." + (i+1));
                mSimpleListAdapter = new ArrayAdapter<String>(ListViewExampleActivity.this,
                        R.layout.support_simple_spinner_dropdown_item,mListData);
                mListView.setAdapter(mSimpleListAdapter);
            }
        });

        mShowCustomListViewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i=0; i < 10; i++ ){
                    mCustomItems[i] = new CustomItem("Item no.: " + (i+1),
                            BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher));
                }

                mCustomListAdapter =
                        new CustomViewAdapter(ListViewExampleActivity.this,mCustomItems);

                mListView.setAdapter(mCustomListAdapter);
            }
        });

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if(mListView.getAdapter() instanceof CustomViewAdapter)
                    Toast.makeText(ListViewExampleActivity.this,
                            "You have clicked custom item." + mCustomItems[i].getLabelText(),
                                 Toast.LENGTH_SHORT).show();
                else if(mListView.getAdapter() instanceof ArrayAdapter)
                    Toast.makeText(ListViewExampleActivity.this,
                            "You have clicked simple item." + mListData.get(i),
                                 Toast.LENGTH_SHORT).show();

            }
        });
    }
}

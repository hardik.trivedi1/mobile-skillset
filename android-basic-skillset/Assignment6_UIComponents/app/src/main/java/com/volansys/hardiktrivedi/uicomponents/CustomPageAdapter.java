package com.volansys.hardiktrivedi.uicomponents;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * For filling pages in ViewPager
 */

public class CustomPageAdapter extends PagerAdapter {

    private Context mContext;

    public CustomPageAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout =
                (ViewGroup) inflater.inflate(ViewPagerActivity.VIEWDATA.get(position), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return ViewPagerActivity.VIEWDATA.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = title + "Red";
                break;
            case 1:
                title = title + "Green";
                break;
            case 2:
                title = title + "Blue";
                break;
        }
        return title;
    }
}

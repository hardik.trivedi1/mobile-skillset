package com.volansys.trainingassignments.studentsrecordmvvm;

/**
 * Student class, facilitates POJO for student information.
 * @author Hardik Trivedi
 * @version 1.0
 * @since 17-02-2018
 */

public class Student {
    private String rollNo,name,email;

    public Student(String rollNo,String name,String email){
        this.rollNo = rollNo;
        this.name = name;
        this.email = email;
    }

    public void setRollNo(String rollNo) {
        this.rollNo = rollNo;
    }
    public String getRollNo(){
        return rollNo;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName() {
        return name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}

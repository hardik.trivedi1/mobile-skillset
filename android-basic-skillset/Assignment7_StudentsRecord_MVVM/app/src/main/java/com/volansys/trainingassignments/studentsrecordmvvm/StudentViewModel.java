package com.volansys.trainingassignments.studentsrecordmvvm;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.databinding.BindingConversion;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * StudentViewModel class, which extends BaseObservable , acts as middle man i.e. communicates with
 * View and Model.
 * @author Hardik Trivedi
 * @version 1.0
 * @since 17-02-2018
 */

public class StudentViewModel extends BaseObservable {
    private Context mContext;
    private ArrayAdapter<String> adapter;
    public ObservableField<String> mStudents = new ObservableField<>();

    public ObservableField<String> mRollNo = new ObservableField<>("");
    public ObservableField<String> mName = new ObservableField<>("");
    public ObservableField<String> mEmail = new ObservableField<>("");
    public ObservableArrayList<String> mList = new ObservableArrayList<>();

    public StudentViewModel(Context context) {
        mContext = context;
    }

    @Bindable
    public String getStudents(){
        return mStudents.toString();
    }

    public void addStudents(Student student){
        mStudents.set("\nRollno. : " + student.getRollNo() +
                    "\nName : " + student.getName() +
                    "\nEmail : " + student.getEmail());
        mList.add("\nRollno. : " + student.getRollNo() +
                "\nName : " + student.getName() +
                "\nEmail : " + student.getEmail());
        adapter.notifyDataSetChanged();
        notifyPropertyChanged(BR.students);
    }

    public View.OnClickListener onAddListener(){
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addStudents(new Student(mRollNo.get(),mName.get(),mEmail.get()));
                Log.w("StudentViewModel","Data added to List.");
            }
        };
    }

    public ArrayAdapter<String> getAdapter(){
        if(adapter == null)
            adapter = new ArrayAdapter<String>(mContext,R.layout.support_simple_spinner_dropdown_item,getList());
        return adapter;
    }
    public ArrayList<String> getList(){
        if (mList.size() == 0)
            return mList;
        else
            return (ArrayList<String>) mList.subList(0,mList.size()-1);
    }

    @BindingAdapter("android:backgroundColor")
    public static void setColour(TextView view,String colour){
        view.setBackgroundColor(Color.DKGRAY);
    }

    @BindingConversion
    public static ColorDrawable convertColorFromIntToDrawable(int color){
        return new ColorDrawable(color);
    }
}

package com.volansys.trainingassignments.studentsrecordmvvm;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.volansys.trainingassignments.studentsrecordmvvm.databinding.ListLayoutBinding;
import com.volansys.trainingassignments.studentsrecordmvvm.databinding.MainActivityBinding;

/**
 * MainActivity class, holds as View in MVVM.
 * @author Hardik Trivedi
 * @version 1.0
 * @since 17-02-2018
 *
 */

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.main_activity);

        MainActivityBinding binding =
                DataBindingUtil.setContentView(this,R.layout.main_activity);

        StudentViewModel viewModel = new StudentViewModel(this);

        binding.setStudents(viewModel);


        ListLayoutBinding layoutBinding = DataBindingUtil.inflate(getLayoutInflater(),R.layout.list_layout,(ViewGroup) findViewById(R.id.list_container),true);
        layoutBinding.listview.setAdapter(viewModel.getAdapter());

        ViewStub stub = findViewById(R.id.viewStub);

        stub.inflate();
    }
}

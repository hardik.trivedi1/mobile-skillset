package com.volansys.hardiktrivedi.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String KEY_NAME = "name";

    private EditText mTakeNameEditText;
    private Button mGotoNextActivityButton;
    private Button mSendTextButton;
    private Button mSerializableExampleButton,mParcelableExampleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.v(TAG,"from onCreate()");

        mTakeNameEditText = findViewById(R.id.editText);
        mGotoNextActivityButton = findViewById(R.id.button);
        mSendTextButton = findViewById(R.id.send_text_button);

        mGotoNextActivityButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent gotoNextActivity = new Intent(MainActivity.this,
                                                                    SecondActivity.class);
                gotoNextActivity.putExtra(KEY_NAME,mTakeNameEditText.getText().toString());
                startActivity(gotoNextActivity);
            }
        });

        mSendTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendText = new Intent();
                sendText.setAction(Intent.ACTION_SEND);
                sendText.putExtra(Intent.EXTRA_TEXT, "Hello there !!");
                sendText.setType("text/plain");
                if (sendText.resolveActivity(getPackageManager()) != null) {
                    startActivity(sendText);
                }
            }
        });

        mSerializableExampleButton = findViewById(R.id.button_serializable);
        mSerializableExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Person onePerson = new Person();
                onePerson.setAge(21);
                onePerson.setName("Hardik");
                Intent intent = new Intent(MainActivity.this, SampleActivity.class);
                intent.putExtra("serial_data",onePerson);
                startActivity(intent);
            }
        });

        mParcelableExampleButton = findViewById(R.id.button_parcelable);
        mParcelableExampleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Book oneBook = new Book();
                oneBook.setAuthor("Krishna Yadav");
                oneBook.setBookName("Bhagvad Gita");
                oneBook.setPublishTime(5000);
                Intent intent = new Intent(MainActivity.this,SampleActivity.class);
                intent.putExtra("parcel_data",oneBook);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.v(TAG,"from onStart()");
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.v(TAG,"from onResume()");
    }

    @Override
    protected void onPause(){
        super.onPause();
        Log.v(TAG,"from onPause()");
    }

    @Override
    protected void onStop(){
        super.onStop();
        Log.v(TAG,"from onStop()");
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        Log.v(TAG,"from onDestroy()");
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        if(savedInstanceState != null)
            mTakeNameEditText.setText(savedInstanceState.getString(MainActivity.KEY_NAME));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(MainActivity.KEY_NAME,mTakeNameEditText.getText().toString());

        super.onSaveInstanceState(outState);
    }
}

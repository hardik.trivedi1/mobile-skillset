package com.volansys.hardiktrivedi.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SecondActivity extends AppCompatActivity {

    private static final String TAG="SecondActivity";

    private TextView mShowNameTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        mShowNameTextView = findViewById(R.id.textView2);

        mShowNameTextView.setText("Welcome " + getIntent().getStringExtra(MainActivity.KEY_NAME));
    }
}

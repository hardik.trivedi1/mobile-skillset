package com.volansys.hardiktrivedi.activities;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);

        if(getIntent().getSerializableExtra("serial_data") != null){
            Person samePerson = (Person) getIntent().getSerializableExtra("serial_data");
            new AlertDialog.Builder(this)
                    .setMessage("Name : " + samePerson.getName() + "\nAge : " + samePerson.getAge())
                    .create().show();
        }
        if(getIntent().getParcelableExtra("parcel_data") != null){
            Book sameBook = getIntent().getParcelableExtra("parcel_data");
            new AlertDialog.Builder(this)
                    .setMessage("Author : " + sameBook.getAuthor() +
                                "\nBook : " + sameBook.getBookName() +
                                "\nPublish year : " + sameBook.getPublishTime())
                    .create().show();
        }
    }
}
